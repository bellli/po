"""po URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from channel.views import sign, \
                            show, \
                            login, \
                            create_channel, \
                            add_user_to_channel, \
                            delete_user_from_channel, \
                            update_channel_base, \
                            get_channels, quit_from_channel, \
                            send_message_to_channel, \
                            is_login, \
                            my_profile, \
                            get_my_groups, \
                            create_group, \
                            alive_group, \
                            get_my_con_email, \
                            clear_message_num, \
                            get_alive_group_user, \
                            update_user_to_channel


urlpatterns = [
    
    url(r'^show/', show),
    url(r'^is_login/', is_login),
    url(r'^create_group/', create_group),
    url(r'^update_user_to_channel/', update_user_to_channel),
    url(r'^get_alive_group_user/', get_alive_group_user),
    url(r'^clear_message_num/', clear_message_num),
    url(r'^get_my_con_email/', get_my_con_email),
    url(r'^alive_group/', alive_group),
    url(r'^get_my_groups/', get_my_groups),
    url(r'^my_profile/', my_profile),
    url(r'^add_user_to_channel/', add_user_to_channel),
    url(r'^send_message_to_channel/', send_message_to_channel),
    url(r'^delete_user_from_channel/', delete_user_from_channel),
    url(r'^update_channel_base/', update_channel_base),
    url(r'^get_channels/', get_channels),
    url(r'^quit_from_channel/', quit_from_channel),
    url(r'^sign/', sign),
    url(r'^login/', login),
    url(r'^create_channel/', create_channel),
    url(r'^admin/', admin.site.urls),
]
