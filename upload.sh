# /bash/bin
scp -r ../po/channel root@us2.polarins.xyz:/opt/po/
scp -r ../po/po root@us2.polarins.xyz:/opt/po/
scp -r ../po/k.sql root@us2.polarins.xyz:/opt/po/
scp -r ../po/templates root@us2.polarins.xyz:/opt/po/
scp -r ../po/static/build root@us2.polarins.xyz:/opt/po/static/
scp -r ../po/static/images root@us2.polarins.xyz:/opt/po/static/
scp -r ../po/chat/app root@us2.polarins.xyz:/opt/po/chat/
scp  ../po/chat/package.json root@us2.polarins.xyz:/opt/po/chat/
scp  ../po/manage.py root@us2.polarins.xyz:/opt/po/