from __future__ import unicode_literals

from django.db import models

class Groups(models.Model):
    name = models.CharField(max_length = 255, unique = True)
    creator = models.IntegerField()
    class Meta:
        db_table = 'Groups'

class Group_user(models.Model):
    group = models.CharField(max_length = 255)
    user = models.IntegerField()
    is_alive = models.IntegerField(default = 0)
    class Meta:
        unique_together = [
            ("group", "user"),
        ]
        db_table = 'Groups_user'


class user_profile(models.Model):
    user = models.IntegerField()
    icon = models.CharField(max_length = 255)
    class Meta:
        unique_together = [
            ("user"),
        ]
        db_table = 'user_profile'

class channel(models.Model):
    name = models.CharField(max_length = 255)
    # 1 private, 0 not
    group = models.IntegerField()
    is_private = models.IntegerField(default = 0)
    creator = models.IntegerField()
    create_time = models.DateTimeField(null = False)
    brief = models.CharField(null = True, max_length = 255)
    class Meta:
        unique_together = [
            ("creator", "name", "group"),
        ]

        db_table = 'channel'


class Message(models.Model):
    text = models.TextField()
    channel = models.IntegerField(default = 1)
    user = models.IntegerField(default = 1)
    send_time = models.DateTimeField(default = None)
    class Meta:
        db_table = 'Message'
        
class channel_user(models.Model):
    channel = models.IntegerField()
    user = models.IntegerField()
    is_in_channel = models.IntegerField(default = 1)
    is_in_group = models.IntegerField(default = 1)
    message_num = models.IntegerField(default = 0)
    class Meta:
        unique_together = [
            ("channel", "user"),
        ]
        db_table = 'channel_user'



class channel_message(models.Model):
    channel = models.IntegerField()
    message = models.IntegerField()
    create_time = models.DateTimeField(null = False)
    class Meta:
        db_table = 'channel_message'

class message_status(models.Model):
    user = models.IntegerField()
    channel = models.IntegerField()
    # 1 is read, 0 not
    is_read = models.IntegerField( default = 0 )
    # 
    class Meta:
        db_table = 'message_status'


import urllib2, urllib
def proxy_socket_server(data = {}):

    # data = {'name' : 'www', 'password' : '123456'}
    # data = {'name' : 'www', 'password' : '123456'}
    f = urllib2.urlopen(url = 'http://127.0.0.1:1220/', data = data)