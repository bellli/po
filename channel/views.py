
# -*- coding: utf8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.template.loader import get_template
from django.template import Template, Context
from django.views.decorators.csrf import csrf_exempt
from django.contrib import auth
from django.contrib.auth.models import User
from channel.models import *
import datetime
import json


#
def base_assets_config():
    # base_path = 'http://192.168.0.100:1022/po/static/build/'
    base_path = 'http://127.0.0.1:1022/po/static/build/'
    # base_path = 'http://us3.polarins.xyz:1022/build/'
    base_static = {
        'base_path': base_path
    }
    return base_static

# 静态模板
def show(request):
    if hasattr(request, 'session') and not request.session.session_key:
        request.session.save()
        request.session.modified = True
    request.session.save()
    request.session.modified = True
    view_name = request.GET.get('view_name')

    base_static_config = base_assets_config()
    res = {
        'base_origin':base_static_config['base_path']
    }

    path = request.GET.get('view_name', 'index')
    path = '%s%s' % (path, '.html')
    temp = get_template(path)
    html = temp.render(Context(res))
    response = HttpResponse(html)
    return response

'''
    is login
'''

''' 
    sign in
    {
        'action': 'sign_in',
        form: {
            'username': '',
            'password': '',
            'repeat_password': ''
        }
   }

   return 
   1 表示成功， 其他数字都是错误，message 返回错误信息

   {
    'status': 1,
    'message': ''
   }
'''

def check_user_exist(username = ''):
    if not username:
        return False

    cu = User.objects.filter(username = username)

    if len(cu):
        return True
    else:
        return False

def check_email_exist(email = ''):
    if not email:
        return False

    cu = User.objects.filter(email = email)

    if len(cu):
        return True
    else:
        return False

def check_login(request):

    if not request.user.is_authenticated():
        return False
    else:
        return request.user
        
@csrf_exempt
def sign(request):
    # user = auth.authenticate(username='john', password='secret')
    username = request.POST.get('username')
    password = request.POST.get('password')
    email = request.POST.get('email')

    res = {
        'status': 2
    }

    if not username or not email:
        res['message'] = '邮箱或用户名不能为空'
        return HttpResponse(json.dumps(res))

    if check_user_exist(username = username):
        res['message'] = '用户名已经存在'
        return HttpResponse(json.dumps(res))

    if check_email_exist(email = email):
        res['message'] = '邮箱已经被注册'
        return HttpResponse(json.dumps(res))

    cu = User.objects.create_user(username = username, password = password, email = email, last_login = datetime.datetime.now())
    user = auth.authenticate(username = username, password = password)
    res = {
        'status': 1
    }
    auth.login(request, user)
    n_res = {
        'action': 'syn_session_email',
        'sessionid': request.COOKIES['sessionid'],
        'email': email
    }
    proxy_socket_server(json.dumps(n_res))
    return HttpResponse(json.dumps(res))


'''
    login

    {
        'action': 'login',
        form: {
            'username': '',
            'password': ''
        }
   }

   return 
   1 表示成功， 其他数字都是错误，message 返回错误信息

   {
    'status': 1,
    'message': ''
   }

'''

@csrf_exempt
def login(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    email = request.POST.get('email')

    res = {
        'status': 2
    }
    if not email:
        res['message'] = '邮箱不能为空'
        return HttpResponse(json.dumps(res))

    users = User.objects.filter(email = email)

    if not len(users):
        res['status'] = 2
        res['message'] = '用户名或者密码错误'
        return HttpResponse(json.dumps(res))

    p_u = users[0]
    user = auth.authenticate(username = p_u.username, password = password)
    res = {
        'status': 1
    }

    print 'login success'
    if not user:
        res['status'] = 2
        res['message'] = '邮箱或者密码错误'
    else:
        auth.login(request, user)

    print dir(request)
    print request.COOKIES
    print dir(request.COOKIES)
    print request.COOKIES['sessionid']
    n_res = {
        'action': 'syn_session_email',
        'sessionid': request.COOKIES['sessionid'],
        'email': email
    }
    proxy_socket_server(json.dumps(n_res))
    return HttpResponse(json.dumps(res))

@csrf_exempt
def get_my_con_email(request):
    rq_user = check_login(request)

    n_res = {
        'action': 'syn_session_email',
        'sessionid': request.COOKIES['sessionid'],
        'email': rq_user.email
    }
    proxy_socket_server(json.dumps(n_res))
    return HttpResponse(json.dumps(n_res))

def is_login(request):
    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 2
        res['message'] = '未登录'
        return HttpResponse(json.dumps(res))

    res['email'] = rq_user.email;
    return HttpResponse(json.dumps(res))

def my_profile(request, is_http = False):
    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 2
        res['message'] = '未登录'
        return HttpResponse(json.dumps(res))

    res['email'] = rq_user.email;
    res['username'] = rq_user.username;
    if is_http:
        return res
    return HttpResponse(json.dumps(res))

'''
   create channel 
   {
        'action': 'create_channel',
        form: {
            'is_private': '0',
            'brief': '',
            'name': '',
            'users': 'a@a.com;b@b.com'
        }
   }

   return 
   1 表示成功， 其他数字都是错误，message 返回错误信息
   {
    'status': 1,
    'message': ''
   }
'''

def add_users_to_channel_db(user_list, channel_id):
    for item in user_list:
        c_u = None

        try:
            c_u = channel_user.objects.get(channel = channel_id, user = item.id)
        except Exception:
            c_u = channel_user(channel = channel_id, user = item.id)
            pass

        c_u.is_in = 1
        c_u.is_in_channel = 1
        c_u.save()


def add_users_to_groups_user_db(user_list, group_id):
    for item in user_list:
        c_u = None

        try:
            c_u = Group_user.objects.get(group = group_id, user = item.id)
        except Exception:
            c_u = Group_user(group = group_id, user = item.id)
            pass

        c_u.is_in = 1
        c_u.save()

@csrf_exempt
def create_channel(request):
    users = request.POST.get('users')
    is_private = request.POST.get('is_private') if type(request.POST.get('is_private')) == int else 0
    brief = request.POST.get('brief')
    name = request.POST.get('name')

    

    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 0
        res['message'] = '未登录'
        return HttpResponse(json.dumps(res))

    alive_group = None
    try:
        alive_group = Group_user.objects.get(user = rq_user.id, is_alive = 1)
    except Exception:
        pass

    c_channel = None
    user_name_list = users.strip().split(';')
    user_name_list.append(rq_user.email)
    user_list = User.objects.filter( email__in = user_name_list)
    channel_user_list = [ {'email': c_item.email, 'username': c_item.username } for c_item in user_list if c_item ]
    # c_channel = channel(name = name, is_private = is_private, creator = rq_user.id, create_time = datetime.datetime.utcnow(), brief = brief, alive_group = 1)
    try:
        c_channel = channel(name = name, is_private = is_private, creator = rq_user.id, create_time = datetime.datetime.utcnow(), brief = brief,  group = alive_group.group)
        c_channel.save()
        add_users_to_channel_db(user_list, channel_id = c_channel.id)
        genneral_channel = channel.objects.get(name = 'all', group = alive_group.group)
        add_users_to_channel_db(user_list, channel_id = genneral_channel.id)
        add_users_to_groups_user_db(user_list, group_id = alive_group.group)
    except Exception:
        res['status'] = 2
        res['message'] = '名称不能重复'
        return HttpResponse(json.dumps(res))

    user_list_json = [ {'email': item.email, 'username': item.username} for item in user_list if Group_user.objects.get(user = item.id, group = alive_group.group).is_alive ]

    n_res = {
        'to': user_list_json,
        'action': 'message',
        'data': {
            'action': 'add_channel',
            'data': {
                'channel': {
                    'id': c_channel.id,
                    'name': c_channel.name,
                    'brief': c_channel.brief,
                    'user_list': channel_user_list
                },
                'message': '添加群组',
                'start_time': datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"),
                'from': {'email': rq_user.email, 'username': rq_user.username}
            }
        }
    }
    proxy_socket_server(json.dumps(n_res))
    # 
    user_group_list_json = [ {'email': item.email, 'username': item.username} for item in user_list if item ]

    group_notify = {
        'to': user_group_list_json,
        'action': 'message',
        'data': {
            'action': 'add_group',
            'data': {
                'name': Groups.objects.get(id = alive_group.group).name, 
                'id': alive_group.group , 
                'is_alive': 0,
                'email': rq_user.email,
                'username': rq_user.username
            }
        }
    }
    proxy_socket_server(json.dumps(group_notify))
    return HttpResponse(json.dumps(res))


'''
   add_user_to_channel 
   {
        'action': 'add_user_to_channel',
        form: {
            'is_private': '0',
            'brief': '',
            'users': 'a@a.com;b@b.com',
            'channel': 1
        }
   }

   return 
   {
    'status': 1,
    'message': ''
   }
'''

@csrf_exempt
def get_alive_group_user(request):
    channel_id = request.POST.get('channel')
    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 0
        return HttpResponse(json.dumps(res))


    alive_group = Group_user.objects.get(user = rq_user.id, is_alive = 1)
    user_ids = [item.user for item in Group_user.objects.filter(group = alive_group.group) if item]
    if channel_id:
        user_list = [ {'email': item.email, 'username': item.username, 'is_focus': len(channel_user.objects.filter(channel = channel_id, user = item.id, is_in_channel = 1)) } for item in User.objects.filter(id__in = user_ids) if item ]
    else:
        user_list = [ {'email': item.email, 'username': item.username, 'is_focus': 0 } for item in User.objects.filter(id__in = user_ids) if item ]

    res['group_user_list'] = user_list
    return HttpResponse(json.dumps(res))

@csrf_exempt
def add_user_to_channel(request):
    users = request.POST.get('users')
    channel_id = request.POST.get('channel')
    
    user_name_list = users.strip().split(';')
    user_list = User.objects.filter( email__in = user_name_list)

    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 0
        return HttpResponse(json.dumps(res))

    add_users_to_channel_db(user_list, channel_id)

    return HttpResponse(json.dumps(res))

@csrf_exempt
def update_user_to_channel(request):
    users = request.POST.get('users')
    channel_id = request.POST.get('channel')
    
    user_name_list = users.strip().split(';')
    user_list = User.objects.filter( email__in = user_name_list)
    user_list_json = [ {'email': u_item.email, 'username': u_item.username } for u_item in user_list if u_item ]
    new_user_list_json = user_list_json[0:]
    user_list_ids = [u_item.id for u_item in user_list if u_item]
    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 0
        return HttpResponse(json.dumps(res))

    alive_group = Group_user.objects.get(user = rq_user.id, is_alive = 1)

    exit_c_u = channel_user.objects.filter(channel = channel_id).exclude(user__in = user_list_ids)

    for item in exit_c_u:
        # 
        u_i = User.objects.get(id = item.user)
        user_list_json.append({
            'email': u_i.email,
            'username': u_i.username    
        })

        # 
        item.is_in_channel = 0
        item.save()

    add_users_to_channel_db(user_list, channel_id)
    genneral_channel = channel.objects.get(name = 'all', group = alive_group.group)
    add_users_to_channel_db(user_list, channel_id = genneral_channel.id)
    add_users_to_groups_user_db(user_list, group_id = alive_group.group)
    # 
    n_res = {
        'to': user_list_json,
        'action': 'message',
        'data': {
            'action': 'update_channel_user_list',
            'data': {
                'channel': channel_id,
                'channel_users': new_user_list_json
            }
        }
    }
    proxy_socket_server(json.dumps(n_res))
    # 
    return HttpResponse(json.dumps(res))

'''
    delete_user_from_channel
    {
        'action': 'delete_user_from_channel',
        'form': {
            'channel': channel_id,
            'is_private': '0',
            'brief': '',
            'user': 'a@a.com;'
        }
    }

    return 
    {
        'status': 1,
        'message': ''
    }
'''

def delete_user_from_channel_db(user_list, channel_id):
    for item in user_list:
        c_u = None

        try:
            c_u = channel_user.objects.get(channel = channel_id, user = item.id)
            c_u.is_in = 0
            c_u.save()
        except Exception:
            pass

@csrf_exempt
def delete_user_from_channel(request):
    users = request.POST.get('users')
    channel_id = request.POST.get('channel')
    
    user_name_list = users.strip().split(';')
    user_list = User.objects.filter( email__in = user_name_list)

    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 0
        return HttpResponse(json.dumps(res))

    delete_user_from_channel_db(user_list, channel_id)

    return HttpResponse(json.dumps(res))

'''
    [push]
    update_channel_base 同时推送channel的基本信息到各成员
    {
        'action': 'update_channel',
        'form': {
            'channel': channel_id,
            'is_private': '0',
            'brief': ''
        }
    }
    return 
    {
        'status': 1,
        'res': {

            'users': [
                {
                    'name': ''
                }
            ]
            ,
            'brief': ''
        }
    }
'''
@csrf_exempt
def update_channel_base(request):

    channel_id = request.POST.get('channel')
    brief = request.POST.get('brief', '')

    try:
        c_i = channel.objects.get(id = channel_id)
        c_i.brief = brief
        c_i.save()
    except Exception:
        pass

    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 0
        return HttpResponse(json.dumps(res))

    return HttpResponse(json.dumps(res))


@csrf_exempt
def clear_message_num(request):
    channel_id = request.POST.get('channel')

    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 0
        return HttpResponse(json.dumps(res))


    c_u = channel_user.objects.get(channel = channel_id, user = rq_user.id)
    c_u.message_num = 0
    c_u.save()
    print 'clear_message_num'
    return HttpResponse(json.dumps(res))

def send_channel_unread_num(user_ob, channel_id, channel_num):
    # channel_num
    n_res = {
        'to': [user_ob],
        'action': 'message',
        'data': {
            'action': 'channel_base',
            'data': {
                'channel_name': '',
                'channel_id': channel_id,
                'channel_num': channel_num
            }
        }
    }
    proxy_socket_server(json.dumps(n_res))

def update_channel_msg_num(channel_id, user_list):
    c_us = channel_user.objects.filter(channel = channel_id)
    for c_u in c_us:
        c_u.message_num = c_u.message_num + 1
        c_u.save()
        u_o = User.objects.get(id = c_u.user)
        send_channel_unread_num({'email': u_o.email, 'username': u_o.username}, c_u.channel, c_u.message_num)
        print 'wir'

@csrf_exempt
def send_message_to_channel(request):

    channel_id = request.POST.get('channel')
    message = request.POST.get('message', '')

    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 0
        return HttpResponse(json.dumps(res))

    msg = Message(text = message, channel = channel_id, user = rq_user.id, send_time = datetime.datetime.utcnow())
    msg.save()

    msg_channel = channel_message(channel = channel_id, message = msg.id, create_time = datetime.datetime.utcnow())
    msg_channel.save()


    channel_user_id = [item.user for item in channel_user.objects.filter(channel = channel_id) if item]
    user_list = [ {'email': item.email, 'message': message} for item in User.objects.filter(id__in = channel_user_id) if item ]
    update_channel_msg_num(channel_id, user_list)

    try:
        c_i = channel.objects.get(id = channel_id)
        c_i.brief = brief
        c_i.save()
    except Exception:
        pass

    

    n_res = {
        'to': user_list,
        'action': 'message',
        'data': {
            'action': 'message',
            'data': {
                'channel': channel_id,
                'message': message,
                'send_time': datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"),
                'from': {'email': rq_user.email, 'username': rq_user.username}
            }
        }
    }
    clear_message_num(request)
    proxy_socket_server(json.dumps(n_res))
    return HttpResponse(json.dumps({}))

'''
    get channels 包含各频道未读数目
    {
        'action': 'get_channels'
    }

    return 
    {
        'status': 1,
        'action': 'get_channels',
        'res': {
            'channels': [
                {
                    'id': ,
                    'brief': '',
                    'unread_num': 10
                }
            ]
        }
    }
'''

def get_channel_message_data(channel_ob, user_id):
    c_u = channel_user.objects.get( channel = channel_ob.id, user = user_id)
    num = c_u.message_num if c_u.message_num else 10
    c_messages = Message.objects.filter(channel = channel_ob.id).order_by('-id')[0 : num]
    # c_messages.reverse()
    res = []
    for item in c_messages:
        username = User.objects.get(id = item.user).username
        # send_time = item.send_time.strftime("%Y-%m-%d %H:%M:%S") if item.send_time else ''
        a_p = ' AM' if item.send_time.hour <=12 else ' PM'
        send_time = item.send_time.strftime("%H:%M") if item.send_time else ''
        ob = {
            'message': item.text,
            'username': username,
            'time': send_time + a_p
        }
        res.append(ob)

    res.reverse()
    return res

@csrf_exempt
def get_channels(request, is_http = True):

    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 0
        return HttpResponse(json.dumps(res))

    res['res'] = {
        'channel_list': []
    }
    try:
        alive_group = Group_user.objects.get(user = rq_user.id, is_alive = 1)
    except Exception:
        pass
        if not is_http:
            return res['res']['channel_list']

        return HttpResponse(json.dumps(res))

    my_channels_ids = [item.channel for item in channel_user.objects.filter(user = rq_user.id) if item.id]
    my_channels = channel.objects.filter(id__in = my_channels_ids, group = alive_group.group)
    print my_channels

    res['res'] = {
        'channel_list': []
    }
    # 
    for item in my_channels:
        channel_user_ids = [c_item.user for c_item in channel_user.objects.filter(channel = item.id) if c_item ]
        channel_users = [{'email': u_item.email, 'username': u_item.username} for u_item in User.objects.filter(id__in = channel_user_ids) if u_item]
        c_u = channel_user.objects.get(channel = item.id, user = rq_user.id)
        ob = {
            'id': item.id,
            'brief': item.brief,
            'name': item.name,
            'group': item.group,
            'num': c_u.message_num,
            'message_data': get_channel_message_data(item, rq_user.id),
            'channel_users': channel_users
        }
        res['res']['channel_list'].append(ob)


    if not is_http:
        return res['res']['channel_list']


    res['my_profile'] = my_profile(request, True)
    res['groups'] = get_my_groups(request, is_http = False)['groups']
    # 

    return HttpResponse(json.dumps(res))

'''
    get_channel_recent_message
    {
        'action': 'get_channel_message',
        'form': {
            'channel': channel_id
        }
    }
    return 
    {
        'status': 1,
        'res': {
            'messages': [
                {
                    'message_id': ,
                    'text': '',
                    'user': {
                        'name': ''
                    }
                }
                ,
                {
                    'message_id': ,
                    'text': '',
                    'user': {
                        'name': ''
                    }
                }
            ]
        }
    }
'''
@csrf_exempt
def get_channel_recent_message(request):

    rq_user = check_login(request)
    res = {
        'status': 1
    }


    return HttpResponse(json.dumps(res))

@csrf_exempt
def alive_group(request, is_http = True):

    group_id = request.POST.get('group_id')
    rq_user = check_login(request)
    res = {
        'status': 1
    }

    for item in Group_user.objects.filter(user = rq_user.id):
        item.is_alive = 0
        item.save()

    is_my_group = Group_user.objects.get(group = group_id, user = rq_user.id)
    is_my_group.is_alive = 1
    is_my_group.save()
    if not is_http:
        return res

    return HttpResponse(json.dumps(res))

def exchange_model_ob_to_json(model_ob):
    print model_ob.__dict__

@csrf_exempt
def get_my_groups(request, is_http = True):

    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 0
        return HttpResponse(json.dumps(res))

    my_channels_ids = [item.channel for item in channel_user.objects.filter(user = rq_user.id) if item.id]
    my_channels = channel.objects.filter(id__in = my_channels_ids)

    group_ids = [ item.group for item in my_channels if item]
    group_ids = list( set(group_ids) )
    print list( set(group_ids) )
    print '*' * 100
    my_groups = []

    for g_item in group_ids:
        is_alive = 0

        g_group = Groups.objects.get(id = g_item)
        try:
            print '*' * 100
            is_alive = Group_user.objects.get(group = g_item, user = rq_user.id,).is_alive
            print is_alive
        except Exception:
            pass

        g_ob = {
                    'name': g_group.name, 
                    'id': g_group.id , 
                    'is_alive': is_alive,
                    'email': rq_user.email,
                    'username': rq_user.username
                }
        my_groups.append(g_ob)

    res['groups'] = my_groups

    if not is_http:
        return res

    
    return HttpResponse(json.dumps(res))


@csrf_exempt
def create_group(request):

    name = request.POST.get('name')

    rq_user = check_login(request)
    res = {
        'status': 1
    }
    try:
        my_group = Groups.objects.get(name = name)
        res['status'] = 2
        res['message'] = '团队名已经存在'
        return HttpResponse( json.dumps(res) )
    except Exception:
        pass

    if not name:
        res['status'] = 2
        return HttpResponse(json.dumps(res))

    new_group = Groups(creator = rq_user.id, name = name)
    new_group.save()

    new_group_user = Group_user(group = new_group.id, user = rq_user.id)
    new_group_user.save()

    gernal_channel = channel(group = new_group.id, name = 'all', creator = rq_user.id, create_time = datetime.datetime.utcnow(), brief = '包含所有团队成员')
    gernal_channel.save()

    me_in_geral = channel_user(channel = gernal_channel.id, user = rq_user.id)
    me_in_geral.save()
    # my_groups = [ {'name': item.name, 'id': item.id } for item in Group.objects.filter(id__in = group_ids) if item ]
    # res['groups'] = my_groups
    res = get_my_groups(request, is_http = False)
    n_res = {
        'action': 'message',
        'to': [{
            'email': rq_user.email
        }],
        'data': {
            'action': 'add_group',
            'data': res['groups'][-1]
        }
    }
    proxy_socket_server(json.dumps(n_res))
    return HttpResponse(json.dumps(res))

'''
    get_channel_unread_number
    {
        'action': 'get_channel_unread_number',
        'form': {
            'channel': channel_id
        }
    }

    return 
    {
        'status': 1,
        'action': 'get_channel_unread_number',
        'res': [
            {
                'channel': channel_id,
                'num': 
            }
        ],
        'meesage': ''
    }
'''
@csrf_exempt
def push_unread_num(request):

    rq_user = check_login(request)
    res = {
        'status': 1
    }

    # 

    return HttpResponse(json.dumps(res))
'''
    [push]
    quit_from_channel 完成时需要推送拥有当前channel成员的channe基本信息
    {
        'action': 'quit_from_channel',
        'form': {
            'channel': channel_id
        }
    }
    return 
    {
        'status': 1,
        'action': 'quit_from_channel',
        'res': {
            'channel': channel_id
        },
        'meesage': ''
    }
'''
@csrf_exempt
def quit_from_channel(request):
    channel_id = request.POST.get('channel')
    

    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 0
        return HttpResponse(json.dumps(res))

    user_list = User.objects.filter( email__in = [rq_user.email])
    delete_user_from_channel_db(user_list, channel_id)

    return HttpResponse(json.dumps(res))

'''
    [socket server push]
    add_message
    {
        'action': 'add_message',
        'form': {
            'channel': channel_id,
            'text': ''
        }
    }

    return 
    {
        'status': 1,
        'action': 'update_message',
        'res': {
            'channel': channel_id
            'messge_id': ''
        }
        'message': ''
    }
'''

@csrf_exempt
def add_message(request):
    channel_id = request.POST.get('channel')
    

    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 0
        return HttpResponse(json.dumps(res))

    user_list = User.objects.filter( email__in = [rq_user.email])
    delete_user_from_channel_db(user_list, channel_id)

    return HttpResponse(json.dumps(res))

'''
    update messagestate
    {
        'action': 'update_messagestate',
        'form': {
            'channel': channel_id,
            'text_ids': 'message_id1;message_id2'
        }
    }

    return 
    {
        'status': 1,
        'action': 'update_messagestate',
        'message': ''
    }
'''
@csrf_exempt
def update_messagestate(request):
    channel_id = request.POST.get('channel')
    

    rq_user = check_login(request)
    res = {
        'status': 1
    }

    if not rq_user:
        res['status'] = 0
        return HttpResponse(json.dumps(res))

    user_list = User.objects.filter( email__in = [rq_user.email])
    delete_user_from_channel_db(user_list, channel_id)

    return HttpResponse(json.dumps(res))



'''
    modify password
'''


# modify name
# @csrf_exempt
def modify_name(request):
    res = {}
    name = request.POST.get('user.name')
    pwd_token = request.POST.get('token')
    htuuid = request.POST.get('htuuid')
    brief_name = request.POST.get('brief_name')

    ht_item = HtcUser.objects.filter(name = name, pwd = pwd_token)
    if len(ht_item):
        ht_ob = ht_item[0]
        mg_seconds = dtToTime(ht_ob.vipLimitTime) - dtToTime(datetime.datetime.utcnow())
        mg_days = (int(mg_seconds) + 3600 * 24 )/ (3600 * 24)
        ht_ob.brief_name = brief_name
        if mg_days <= 0:
            ht_ob.vip = 0
            mg_days = 0
            ht_ob.save()
        res = {
            'ret': 0,
            'brief_name': ht_ob.brief_name,
            'msg': '用户名修改成功',
            'name': name,
            'token': pwd_token,
            'phone': ht_ob.phone if ht_ob.phoneType else '',
            'proxy': proxy_auth_info(user = ht_ob, htuuid = htuuid),
            'vip_limit_day': mg_days,
            'vip': ht_ob.vip,
            'vip_limit_time': ht_ob.vipLimitTime.strftime("%Y-%m-%d %H:%M:%S")
        }
        ht_ob.save()
    else:
        res = {
            'ret': 10,
            'msg': '用户名或者密码错误'
        }

    return HttpResponse(json.dumps(res))
