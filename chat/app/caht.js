var ws = require("nodejs-websocket");
var cookie = require('cookie');
var request = require('request');

var server, cons = [], cons_key = {}, cons_key_names = {};

var expand_all = function(cons, str){
    for (var i = cons.length - 1; i >= 0; i--) {
        cons[i].sendText(str.toUpperCase());
    }
}
// 
var send_to_django = function(ob, conn, after_fn){

    var call_back = function(error, response, body) {
        // var json_ob = JSON.parse( body );
        // console.log( json_ob );
    }
    var options = {
        'url': 'http://127.0.0.1:8000/' + ob.action + '/',
        form: ob.data,
        'method': 'POST',
        'dataType': 'json',
        'headers': conn.headers
    }
    
    request(options, call_back);
}
// 
/*
    {
        'action': '',
        'data': {
            
        }
    }
*/
var emit_url = {
    'send_message_to_channel': function(ob, conn){
        /*
            {
                'action': 'send_message_to_channel',
                'data': {
                    'channel': channel
                }
            }
        */
        send_to_django(ob, conn);
    },
    'get_my_con_email': function(ob, conn){
        /*
            {
                'action': 'get_my_con_email',
                'data': {
                    'channel': channel
                }
            }
        */
        send_to_django(ob, conn, function(data){
            // cons_key_names[ob.email] = ob.sessionid;
        });
    },
    'clear_message_num': function(ob, conn){
        send_to_django(ob, conn, function(data){
            // cons_key_names[ob.email] = ob.sessionid;
        });
    },
    'onEmit': function(ob, conn){
        var _this = this;
        if( ob && _this[ob.action] ){
            _this[ob.action](ob, conn);
        }
    }
}
// 
server = ws.createServer(function (conn) {
    console.log("New connection");

    var cookie_ob = cookie.parse(conn.headers.cookie);

    cons_key[cookie_ob['sessionid']] = cons_key[cookie_ob['sessionid']] || [];
    cons_key[cookie_ob['sessionid']].push( conn );

    // get_my_con_email

    emit_url.onEmit({
        'action': 'get_my_con_email'
    }, conn);
    console.log('注册');
    // 
    conn.on("text", function (str) {

        var ob = JSON.parse(str);
        emit_url.onEmit(ob, conn);
        // 
        console.log("Received "+str)

        // conn.sendText(str.toUpperCase()+"!!!");

        
        // test
    });

    conn.on("close", function (code, reason) {
        console.log("Connection closed");
    });

    conn.on('error', function(err){

    });


}).listen(8001);


var http = require('http');
var make_body_to_json = function(body_str){
    var body_arr = body_str.split('&');
    // console.log(body_arr);
    var res = {};
    for (var i = body_arr.length - 1; i >= 0; i--) {
        var t = body_arr[i].split('=');

        res[t[0]] = t[1];
    }
    return res;
}

var emit_dj_url = {
    'syn_session_email': function(ob, conn){
        cons_key_names[ob.email] = ob.sessionid;
        console.log( cons_key_names );
        for(var item in cons_key){
            console.log(item);
            console.log(typeof cons_key[item]);
        }
    },
    'message': function(ob, conn){
        // console.log(ob);
        // console.log(cons_key_names);
        // console.log(cons_key);
        console.log(ob);
        for (var i = ob.to.length - 1; i >= 0; i--) {
            var email = ob.to[i].email;
            if(cons_key[ cons_key_names[email] ]){
                for (var i = cons_key[ cons_key_names[email] ].length - 1; i >= 0; i--) {
                    try{
                        var send_sk = cons_key[ cons_key_names[email] ][i];

                        if( send_sk.readyState ){
                            cons_key[ cons_key_names[email] ][i].sendText( JSON.stringify(ob.data) );
                        }

                    }catch(err){
                        console.log('wrong');
                    }
                    
                }
            }
        }
    },
    'onEmit': function(ob, conn){
        var _this = this;

        if( ob && _this[ob.action] ){
            _this[ob.action](ob, conn);
        }

    }
}

http.createServer(function(request, response, body) {
    console.log(body);
    var headers = request.headers;
    var method = request.method;
    var url = request.url;
    var body = [];
    var actions = '/';
    if(url !== actions){
        response.end();
        return ;
    }


    request.on('error', function(err) {
        console.error(err);
    }).on('data', function(chunk) {
        console.log(chunk);
        body.push(chunk);
    }).on('end', function() {
        // console.log(Buffer.concat(body));
        body = Buffer.concat(body).toString();
        body = JSON.parse(body);
        console.log('body');
        emit_dj_url.onEmit(body);

    });
    

    for(var item in cons_key){
        // cons_key[item].sendText('django 访问我了?' + item);
    }

    response.write('</html>');
    response.end();
}).listen(1220); 
