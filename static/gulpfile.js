var gulp = require('gulp');
var gulp_css = require('gulp-css');
var gulp_less = require('gulp-less')
var base_64 = require('gulp-css-base64');
var mini_css = require('gulp-minify-css');
var uglify_js = require('gulp-uglify');
var seajs_transport = require('gulp-seajs-transport');
var gulp_react = require('gulp-react');
var gulp_concat = require('gulp-concat');

var config = {
    mangle: {except: ['define', 'require', 'module', 'exports']},
    compress: true
  };

gulp.task('app_css', function(){

  gulp.src(['assets/app/css/*.less'])
  .pipe( gulp_less() )
  .pipe( base_64() )
  .pipe( mini_css() )
  .pipe( gulp.dest('build/app/css/') )

});

gulp.task('app_js', function(){

  gulp.src(['assets/app/js/*.js', 'assets/app/js/*/*.js'], { 'base': 'assets/'})
  .pipe( gulp_react() )
  .pipe( uglify_js(config) )
  .pipe( seajs_transport() )
  .pipe( gulp.dest('build') );
  console.log(new Date())
});

gulp.task('jquery', function(){

  gulp.src(['node_modules/jquery/dist/jquery.min.js'])
  .pipe( gulp.dest('build/gallery/jquery/') );

});

gulp.task('react', function(){

  gulp.src(['node_modules/react/dist/react.js', 'node_modules/react-dom/dist/react-dom.js'])
  .pipe( gulp_concat('react.js') )
  .pipe( uglify_js({compress: true}) )
  .pipe( gulp.dest('build/gallery/react') );

});

gulp.task('seajs', function(){

  gulp.src(['node_modules/seajs/dist/sea.js'])
  .pipe( gulp.dest('build/gallery/sea/') )

  gulp.src(['assets/root_config.js'])
  .pipe( gulp.dest('build') )

});

gulp.watch(['assets/app/css/*.less', 'assets/app/js/*.js', 'assets/app/js/*/*.js'], ['app_css', 'app_js', 'app_js']);
gulp.task('default', [ 'seajs', 'jquery', 'react', 'app_css', 'app_js']);
