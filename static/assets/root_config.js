var base_host = base_host || '/hong/hs/yystatic/build/';
seajs.config({
  'base':base_host + '',
  'paths': {
    'mod': base_host + 'gallery',
    'appcss':base_host + 'app/css',
    'appjs':base_host + 'app/js'
  },
  'alias': {
    'jquery':base_host + 'gallery/jquery/jquery.min.js',
    'zepto':base_host + 'gallery/zepto/zepto.js'
  }
});
