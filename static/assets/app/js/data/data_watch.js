define(function(require, exports){
    var DataWath = {
        'data_fn': {
            'key_name': {
                'id': function(){}
            },
            'on_register_key_name': {

            }
        },
        'callback': function(item_name, id, key_name, data, fn){
            var _this = this;
            var fn_name = item_name ;
            if(_this.data_fn[fn_name] && _this.data_fn[fn_name][id] && _this.data_fn[fn_name][id][key_name]){
                 _this.data_fn[fn_name][id][key_name](data, fn , fn_name);
            }
        },
        'on_register': function(item_name, key_name, fn){
            var t = item_name + key_name;
            this.data_fn.on_register_key_name[t] = fn;
        },
        'register': function(item_name, id, key_name, fn){
            var _this = this;
            _this.data_fn[item_name] = _this.data_fn[item_name] || {};
            _this.data_fn[item_name][id] = _this.data_fn[item_name][id] || {};
            _this.data_fn[item_name][id][key_name] = fn;
            if(_this.data_fn.on_register_key_name[item_name + key_name]){
                _this.data_fn.on_register_key_name[item_name + key_name](item_name, id, key_name);
            }
        }
    }

    return DataWath;
})