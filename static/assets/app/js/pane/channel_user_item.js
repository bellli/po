define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    // 
    var ChannelUserItem = React.createClass({
        'getInitialState': function(){
            var ob = {
                'icon': 'https://cfl.dropboxstatic.com/static/images/avatar/faceholder-64-vflHTEplh.png', // parseInt(Math.random() * 10 / 2) ? 'https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg' : 'https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg',
                'username': 'None',
                'email': ''
            };
            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }

            return ob;
        },
        'resize': function(){

        },
        'show_des_pane': function(){
            
        },
        'componentDidMount': function(){

        },
        'render': function(){
            var _this = this;
            var main_el = _this.props.main_el;
            
            return (


                    <div className="channel_user_item" onClick={this.show_des_pane}>
                            <img src={this.state.icon} />
                    </div>

            )
        }
    })
   
    return ChannelUserItem;
    // 
})