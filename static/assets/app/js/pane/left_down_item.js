define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var data_watch = require('appjs/data/data_watch');
    // 
    var ChannelItem = React.createClass({
        'getInitialState': function(){
            var ob = {
                id : 0,
                is_alive : 0,
                name : "funk",
            };
            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }

            return ob;
        },
        'resize': function(){
            alert('resize');
        },
        'update_state': function(){
            this.state.update_state(this);
        },
        'focus': function(){
            this.setState({is_focus: true});
        },
        'focus_out': function(){
            this.setState({is_focus: false});
        },
        'handle_click': function(){
            var _this = this;
            if(this.state.is_alive){
                return ;
            }
            data_watch.callback('left_pane_team_item', this.state.id, 'click', _this);
        },
        'componentDidMount': function(){
            var _this = this;

            if(data_watch){
                data_watch.register('left_pane_team_item', this.state.id, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }
        },
        'render': function(){
            var _this = this;
            var main_el = _this.props.main_el;
            var num_class = this.state.num ? 'user_info_num' : 'user_info_num hide';
            var class_name = this.state.is_focus ? 'user_item user_item_focus' : 'user_item';
            var show_alive = this.state.is_alive ? 'is_alive_team' : 'is_alive_team hide';
            
            
            return (
                    <div className="team_ct_item" onClick={this.handle_click} >
                        <div className={show_alive}></div>
                        {this.state.name}
                    </div>
            )
        }
    })
   
    return ChannelItem;
    // 
})