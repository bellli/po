define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var UserItem = require('appjs/pane/user_item');
    var LeftDown = require('appjs/pane/left_down');
    // 
    var LeftPane = React.createClass({
        'getInitialState': function(){
            var ob = {
                'UserItem': {},
                'channel_select': function(id){},
                'update_state': function(){},
                'groups': [{}]
            };

            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }

            return ob;
        },
        'resize': function(){

        },
        'componentDidMount': function(){

        },
        'render': function(){
            var _this = this;
            var main_el = _this.props.main_el;
            var UserItem_data = this.state.UserItem;
            UserItem_data.update_state = function(channel_item, user_items){
                _this.state.update_state(channel_item, user_items, _this);
            }
            return (<div className="left_pane">
                      <div className="top_nav" >
                            <LeftDown data={this.state}/>
                      </div>
                      <div className="left_pane_inner">
                          <UserItem data={UserItem_data}/>
                      </div>
                    </div>);
        }
    });
   
    return LeftPane;
    // 
})