define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var ChannelUserItem = require('appjs/pane/channel_user_item');
    var data_watch = require('appjs/data/data_watch');

    // 
    var ChannelUsers = React.createClass({
        'getInitialState': function(){
            var ob = {
                'id': 0,
                'icon': 'https://www.cdn.whatsapp.net/img/v4/icon-shield.png',
                'add_user_to_channel': function(){

                },
                'channel_users': [
                    {
                        icon: 'https://cfl.dropboxstatic.com/static/images/icons/blue_dropbox_glyph-vflOJKOUw.png',
                        username: "a", 
                        email: "a@a.com"
                    },
                    {
                        icon: 'https://www.cdn.whatsapp.net/img/v4/icon-shield.png',
                        username: "a", 
                        email: "a@a.com"
                    },
                    {
                        icon: 'https://cfl.dropboxstatic.com/static/images/icons/blue_dropbox_glyph-vflOJKOUw.png',
                        username: "a", 
                        email: "a@a.com"
                    },
                    {
                        icon: 'https://www.cdn.whatsapp.net/img/v4/icon-shield.png',
                        username: "a", 
                        email: "a@a.com"
                    }
                ]
            };
            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }

            return ob;
        },
        'resize': function(){

        },
        'add_user_to_channel': function(){
            var _this = this;
            if(_this.state.add_user_to_channel){
                _this.state.add_user_to_channel();
            }
        },
        'componentDidMount': function(){
            var _this = this;
            if(data_watch){
                data_watch.register('channel_users', this.state.id, 'change', function(data, fn, fn_name){
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }
        },
        'render': function(){
            var _this = this;
            
            var user_list = this.state.channel_users;
            var user_html = [];
            for (var i = user_list.length - 1; i >= 0; i--) {
                var ob = user_list[i];
                ob.key = new Date().getTime() + Math.random();
                user_html.push(<ChannelUserItem data={ob} key={ob.key} />);
            }
            return (


                    <div className="channel_user_list_ct clr">
                            {user_html}
                            <div className="channel_user_item plus_icon_ct" onClick={this.add_user_to_channel}>
                                <div className="plus_icon"></div>
                            </div>
                    </div>

            )
        }
    })
   
    return ChannelUsers;
    // 
})