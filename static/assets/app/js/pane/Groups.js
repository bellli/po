define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var GroupItem = require('appjs/pane/Group_item');
    // 
    var Groups = React.createClass({
        'getInitialState': function(){
            var ob = {};
            ob['title'] = this.props.title;
            this.props.list = this.props.list || [];
            ob['num'] = this.props.list.length;
            ob['list'] = this.props.list;

            $(ob['list']).each(function(a, b){
                b.key = new Date().getTime() + Math.random();
            });
            
            return ob;
        },
        'resize': function(){

        },
        
        'componentDidMount': function(){

        },
        'render': function(){
            var _this = this;
            var main_el = _this.props.main_el;

            var items = [];
            for (var i = _this.state.list.length - 1; i >= 0; i--) {
                var ob = _this.state.list[i];
                var handle_click = function(e, id){
                    if(_this.props.handle_click){
                        _this.props.handle_click(e, id);
                    }
                }
                items.push( <GroupItem is_alive={ob.is_alive} title={ob.title} num={ob.num} id={ob.id} handle_click={handle_click} key={ob.key} /> );
            }

            return (
                <div className="user_items">

                    <div className="user_item user_item_title" >
                        <div className="user_item_ricon">
                            <div className="user_item_icon_ct" >
                                <img src="http://192.168.0.100:1022/po/static/images/channel_icon.png" />
                            </div>
                        </div>
                        <div className="user_item_inner" >
                            <span className="user_item_text">{this.state.title} {this.state.num}</span>
                        </div>
                    </div>

                    {items}

                </div>
            )
        }
    })
   
    return Groups;
    // 
})