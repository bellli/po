define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var ChannelItem = require('appjs/pane/channel_item');
    var data_watch = require('appjs/data/data_watch');

    // 
    var UserItem = React.createClass({
        'getInitialState': function(){
            var _this = this;
            var ob = {
                'id': '',
                'placeholder': '',
                'scrollHeight': 0
            };

            

            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }
            
            return ob;
        },
        'resize': function(){

        },
        'handle_click': function(id, channel_item){
            var _this = this;
            channel_item.focus();
            $(_this.state.channel_list).each(function(a, b){
                b.is_focus = false;
                if(b.id == id){
                    b.is_focus = true;
                }
                b.key = new Date().getTime() + Math.random();
            });
            _this.setState({'channel_list': _this.state.channel_list});
            if(_this.state.channel_select && typeof _this.state.channel_select == 'function'){
                _this.state.channel_select(id);
            }
        },
        'start_add_channel': function(){
            // data_watch.callback('J_add_channel', 1, 'show');
            data_watch.callback('add_user_to_channel_invite_box', 1, 'change', {}, function(a){
                a.setState({
                    'is_show': true
                });
            });
            // 
        },
        'componentDidMount': function(){
            var _this = this;

            if(data_watch){
                data_watch.register('channel_item_items', 1, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }
        },
        'focusout': function(){
            var _this = this;
            var input = _this.input;

            console.log( input.offsetHeight );
            console.log( input.scrollHeight );
            // input.style.height = input.scrollHeight;
            if( input.offsetHeight < input.scrollHeight ){
                // $(input).height(input.scrollHeight - 15);                
            }
        },
        'init_base_config': function(input){
            var _this = this;
            var input = _this.input;
            // _this.setState({
            //     'scrollHeight': input.scrollHeight
            // });
        },
        'add_channel': function(){
            // data_watch.callback('add_channel_to_list', this.state.id, 'update', _this);
                // <textarea className="atd_text_area" ref={function(tt){_this.input = tt;_this.init_base_config();}} onBlur={_this.focusout} placeholder={this.state.placeholder}></textarea>
        },
        'render': function(){
            var _this = this;
            
            return (
                <textarea className="atd_text_area" ref={function(tt){_this.input = tt;_this.init_base_config();}} onBlur={_this.focusout} placeholder={this.state.placeholder}></textarea>
            )
        }
    })
   
    return UserItem;
    // 
})