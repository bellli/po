define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var data_watch = require('appjs/data/data_watch');
    // 
    var ChannelItem = React.createClass({
        'getInitialState': function(){
            var ob = {
                'id': 0,
                'num': 0,
                'icon': parseInt(Math.random() * 10 / 2) ? 'https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg' : 'https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg',
                'name': 'None',
                'is_focus': false,
                'handle_click': function(id){},
                'update_state': function(){}
            };
            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }
            
            return ob;
        },
        'resize': function(){
            alert('resize');
        },
        'update_state': function(){
            this.state.update_state(this);
        },
        'focus': function(){
            this.setState({is_focus: true});
        },
        'focus_out': function(){
            this.setState({is_focus: false});
        },
        'handle_click': function(){
            var _this = this;
            data_watch.callback('channel_item', this.state.id, 'click', _this);
        },
        'componentDidMount': function(){
            var _this = this;

            if(data_watch){
                data_watch.register('channel_item', this.state.id, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }
        },
        'render': function(){
            var _this = this;
            var main_el = _this.props.main_el;
            var num_class = this.state.num ? 'user_info_num' : 'user_info_num hide';
            var class_name = this.state.is_focus ? 'user_item user_item_focus' : 'user_item';
            return (


                    <div className={class_name} onClick={this.handle_click}>
                        <div className={num_class} >{this.state.num}</div>
                        <div className="user_item_ricon">
                            <div className="user_item_icon_ct" >
                                <img src={this.state.icon} />
                            </div>
                        </div>
                        <div className="user_item_inner" >
                            <span className="user_item_text">{this.state.name}</span>
                        </div>
                    </div>

            )
        }
    })
   
    return ChannelItem;
    // 
})