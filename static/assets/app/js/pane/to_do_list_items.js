define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var data_watch = require('appjs/data/data_watch');
    var TodoListItem = require('appjs/pane/to_do_list_item');

    // 
    var ToDoListItems = React.createClass({
        'getInitialState': function(){
            var ob = {
                'id': 0,
                'num': 0,
                'icon': parseInt(Math.random() * 10 / 2) ? 'https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg' : 'https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg',
                'name': 'None',
                'list': [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
                'handle_click': function(id){},
                'update_state': function(){}
            };
            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }
            
            return ob;
        },
        'componentDidMount': function(){
            var _this = this;
            if(data_watch){
                data_watch.register('to_do_list_items', this.state.id, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }
        },
        'select': function(){
            var _this = this;
            var is_select = _this.state.select ? false : true;
            _this.setState({
                'select': is_select
            });
        },
        'close': function(){
            var _this = this;
            _this.setState({
                'is_show': false
            });
        },
        'render': function(){
            var _this = this;
            
            var class_name = _this.state.is_show ? 'add_to_do_list_form' : 'add_to_do_list_form hide';
            // list_item_selected
            var select_radio_class = _this.state.select ? 'list_item list_item_selected' : 'list_item';
            var list = [];

            $(_this.state.list).each(function(a, b){
                b.key = b.key || new Date().getTime() + '' + Math.random();
                b.name = (b.name||'官网todolist 设计') + parseInt(Math.random() * 100);
                list.push(<TodoListItem data={b} key={b.key} />);
            });

            return (
                <div className="list_pane_ct">
                    <div className="list_pane">
                        {list}
                    </div>
                </div>
            );
        }
    })
   
    return ToDoListItems;
    // 
})