define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var LeftPane = require('appjs/pane/left_pane');
    var RightPane = require('appjs/pane/right_pane');
    var data_watch = require('appjs/data/data_watch');

    // 
    var MainPane = React.createClass({
        'getInitialState': function(){
            var ob = {
                 'left_pane_data': {
                    'UserItem': {
                        'channel_name': '频道',
                        'channel_num': '3',
                        'channel_list': [
                            {
                                'name': 'bandon',
                                'num': 8,
                                'id': 100,
                                'is_show': true,
                                'channel_users': [],
                                'channel_select': function(){}
                            },
                            {
                                'name': 'bandon',
                                'num': 2,
                                'id': 1002,
                                'is_show': false,
                                'channel_users': [],
                                'channel_select': function(){}
                            },
                            {
                                'name': 'bandon',
                                'is_show': false,
                                'channel_users': [],
                                'channel_select': function(){}
                            }
                        ]
                    }
                },
                'after_mounted': function(){},
                'channel_select': function(){},
                'update_state': function(){

                }
            };

            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }
            return ob;
        },
        'resize': function(){

        },
        'componentDidMount': function(){
            var _this = this;
            this.state.after_mounted(_this);

            if(data_watch){
                data_watch.register('chat_pane', 1, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }
        },
        'render': function(){
            var _this = this;
            var main_el = _this.props.main_el;
            
            if(  _this.props.hide ){
                main_el.hide();
            }else{
                main_el.show();
            }
            var chat_pane = this.state.left_pane_data.UserItem.channel_list;
            var chat_pane_html = [];
            for (var i = chat_pane.length - 1; i >= 0; i--) {
                var ob = chat_pane[i];
                ob.after_mounted = function(a){
                    _this.state.after_mounted(a);
                }
                ob.key = ob.key ? ob.key : new Date().getTime() + Math.random() ;
                chat_pane_html.push( <RightPane key={ob.key} data={ob}/> );
            }
            // var get_channels = function(){
            //     $.ajax({
            //         'url': '/get_channels/',
            //         'type': 'POST',
            //         'dataType': 'json',
            //         'success': function(){

            //         },
            //         'error': function(){

            //         }
            //     })
            // }
            // 
            // var left_pane_data = {
            //     'UserItem': {
            //         'channel_name': '频道',
            //         'channel_num': '3',
            //         'channel_list': [
            //             {
            //                 'name': 'bandon',
            //                 'num': 8,
            //                 'id': 100
            //             },
            //             {
            //                 'name': 'bandon',
            //                 'num': 2,
            //                 'id': 1002
            //             },
            //             {
            //                 'name': 'bandon'
            //             }
            //         ]
            //     }
            // }
            var left_pane_data = this.state.left_pane_data;
            left_pane_data.update_state = function(channel_item, user_items, left_pane){
                this.state.update_state(channel_item, user_items, left_pane, _this);
            }

            return (<div  onResize={_this.resize}>
                        <LeftPane data={this.state.left_pane_data} />
                        {chat_pane_html}
                    </div>);
        }
    })
   
    return MainPane;
    // 
})