define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var data_watch = require('appjs/data/data_watch');

    // 
    var GroupsItem = React.createClass({
        'getInitialState': function(){
            var ob = {};
            ob['title'] = this.props.title;
            ob['num'] = this.props.num;
            ob['icon'] = this.props.icon || 'https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg';
            ob['key'] = new Date().getTime() + Math.random();
            ob['id'] = this.props.id; 
            return ob;
        },
        'resize': function(){

        },
        'componentDidMount': function(){
            var _this = this;

            if(data_watch){
                data_watch.register('left_pane_team_item', this.state.id, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }
        },
        'render': function(){
            var _this = this;
            var main_el = _this.props.main_el;
            var handle_click = function(e){
                data_watch.callback('left_pane_team_item', _this.state.id, 'click', _this);
                if(_this.props.handle_click){
                    _this.props.handle_click(e, _this.state.id);
                }
            }
            var is_show_num = this.state.num ? 'user_info_num' : 'user_info_num hide';
            var is_alive = this.props.is_alive ? 'is_alive_team' : 'is_alive_team hide';
            
            return (
                

                    <div className="user_item" key={this.state.key} attr_id={this.state.id} onClick={handle_click}>
                        <div className={is_show_num} >{this.state.num}</div>
                        <div className={is_alive} ></div>
                        <div className="user_item_ricon">
                            <div className="user_item_icon_ct" >
                                <img src={this.state.icon} />
                            </div>
                        </div>
                        <div className="user_item_inner" >
                            <span className="user_item_text">{this.state.title}</span>
                        </div>
                    </div>

            )
        }
    })
   
    return GroupsItem;
    // 
})