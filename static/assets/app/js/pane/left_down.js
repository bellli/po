define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var data_watch = require('appjs/data/data_watch');
    var LeftDownItem = require('appjs/pane/left_down_item');
    var data_watch = require('appjs/data/data_watch');

    // 
    var ChannelItem = React.createClass({
        'getInitialState': function(){
            var ob = {
                'id': 0,
                'name': '',
                'groups' : [],
                'is_show': false
            };
            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }
            ob['name'] = '';
            $(ob.groups).each(function(a, b){
                b.key = b.key || new Date().getTime() + Math.random();
                if(b.is_alive){
                    ob['name'] = b.name;
                    ob['id'] = b.id;
                    ob['username'] = b.username;
                    ob['email'] = b.email;
                }
            });
            return ob;
        },
        'resize': function(){
            alert('resize');
        },
        'update_state': function(){
            this.state.update_state(this);
        },
        'focus': function(){
            this.setState({is_focus: true});
        },
        'focus_out': function(){
            this.setState({is_focus: false});
        },
        'handle_click': function(){
            var _this = this;
            data_watch.callback('channel_item', this.state.id, 'click', _this);
        },
        'add_group': function(data){
            var _this = this;
            var is_in = false;
            $(_this.state.groups).each(function(a, b){
                if(b.id == data.id){
                    is_in = true;
                }
            });
            if(!is_in){
                _this.state.groups.push(data);
                data.key = new Date().getTime() + Math.random();
                _this.setState({'groups': _this.state.groups});
            }
        },
        'componentDidMount': function(){
            var _this = this;

            if(data_watch){
                data_watch.register('left_down_groups', 1, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }

            $(document.body).on('click', function(){
                _this.setState({
                    // 'is_show': false
                })
            });
        },
        'toggle': function(){
            var _this = this;
            if(_this.state.is_show){
                _this.setState({
                    'is_show': false
                });
            }else{
                _this.setState({
                    'is_show': true
                });
            }
        },
        'add_team': function(){
            data_watch.callback('add_team_pane', 1, 'show');
        },
        'render': function(){
            var _this = this;

            var class_name = this.state.is_show ? 'profile_dn_des' : 'profile_dn_des hide';

            var t_item = [];
            for (var i = this.state.groups.length - 1; i >= 0; i--) {
                var ob = this.state.groups[i];
                t_item.push(<LeftDownItem data={ob} key={ob.key} />);
            }

            return (


                    <div className="channel_hd_brief group_hd_brief clr">
                        
                        <div className="channel_hd_txt" onClick={this.toggle}>
                            <div className="channel_hd_name">
                                {this.state.name}
                            </div>
                            <div className="channel_hd_person_name">
                                {this.state.email}
                            </div>
                        </div>

                        <div className={class_name}>
                            <div className="team_ct">
                                <div className="team_ct_hd">
                                团队
                                <span className="add_team" onClick={this.add_team}>新建</span>
                                </div>
                                {t_item}
                            </div>
                        </div>

                    </div>

            )
        }
    })
   
    return ChannelItem;
    // 
})