define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    // 
    var UserItem = React.createClass({
        'getInitialState': function(){
            var ob = {};
            return ob;
        },
        'resize': function(){

        },
        'componentDidMount': function(){

        },
        'render': function(){
            var _this = this;
            var main_el = _this.props.main_el;
            return (
                <div className="user_items">

                    <div className="user_item user_item_title" >
                        <div className="user_item_ricon">
                            <div className="user_item_icon_ct" >
                                <img src="http://192.168.0.100:1022/po/static/images/channel_icon.png" />
                            </div>
                        </div>
                        <div className="user_item_inner" >
                            <span className="user_item_text">频道 3</span>
                        </div>
                    </div>

                    <div className="user_item" >
                        <div className="user_info_num" >2</div>
                        <div className="user_item_ricon">
                            <div className="user_item_icon_ct" >
                                <img src="https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg" />
                            </div>
                        </div>
                        <div className="user_item_inner" >
                            <span className="user_item_text">bandon</span>
                        </div>
                    </div>

                    <div className="user_item" >
                        <div className="user_item_ricon">
                            <div className="user_item_icon_ct" >
                                <img src="https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg" />
                            </div>
                        </div>
                        <div className="user_item_inner" >
                            <span className="user_item_text">Jeff &bull; end</span>
                        </div>
                    </div>

                </div>
            )
        }
    })
   
    return UserItem;
    // 
})