define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var data_watch = require('appjs/data/data_watch');
    // 
    var ToDoListItem = React.createClass({
        'getInitialState': function(){
            var ob = {
                'id': 0,
                'num': 0,
                'icon': parseInt(Math.random() * 10 / 2) ? 'https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg' : 'https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg',
                'name': 'None',
                'is_show': true,
                'handle_click': function(id){},
                'update_state': function(){}
            };
            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }
            
            return ob;
        },
        'componentDidMount': function(){
            var _this = this;
            if(data_watch){
                data_watch.register('to_to_list_item', this.state.id, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }
        },
        'select': function(){
            var _this = this;
            var is_select = _this.state.select ? false : true;
            _this.setState({
                'select': is_select
            });
            setTimeout(function(){
                _this.setState({
                    'is_show': false
                });
            }, 500);
        },
        'close': function(){
            var _this = this;
            _this.setState({
                'is_show': false
            });
        },
        'render': function(){
            var _this = this;
            
            var class_name = _this.state.is_show ? 'add_to_do_list_form' : 'add_to_do_list_form hide';
            // list_item_selected
            var select_radio_class = _this.state.select ? 'list_item list_item_selected' : 'list_item';
            var select_radio_class = _this.state.is_show ? select_radio_class : select_radio_class + ' hide';

            return (
                <div className={select_radio_class} >
                    <div className="list_item_select_ct"  onClick={_this.select}>
                        <div className="list_select">
                            <div className="list_select_pointer"></div>
                        </div>
                    </div>
                    <div className="list_item_brief_ct">
                        <div className="list_item_brief_text">{this.state.name}</div>
                        <div className="list_item_create_info">5月12日 周五 来自 Jeffbloom</div>
                    </div>
                    <div className="list_create_hd">
                        <img src="https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg" />
                    </div>
                </div>
            );
        }
    })
   
    return ToDoListItem;
    // 
})