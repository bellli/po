define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var data_watch = require('appjs/data/data_watch');
    var UserChannelForm = require('appjs/forms/add_user_to_channel_form');
    // 
    var Info = React.createClass({
        'getInitialState': function(){
            var ob = {
                'id': 0,
                'name': 'None',
                'send_message': function(){alert('m')},
                'is_show': false,
                'info_text': ''
            };
            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }
            return ob;
        },
        'send_message': function(e){
            var key_code = e.keyCode;

            if(key_code != 13){

                return ;
            }
            var ipt = e.target || e.srcElement;
            ipt = $(ipt);
            var val = ipt.val();
            if(this.state.send_message && typeof this.state.send_message == 'function'){
                this.state.send_message(this.state.id, val);
            }
            this.clear();
        },
        'clear': function(){
            this.input.value = '';
        },
        'close': function(){
            var _this = this;
            _this.setState({
                'is_show': false
            });
        },
        'show_text': function(text){
            var _this = this;
            _this.setState({
                'is_show': true,
                'info_text': text
            });
            setTimeout(function(){
                _this.setState({
                    'is_show': false
                });
            }, 2000);
        },
        'componentDidMount': function(){
            var _this = this;

            if(data_watch){
                data_watch.register('info_message', 1, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }
        },
        'render': function(){
            var _this = this;
            
            var show_class_name = this.state.is_show ? 'info_msg_ct': 'info_msg_ct hide';
            
            return (
                <div className={show_class_name}>{this.state.info_text}</div>
            );
        }
    })
   
    return Info;
    // 
})