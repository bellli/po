
define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var MessageForm = require('appjs/forms/MessageForm.js');
    var MessageItem = require('appjs/pane/message_item');
    var ChannelUsers = require('appjs/pane/channel_users');
    var data_watch = require('appjs/data/data_watch');
    var TodoListForm = require('appjs/forms/add_to_do_list_form');
    var TodoListItems = require('appjs/pane/to_do_list_items');
    // 
    var RightPane = React.createClass({
        'getInitialState': function(){
            var _this = this;
            var ob = {
                'icon': 'https://www.cdn.whatsapp.net/img/v4/icon-shield.png',
                'name': 'None',
                'id': 0,
                'send_message': function(){alert('r')},
                'after_mounted': function(a){
                    return _this;
                },
                'is_show': false,
                'brief': '这里是设计师部门的主场，主要用于设计师团队内部的沟通交流，知识分享等。',
                'message_data': [{}],
                'message_data': [],
                'first_show': true,
                'message_form_key': new Date().getTime() + '*' + Math.random() + '*' + Math.random(),
                'channel_user_key': new Date().getTime() + '*' + Math.random() + '*' + Math.random() ,
                'TodoListForm_key': new Date().getTime() + '*' + Math.random() + '*' + Math.random(),
                'channel_users': [
                    {username: "a", email: "a@a.com"},
                    {username: "a", email: "a@a.com"}
                ]
            };
            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }
            var t = ob.send_message;
            ob.send_message = function(a, b){
                t(a, b, _this);
            }
            return ob;
        },
        'componentDidMount': function(){
            var _this = this;
            this.state.after_mounted(_this);
            _this.to_bottom();
            if(data_watch){
                data_watch.register('right_pane', this.state.id, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }
        },
        'show_user_channel_box': function(){
            
        },
        'resize': function(){

        },
        'hide': function(){
            this.setState({'is_show': false});
        },
        'show': function(){
            var _this = this;
            _this.setState({'is_show': true});
            
            if(_this.state.first_show){

                setTimeout(function(){
                    _this.to_bottom();
                }, 50);
                
                _this.setState({'first_show': false});
            }

        },
        'first_show': function(){
            if(this.state.first_show){
                this.to_bottom();
                this.setState({'first_show': false});
            }
        },
        'check_to_show': function(id){
            if(id == this.state.id){
                this.show();
            }else{
                this.hide();
            }
        },
        'show_to_do_form': function(){
            var _this = this;
            data_watch.callback('to_do_form', _this.state.id, 'change', {}, function(a, b){
                a.setState({
                    'is_show': true
                });
            });
        },
        'to_bottom': function(){
            var _this = this;
            _this.pane.scrollTop = _this.pane.scrollHeight;
        },
        'set_channel_users': function(channel_users){
            var _this = this;
            _this.setState({
                'channel_users': channel_users,
                'channel_user_key': new Date().getTime() +  Math.random() + Math.random() * 200000 
            });
        },
        'add_message': function(data){
            this.state.message_data.push(data);
            var t = this.state.message_data;
            this.setState({'message_data': t});
        },
        'render': function(){
            var _this = this;
            var main_el = _this.props.main_el;
            // 
            var to_do_form_data = {
                'id': this.state.id
            }
            // 
            var message_data = this.state.message_data;
            var message_list = [];
            for (var i = message_data.length - 1; i >= 0; i--) {
                var ob = message_data[message_data.length - i - 1];    
                ob.key = ob.key ? ob.key : new Date().getTime() + '*' + Math.random() + '*' + Math.random();
                message_list.push( <MessageItem key={ob.key} data={ob} /> );
            }
            


            var channel_user_list = {
                'channel_users': this.state.channel_users,
                'id': this.state.id,
                'add_user_to_channel': function(){

                    data_watch.callback('add_user_to_channel_box', 1, 'change', {}, function(a, b){
                        a.setState({
                            'is_show': true,
                            'name': _this.state.name,
                            'id': _this.state.id
                        });
                    });
                    
                    data_watch.callback('add_user_to_channel_form', 1, 'change', {}, function(a, b){
                        a.setState({
                            'id': _this.state.id
                        });
                        a.load_data( _this.state.id );
                    });

                    // 'add_user_to_channel_form', 1, 'change'

                }
            }
            var is_show = this.state.is_show ? 'right_pane when_right_show' : 'right_pane when_right_show hide';
            // 
            return (<div className={is_show}>

                      <div className="top_nav" >
                            <div className="channel_hd_brief clr">
                                <div className="channel_hd_pic">
                                    <img src={this.state.icon} className="com_in_img" />
                                </div>
                                <div className="channel_hd_txt">
                                    <div className="channel_hd_name">
                                        {this.state.name}
                                    </div>
                                </div>
                            </div>
                            <div className="to_do_icon"></div>
                      </div>  
                      <div className="to_do_pane" >
                            <div className="to_do_top_nav">
                                <a href="javascript:;" className="start_to_do" onClick={_this.show_to_do_form}>
                                    <span className="add_icon"></span>
                                    <span className="start_to_do_text">添加任务...</span>
                                </a>
                                <a href="" className="finished_to_action"></a>
                            </div>
                            <TodoListForm  data={to_do_form_data} />
                            <TodoListItems />
                      </div>
                      <div className="right_pane_inner_ct">
                          <div className="right_pane_inner" ref={function(el) {
                                                              _this.pane = el;
                                                            }}>

                            <div className="msg_item">
                                <div className="msg_item_logo">
                                    <img src="https://cfl.dropboxstatic.com/static/images/icons/blue_dropbox_glyph-vflOJKOUw.png" />
                                </div>
                                <div className="channel_detail_show">
                                    <div className="channel_detail_title">
                                        频道详情
                                    </div>
                                    <div className="channel_detail_text">
                                    {this.state.brief}
                                    </div>
                                </div>
                                <ChannelUsers key={this.state.channel_user_key} data={channel_user_list} />

                            </div>

                            {message_list}

                          </div>
                      </div>

                      <MessageForm key={_this.state.message_form_key}  data={this.state} />

                      

                    </div>);
        }
    })
   
    return RightPane;
    // 
})