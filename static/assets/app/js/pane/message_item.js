define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    // 
    var MessageItem = React.createClass({
        'getInitialState': function(){
            var ob = {
                'icon': 'https://www.cdn.whatsapp.net/img/v4/icon-shield.png',
                'username': 'Nodne',
                'message': '',
                'time': 'PM',
                'key': new Date().getTime() + '*' + Math.random() + '*' + Math.random()
            };
            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }

            // var at_names = ob['message'].match(/@\w+\s+/g);
            // //  <span className="message_at_name">
            // $(at_names).each(function(a, b){
            //     var at_name_style = <span className="message_at_name"> + b + </span>;
            //     ob.message = ob.message.replace(b, at_name_style);
            // });

            return ob;
        },
        'resize': function(){

        },
        'componentDidMount': function(){

        },
        'render': function(){
            var _this = this;
            // message_at_name
            var message = this.state.message;
            var random_split = new Date().getTime() + Math.random();
            var at_names = message.match(/@\w+\s+/g);
            var split_str = message.split(/@\w+[\s+|s]/g, random_split);

            var h_m = [];
            if(at_names){
                
                $(split_str).each(function(a, b){
                    var key = new Date().getTime() + '*' + Math.random() + '*' + Math.random();
                    var t = <span key={key}>{b}</span>;
                    var key = new Date().getTime() + '*' + Math.random() + '*' + Math.random();
                    var n = <span key={key}></span>;
                    var key = new Date().getTime() + '*' + Math.random() + '*' + Math.random();
                    if(at_names[a]){
                        var n = <span key={key}   className="message_at_name">{at_names[a]}</span>
                    }
                    h_m.push(t);
                    h_m.push(n);
                });

            }else{
                h_m.push(<span key={_this.state.key} >{message}</span>);
            }

            return (


                    <div className="msg_item">
                        <div className="msg_item_logo">
                            <img src={this.state.icon} />
                        </div>
                        <div className="channel_message_ct">
                            <div className="channel_message_title">
                                <span>{this.state.username}</span><span className="channel_message_time">{this.state.time}</span>
                            </div>
                            <div className="channel_message_text">
                                {h_m}
                            </div>
                        </div>
                    </div>

            )
        }
    })
   
    return MessageItem;
    // 
})