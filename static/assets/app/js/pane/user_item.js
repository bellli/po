define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var ChannelItem = require('appjs/pane/channel_item');
    var data_watch = require('appjs/data/data_watch');

    // 
    var UserItem = React.createClass({
        'getInitialState': function(){
            var _this = this;
            var ob = {
                'channel_name': '',
                'channel_id': '',
                'channel_num': '',
                'channel_list': [{}],
                'channel_select': function(id){},
                'update_state': function(){}
            };

            

            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }
            $(ob.channel_list).each(function(a, b){
                b.handle_click = _this.handle_click;
                b.key = b.key || new Date().getTime() + Math.random();
            });
            ob['channel_num'] = ob.channel_list.length;
            return ob;
        },
        'resize': function(){

        },
        'handle_click': function(id, channel_item){
            var _this = this;
            channel_item.focus();
            $(_this.state.channel_list).each(function(a, b){
                b.is_focus = false;
                if(b.id == id){
                    b.is_focus = true;
                }
                b.key = new Date().getTime() + Math.random();
            });
            _this.setState({'channel_list': _this.state.channel_list});
            if(_this.state.channel_select && typeof _this.state.channel_select == 'function'){
                _this.state.channel_select(id);
            }
        },
        'start_add_channel': function(){
            // data_watch.callback('J_add_channel', 1, 'show');
            data_watch.callback('add_user_to_channel_invite_box', 1, 'change', {}, function(a){
                a.setState({
                    'is_show': true
                });
            });
            // 
        },
        'componentDidMount': function(){
            var _this = this;

            if(data_watch){
                data_watch.register('channel_item_items', 1, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }
        },
        'add_channel': function(){
            // data_watch.callback('add_channel_to_list', this.state.id, 'update', _this);
        },
        'render': function(){
            var _this = this;
            var main_el = _this.props.main_el;

            var channel_items = [];
            for (var i = this.state.channel_list.length - 1; i >= 0; i--) {
                var ob = this.state.channel_list[this.state.channel_list.length - 1 - i];
                ob.update_state = function(channel_item){
                    _this.setState.update_state(channel_item, _this);
                }
                channel_items.push(<ChannelItem data={ob} key={ob.key} />);
            }
            return (
                <div className="user_items">

                    <div className="user_item user_item_title" >
                        <div className="user_item_ricon">
                            <div className="user_item_icon_ct user_item_chat_icon" >
                            </div>
                        </div>
                        <div className="user_item_inner" >
                            <span className="user_item_text">{this.state.channel_name} {this.state.channel_num}</span>
                        </div>
                        <div className="add_channel_action" onClick={this.start_add_channel}>
                            <div className="plus_icon"></div>
                        </div>
                    </div>

                    {channel_items}

                </div>
            )
        }
    })
   
    return UserItem;
    // 
})