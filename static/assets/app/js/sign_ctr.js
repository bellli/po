define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    // 
    var SignForm = React.createClass({
        'getInitialState': function(){

            // this.setState({'action': this.props.action});
            ob = {};
            ob.action = this.props.action;
            $.extend(
                ob,
                {'user_name': '', 'password': ''}
            );
            return ob;

        },
        'handleChange': function(e){
            var el = e.target || e.srcElement;
            el = $(el);
            var val = el.val();
            var name = el.attr('name');

            var ob = {}
            ob[name] = val;
            this.setState(ob);
        },
        'submit': function(){

            var _this = this;
            $.ajax({
                'url':  _this.state.action,
                'type': _this.state.type,
                'dataType': 'json',
                'success': function(data){
                    alert('success');
                },
                'error': function(){
                    alert('error');
                }
            })

        },
        'render': function(){


            return (

                <div>
                    <form action={this.props.action} type={this.props.type} >
                    <input type="text" onChange={this.handleChange} value={this.state.user_name} name="user_name" placeholder="用户名" />
                    <input type="text" onChange={this.handleChange} value={this.state.password} name="password" placeholder="密码" />
                    <a onClick={this.submit} href="javascript:;">注册</a>
                    </form>
                </div>
            );
        }
    });
    // 
    ReactDOM.render(
    <SignForm action="/" type="POST" />,
    document.getElementById('J_sign_form')
  );

    // 
})