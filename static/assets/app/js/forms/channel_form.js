define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var ConcatForm = require('appjs/forms/ConcatForm.js');
    var Groups = require('appjs/pane/Groups');
    var data_watch = require('appjs/data/data_watch');

    // 
    // var Formitem = React.createClass({
    //     'render': function(){
            
    //         return (<div className="ipt_item">
    //                     <input type={this.props.data.type}  onChange={this.props.data.ipt_change} name={this.props.data.name} className="input"   placeholder={this.props.data.placeholder} />
    //                 </div>);
    //     }
    // })
    // 
    var ChannelForm = React.createClass({
        'getInitialState': function(){

            // this.setState({'action': this.props.action});
            ob = {};
            ob.action = this.props.action;
            var ipt_items = [
                {
                    'type': 'text',
                    'placeholder': '频道名称',
                    'name': 'name'
                },
                {
                    'type': 'text',
                    'placeholder': '用户名称以;分割',
                    'name': 'users'
                },
                {
                    'type': 'text',
                    'placeholder': '简介',
                    'name': 'brief'
                }
                
            ]
            ob.ipt_items = ipt_items;
            ob.list = this.props.list || [];
            $(ob['list']).each(function(a, b){
                b.key = new Date().getTime() + Math.random();
            });
            $.extend(
                ob,
                {
                    'data': {}
                }
            );
            return ob;

        },
        'setList': function(list){
            $(list).each(function(a, b){
                b.key = new Date().getTime() + Math.random();
            });

            this.setState({
                'list': list
            });
        },
        'handleChange': function(e){
            var el = e.target || e.srcElement;
            el = $(el);

            var val = el.val();
            var name = el.attr('name');
            console.log(val);
            var ob = {
                'data': {}
            }
            var pre_data = this.state.data;
            ob['data'][name] = val;
            var n_data = $.extend(pre_data, ob.data);
            console.log( n_data );
            // this.setState( n_data );
        },
        'submit': function(){

            var _this = this;
            $.ajax({
                'url':  _this.state.action,
                'type': _this.props.type,
                'dataType': 'json',
                'data': _this.state.data,
                'success': function(data){
                    data_watch.callback('channel_form_result', 1, 'change', data);
                    if(data.status != 1){
                        alert(data.message);    
                    }
                },
                'error': function(){
                    alert('error');
                }
            })

        },
        'out_hide': function(){
            data_watch.callback('J_add_channel', 1, 'hide');
        },
        'render': function(){
            
            var _this = this;
            var handle_data = function(data, _form){
                data_watch.callback('channel_form_result', 1, 'change', data);
                _this.props.handle_data(data, _form, _this);
            }
            var handle_click = function(e, id){
                if(_this.props.handle_click){
                    _this.props.handle_click(e, id, _this);
                }
            }
            var j = [];
            for (var i =  100; i >= 0; i--) {
                j.push(<li>Polar</li>);
            }

            var group_list_html = [];
            if(_this.state.list.length){
                var key = new Date().getTime() + Math.random();
                group_list_html.push(<Groups key={key} title="频道列表" handle_click={handle_click} num="3" list={_this.state.list} />);
            }
            var group_list_html = [];

            return (
                <div className="base_auth_form">
                    <div className="close_pane_action" onClick={this.out_hide}>
                        <div className="plus_icon"></div>
                    </div>
                    <ConcatForm title={this.props.title} action="/create_channel/" type="POST" exe={this.props.exe} handle_data={handle_data}  exchange_text="" action_text="创建" is_show={this.props.is_show} ipt_items={_this.state.ipt_items}/>
                    {group_list_html}
                </div>
            )
        }
    });

  //   ReactDOM.render(
  //   <GroupForm action="/" type="POST" />,
  //   document.getElementById('J_sign_form')
  // );
    return ChannelForm;
    // 
})