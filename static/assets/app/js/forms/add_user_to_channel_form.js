define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var ConcatForm = require('appjs/forms/ConcatForm.js');
    var Groups = require('appjs/pane/Groups');
    var data_watch = require('appjs/data/data_watch');
    var UserItemChannelForm = require('appjs/forms/user_item_channel_form');
    // 
    // var Formitem = React.createClass({
    //     'render': function(){
            
    //         return (<div className="ipt_item">
    //                     <input type={this.props.data.type}  onChange={this.props.data.ipt_change} name={this.props.data.name} className="input"   placeholder={this.props.data.placeholder} />
    //                 </div>);
    //     }
    // })
    // 
    var ChannelForm = React.createClass({
        'getInitialState': function(){

            // this.setState({'action': this.props.action});
            var ob = {
                'id': 0,
                'num': 0,
                'icon': parseInt(Math.random() * 10 / 2) ? 'https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg' : 'https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg',
                'name': 'None',
                'is_focus': false,
                'group_user_list': [{'username': 'bandon.li'}, {}, {}, {}],
                'selected_email_list': [],
                'handle_click': function(id){},
                'update_state': function(){}
            };
            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }

            $(ob.group_user_list).each(function(a, b){
                b.key = new Date().getTime() + Math.random();
            });

            return ob;

        },
        'setList': function(list){
            $(list).each(function(a, b){
                b.key = new Date().getTime() + Math.random();
            });

            this.setState({
                'list': list
            });
        },
        'handleChange': function(e){
            var el = e.target || e.srcElement;
            el = $(el);

            var val = el.val();
            var name = el.attr('name');
            console.log(val);
            var ob = {
                'data': {}
            }
            var pre_data = this.state.data;
            ob['data'][name] = val;
            var n_data = $.extend(pre_data, ob.data);
            console.log( n_data );
            // this.setState( n_data );
        },
        'update_user_to_channel': function(){

            var _this = this;
            $.ajax({
                'url': '/update_user_to_channel/',
                'type': 'POST',
                'dataType': 'json',
                'data': {
                    'channel': _this.state.id,
                    'users': _this.state.selected_email_list.join(';')
                },
                'success': function(data){
                    data_watch.callback('info_message', 1, 'change', {}, function(a, b){
                        a.show_text('保存成功');
                    });
                    // if(data.status != 1){
                        // alert(data.message);    
                    // }
                },
                'error': function(){
                    alert('error');
                }
            })

        },
        'load_data': function(id){
            var _this = this;
            $.ajax({
                'url': '/get_alive_group_user/',
                'type': 'POST',
                'dataType': 'json',
                'data': {
                    'channel': id || _this.state.id
                },
                'success': function(data){
                    var t = [];
                    $(data.group_user_list).each(function(a, b){
                        b.key = new Date().getTime() + Math.random();
                        if(b.is_focus){
                            t.push(b.email);
                        }
                    });
                    _this.setState({'group_user_list': data.group_user_list, 'selected_email_list': t});
                },
                'error': function(){

                }
            });
        },
        'componentDidMount': function(){
            // get_alive_group_user
            var _this = this;
            _this.load_data();
            if(data_watch){
                data_watch.register('add_user_to_channel_form', 1, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }
        },
        'add_email': function(e){
            var key_code = e.keyCode;
            var _this = this;
            if(key_code != 13){
                return ;
            }
            var ipt = $(_this.ipt);
            var new_fd = {
                'username': '',
                'email': ipt.val(),
                'is_focus': true,
                'key': new Date().getTime() + Math.random()
            }
            _this.state.selected_email_list.push(ipt.val());
            _this.state.group_user_list.push(new_fd);
            _this.setState({
                'group_user_list': _this.state.group_user_list,
                'selected_email_list': _this.state.selected_email_list
            });
            ipt.val('');
        },
        'out_hide': function(){
            data_watch.callback('J_add_channel', 1, 'hide');
        },
        'render': function(){
            
            var _this = this;
            console.log(this.state.selected_email_list);
            var c_list = [];
            for (var i = this.state['group_user_list'].length - 1; i >= 0; i--) {
                var ob = this.state['group_user_list'][i];

                ob.selected = function(email, is_choose){
                    var selected_email_list = _this.state.selected_email_list;
                    var is_in = false;
                    var e_index = -1;
                    $(selected_email_list).each(function(a, b){
                        if(b == email){
                            is_in = true;
                            e_index = a;
                        }
                    });

                    if(!is_choose && is_in){
                        selected_email_list.splice(e_index, 1);
                    }
                    if(is_choose && !is_in){
                        selected_email_list.push(email);
                    }
                    _this.setState({'selected_email_list': selected_email_list});
                    console.log(selected_email_list);
                }

                c_list.push(<UserItemChannelForm data={ob} key={ob.key} />);
            }

            return (
                <div className="">
                    
                    {c_list}

                    <div className="user_item" >
                        <div className="user_item_ricon">
                            <div className="user_item_icon_ct invite_ipt_icon">
                            </div>
                        </div>
                        <div className="user_item_inner">
                            <input className="invite_ipt" ref={function(ipt){_this.ipt = ipt;}} value={this.state.add_email} onKeyUp={this.add_email} placeholder="添加你需要邀请的朋友邮箱" />
                        </div>
                        <div className="item_radio_circle">
                            <div className="item_radio_circle_pointer"></div>
                        </div>
                    </div>

                    <div className="user_item user_item_focus add_com_mask_btn" onClick={this.update_user_to_channel}>
                        保存
                    </div>
                </div>
            )
        }
    });

    // <div className="user_item user_item_focus">
    //     <div className="user_item_ricon">
    //         <div className="user_item_icon_ct">
    //             <img src="http://192.168.0.100:1022/po/static/images/fly.png" />
    //         </div>
    //     </div>
    //     <div className="user_item_inner">
    //         <span className="user_item_text">bandon.li</span>
    //     </div>
    //     <div className="item_radio_circle">
    //         <div className="item_radio_circle_pointer"></div>
    //     </div>
    // </div>

    return ChannelForm;
    // 
})