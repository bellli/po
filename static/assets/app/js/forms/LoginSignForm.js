define(function(require, exports){
    var SignForm = require('appjs/forms/sign_form.js');
    var LoginForm = require('appjs/forms/login_form.js');
    require('gallery/react/react');
    var data_watch = require('appjs/data/data_watch');


    var LoginSignForm = React.createClass({
        'getInitialState': function(){
            var states = {
                'show_sign': false,
                'show_login': true,
                'hide_all': false
            }
            states['show_sign'] = typeof this.props.show_sign == 'boolean' ? this.props.show_sign : false;
            states['show_login'] = typeof this.props.show_login == 'boolean' ? this.props.show_login : true;
            states['hide_all'] = typeof this.props.hide_all == 'boolean' ? this.props.hide_all : true;
            return states;
        },
        'out_hide': function(){
            var _this = this;
            data_watch.callback('login_sign_pane', 1, 'hide', _this);
        },
        'render': function(){
            var _this = this;


            var exchange_state = function(){
                if(_this.state.show_sign){
                    _this.setState({
                        'show_sign': false,
                        'show_login': true
                    });
                }else{
                    _this.setState({
                        'show_sign': true,
                        'show_login': false
                    });
                }
            }

            var hide_all = function(){
                _this.setState({
                    'show_sign': false,
                    'show_login': false,
                    'hide_all': true
                });
                if(_this.props.success){
                    _this.props.success();
                }
            }
            var handle_data = function(data, _form){

                data_watch.callback('login_sign_result', 1, 'data', data);
                if(data.status == 1){
                    hide_all();
                }else{
                    _form.show_error(data.message);
                }

            }
            var  class_name = _this.state.hide_all ? 'base_auth_form hide' : 'base_auth_form ';
            return (
                <div className={class_name}>
                    <div className="close_pane_action" onClick={this.out_hide}>
                        <div className="plus_icon"></div>
                    </div>

                    <div className="auth_logo">
                        <span className="auth_logo_icon">
                            <span className="auth_logo_beta">beta</span>
                        </span>
                    </div>
                    <SignForm is_show={this.state.show_sign} exe={exchange_state} handle_data={handle_data} title="注册" />
                    <LoginForm is_show={this.state.show_login} exe={exchange_state} handle_data={handle_data} title="登录" />
                </div>
            )

        }
    });

    return LoginSignForm;
})