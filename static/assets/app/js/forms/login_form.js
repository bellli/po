define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var ConcatForm = require('appjs/forms/ConcatForm.js');
    // 
    // var Formitem = React.createClass({
    //     'render': function(){
            
    //         return (<div className="ipt_item">
    //                     <input type={this.props.data.type}  onChange={this.props.data.ipt_change} name={this.props.data.name} className="input"   placeholder={this.props.data.placeholder} />
    //                 </div>);
    //     }
    // })
    // 
    var SignForm = React.createClass({
        'getInitialState': function(){

            // this.setState({'action': this.props.action});
            ob = {};
            ob.action = this.props.action;
            var ipt_items = [
                {
                    'type': 'text',
                    'placeholder': '邮箱',
                    'name': 'email'
                },
                {
                    'type': 'text',
                    'placeholder': '密码',
                    'name': 'password'
                }
            ]
            ob.ipt_items = ipt_items;
            $.extend(
                ob,
                {
                    'data': {}
                }
            );
            return ob;

        },
        'handleChange': function(e){
            var el = e.target || e.srcElement;
            el = $(el);

            var val = el.val();
            var name = el.attr('name');
            console.log(val);
            var ob = {
                'data': {}
            }
            var pre_data = this.state.data;
            ob['data'][name] = val;
            var n_data = $.extend(pre_data, ob.data);
            console.log( n_data );
            // this.setState( n_data );
        },
        'submit': function(){

            var _this = this;
            $.ajax({
                'url':  _this.state.action,
                'type': _this.props.type,
                'dataType': 'json',
                'data': _this.state.data,
                'success': function(data){
                    if(data.status != 1){
                        alert(data.message);    
                    }
                },
                'error': function(){
                    alert('error');
                }
            })

        },
        'render': function(){
            
            var _this = this;
            var handle_data = function(data, _form){
                _this.props.handle_data(data, _form);
            }

            return <ConcatForm destext="还没有账户？" title="" action="/login/" type="POST" exe={this.props.exe} handle_data={handle_data}  exchange_text="注册" action_text="登录" is_show={this.props.is_show} ipt_items={_this.state.ipt_items}/>;
        }
    });

  //   ReactDOM.render(
  //   <SignForm action="/" type="POST" />,
  //   document.getElementById('J_sign_form')
  // );
    return SignForm;
    // 
})