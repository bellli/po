define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var data_watch = require('appjs/data/data_watch');

    // 
    var MessageForm = React.createClass({
        'getInitialState': function(){
            var ob = {
                'id': 0,
                'send_message': function(){alert('m')},
                'is_focus': false
            };
            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }
            return ob;
        },
        'send_message': function(e){
            var key_code = e.keyCode;

            // if(key_code != 13){
            //     return ;
            // }
            var ipt = e.target || e.srcElement;
            ipt = $(ipt);
            var val = ipt.val();
            if(!val){
                return ;
            }
            if(this.state.send_message && typeof this.state.send_message == 'function'){
                this.state.send_message(this.state.id, val);
            }
            this.clear();
        },
        'clear': function(){
            this.input.value = '';
        },
        'focus': function(){
            var _this = this;
            setTimeout(function(){
                // _this.input.focus();
            }, 50);
            
        },
        'componentDidMount': function(){
            var _this = this;
            if(data_watch){
                data_watch.register('message_form', this.state.id, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }

            // 
            $(_this.input).on('change', function(e){
                _this.send_message(e);
            });
            // 
        },
        'render': function(){
            var _this = this;

            return (
                <div className="msg_ipt_mg">

                    <div className="msg_ipt_mg_inner_tools" >

                        <input className="msg_ipt_tool" ref={function(input){_this.input=input;}}  placeholder="说点什么吧..." />

                        <div className="msg_ipt_toos_ct">

                            <div className="msg_ipt_toos_action " >
                                <div className="mgs_ipt_icon todo_ipt_icon"></div>
                            </div>

                            <div className="msg_ipt_toos_action " >
                                <div className="mgs_ipt_icon file_ipt_icon"></div>
                            </div>
                        </div>

                    </div>

                </div>
            );
        }
    })
   
    return MessageForm;
    // 
})