define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    // 
    var Formitem = require('appjs/forms/InputItem');
    // 
    var SignForm = React.createClass({
        'getInitialState': function(){
            // this.setState({'action': this.props.action});
            var ob = {};
            ob.action = this.props.action;
            var ipt_items = this.props.ipt_items;
            $(ipt_items).each(function(a, b){
                b.key = new Date().getTime() + '' + Math.random();
            });
            ob.title = this.props.title || '';
            ob.ipt_items = ipt_items;
            ob.destext = this.props.destext;
            ob.show_ot = typeof this.props.show_ot == 'boolean' ? this.props.show_ot : true;
            // var data = {};
            $.extend(
                ob,
                {
                    'data': {}
                }
            );

            return ob;

        },
        'show_error': function(text){
            this.setState({
                'is_error': true,
                'error_text': text
            });
        },
        'handle_Change': function(e){
            var el = e.target || e.srcElement;
            el = $(el);

            var val = el.val();
            var name = el.attr('name');

            var ob = {
                'data': {}
            }

            var pre_data = this.state.data;
            ob['data'][name] = val;
            var n_data = $.extend(pre_data, ob.data);
            console.log( n_data );
            // this.setState( n_data );
        },
        'submit': function(e){
            e ? e.preventDefault() : '';
            var handle_data = this.props.handle_data;
            var _this = this;
            $.ajax({
                'url':  _this.state.action,
                'type': _this.props.type,
                'dataType': 'json',
                'data': _this.state.data,
                'success': function(data){
                    if(handle_data && typeof handle_data == 'function'){
                        handle_data(data, _this);    
                    }
                },
                'error': function(){
                    alert('error');
                }
            })
        },
        'render': function(){
            var _this = this;
            var ipt_items = _this.state.ipt_items;
            // ipt_items.reverse();
            var ipt_html = [];
            for (var i =0; i <=  ipt_items.length - 1; i++) {
                // ipt_items[i]
                console.log(ipt_items[i]);
                ipt_items[i]['ipt_change'] = function(e, name, val){
                    _this.handle_Change(e);
                }
                // get state
                var getState = function(state){
                    console.log(state);
                }
                var onEnter = function(){
                    _this.submit();
                }
                var j = <Formitem onEnter={onEnter} getState={getState} data={ ipt_items[i] } key={ ipt_items[i]['key'] } />;
                ipt_html.push(j);
            }

            var class_name = _this.props.is_show ? 'lg_form' : 'lg_form hide';
            var exchange_text = _this.props.exchange_text || '';
            var action_text = _this.props.action_text || '';
            var error_class = _this.state.is_error ? 'show_form_error' : 'show_form_error hide';
            var ot_class_name = _this.state.show_ot ? 'box_form box_form_ot': 'box_form box_form_ot hide'
            return (

                <div className={class_name} formtype="form">
                        <div className="title"> {_this.state.title} </div>
                        <div className={error_class}> {_this.state.error_text} </div>
                    <form action={this.props.action} type={this.props.type} onSubmit={this.submit}>
                        
                        <div className="box_form">
                            {ipt_html}
                            
                            <div className="ipt_item ipt_action_ct">
                                <a onClick={this.submit} className="btn" href="javascript:;">{action_text}</a>
                            </div>
                        </div>
                        
                        <div className={ot_class_name}>
                            <div className="ipt_item ipt_ot_item">
                                <span>{_this.state.destext}</span>
                                <a href="javascript:;" onClick={this.props.exe}>{exchange_text}</a>
                            </div>
                        </div>
                    </form>
                </div>
            );
        }
    });

  //   ReactDOM.render(
  //   <SignForm action="/" type="POST" />,
  //   document.getElementById('J_sign_form')
  // );
    return SignForm;
    // 
})