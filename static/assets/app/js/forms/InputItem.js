define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    // 
    var InputItem = React.createClass({
        'getInitialState': function(){
            var ob = {};
            ob[this.props.data.name] = this.props.value;
            ob['name'] = this.props.data.name;
            ob['value'] = this.props.value;
            return ob;
        },
        'render': function(){
            var _this = this;
            
            var handle_change = function(e){
                var el = e.target || e.srcElement;
                el = $(el);
                var val = el.val();
                var name = el.attr('name');

                var ob = {};
                
                ob[name] = val;
                // _this.setState(ob);
                _this.props.data.ipt_change(e, name, val);

                if(_this.props.getState){
                    _this.props.getState(_this.state);
                }

                if(e.keyCode == 13){
                    
                    if(_this.props.onEnter){
                        // _this.props.onEnter(e);
                    }
                }
                return val;
            }
            return (<div className="ipt_item">
                        <input type={this.props.data.type}  onKeyUp={handle_change} name={this.state.name} className="input"   placeholder={this.props.data.placeholder} />
                    </div>);
        }
    })
   
    return InputItem;
    // 
})