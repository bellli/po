define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var TodoTextArea = require('appjs/pane/to_do_textarea');
    var data_watch = require('appjs/data/data_watch');
    // 
    var ChooseListItem = React.createClass({
        'getInitialState': function(){
            var ob = {
                'id': 0,
                'num': 0,
                'icon': parseInt(Math.random() * 10 / 2) ? 'https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg' : 'https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg',
                'username': 'None',
                'email': 'no email',
                'is_select': false,
                'is_show': true,
                'onSelect': function(){

                },
                'select_ob': {},
                'handle_click': function(id){},
                'update_state': function(){}
            };
            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }
            
            return ob;
        },
        'componentDidMount': function(){
            var _this = this;
            if(data_watch){
                data_watch.register('choose_list_item', this.state.id, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }
        },
        'select': function(){
           
        },
        'close': function(){
            var _this = this;
            _this.setState({
                'is_show': false
            });
        },
        'focus': function(){
            var _this = this;
            var list = _this.list;
            $(list).show();
            // console.log('show');
        },
        'out': function(){
            var _this = this;
            var list = _this.list;
            // $(list).hide();
            // console.log('show');
        },
        'choose': function(){
            var _this = this;
            var is_select = _this.state.is_select ? false : true;

            _this.setState({
                'is_select': is_select
            });
            if(_this.state.onSelect){
                _this.state.onSelect(_this, is_select);
            }
        },
        'render': function(){
            var _this = this;
            
            var to_do_cont = {
                'placeholder': '任务描述'
            }
            var class_name = _this.state.is_select ? 'user_item user_item_focus duser_item': 'user_item  duser_item';
            return (

                        <div className={class_name} onClick={_this.choose}>
                            <div className="user_item_ricon">
                                <div className="user_item_icon_ct">
                                    <img src="https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg" />
                                </div>
                            </div>
                            <div className="user_item_inner">
                                <div className="us_sel_item_name">{this.state.username}</div>
                                <div className="us_sel_item_email">{this.state.email}</div>
                            </div>
                            <div className="item_radio_circle">
                                <div className="item_radio_circle_pointer">
                                </div>
                            </div>
                        </div>
            );
        }
    })
   
    return ChooseListItem;
    // 
})