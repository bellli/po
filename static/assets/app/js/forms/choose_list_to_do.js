define(function(require, exports){
    require('jquery');
    require('gallery/react/react');
    var TodoTextArea = require('appjs/pane/to_do_textarea');
    var data_watch = require('appjs/data/data_watch');
    var ChooseListItem = require('appjs/forms/choose_list_item');
    // 
    var ChooseListToDo = React.createClass({
        'getInitialState': function(){
            var ob = {
                'id': 0,
                'num': 0,
                'icon': parseInt(Math.random() * 10 / 2) ? 'https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg' : 'https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg',
                'name': 'None',
                'is_show': true,
                'user_list': [{}, {}, {}, {}],
                'seleted_emails': {},
                'key': new Date().getTime() + '*' + Math.random() + '*' + Math.random()
            };
            var data = this.props.data || {};
            for(var item in ob){
                ob[item] = data[item] || ob[item];
            }
            // 
            return ob;
        },
        'componentDidMount': function(){
            var _this = this;
            if(data_watch){
                data_watch.register('choose_list_to_do', this.state.id, 'change', function(data, fn, fn_name){
                    
                    if(fn && typeof fn == 'function'){
                        fn( _this, data);
                    }

                    if(_this[fn_name] && typeof _this[fn_name] == 'function'){
                        _this[fn_name]();
                    }

                });
            }
            // 
            var id = _this.state.id;            
            // 
            $(document).on('click', function(e){
                var t = e.target || e.srcElement;
                if($.contains(_this.area, t)){
                    return ;
                }else if(t === _this.area){
                    _this.toggle();
                }else{
                    _this.out();
                }
            });
            
        },
        'select': function(){
            var _this = this;
            var is_select = _this.state.select ? false : true;
            _this.setState({
                'select': is_select
            });
            setTimeout(function(){
                _this.setState({
                    'is_show': false
                });
            }, 500);
        },
        'close': function(){
            var _this = this;
            _this.setState({
                'is_show': false
            });
        },
        'focus': function(){
            var _this = this;
            var list = _this.list;
            _this.setState({
                'is_show_list': true
            });
            _this.input.focus();
            _this.get_user_data();
        },
        'get_user_data': function(){
            var _this = this;
            var emails = _this.state.seleted_emails;
            // 
            data_watch.callback('channel_users', this.state.id, 'change', {}, function(ob){
                var new_list = [];
                $(ob.state.channel_users).each(function(a, b){
                    var new_ob = $.extend({}, b);
                    new_ob.is_select = emails[new_ob.email] ? true : false;
                    new_list.push(new_ob);
                });
                _this.setState({
                    'user_list': new_list
                });
            });

        },
        'out': function(e){
            var _this = this;
            var list = _this.list;

            _this.setState({
                'is_show_list': false
            });

        },
        'toggle': function(){
            var _this = this;
            var is_show_list = _this.state.is_show_list ? false : true;
            _this.setState({
                'is_show_list': is_show_list
            });
        },
        'toggle_select_email': function(email, is_select){
            var _this = this;
            var emails = _this.state.seleted_emails;
            emails[email] = is_select;
            console.log(emails);
            _this.setState({
                'seleted_emails': emails
            });
        },
        'render': function(){
            var _this = this;
            
            var to_do_cont = {
                'placeholder': '任务描述'
            }
            var cl_items = [];

            $(_this.state.user_list).each(function(a, b){
                var t = new Date().getTime() + '' + Math.random();
                b.key = b.key || t;
                b.onSelect = function(ob, is_select){
                    _this.focus();
                    _this.toggle_select_email(ob.state.email, is_select);
                }
                cl_items.push(<ChooseListItem key={b.key} data={b} />);
            });
            
            var is_show_list = this.state.is_show_list ? 'down_ai_select': 'down_ai_select hide';
            return (
                <div className="atd_item" ref={
                    function(area){
                        _this.area = area;
                    }
                }  >
                    <div className="atd_item_icon"></div>
                    <div className="atd_val_ct">
                        <textarea onFocus={_this.focus} ref={function(input){_this.input = input;}}  className="input_text_area_normal" placeholder="分配给"></textarea>
                    </div>
                    
                    <div onClick={_this.list_show} className={is_show_list} ref={
                        function(list){
                            _this.list = list;
                        }
                    }>

                    {cl_items}

                    </div>

                </div>
            );
        }
    })
   
    return ChooseListToDo;
    // 
})