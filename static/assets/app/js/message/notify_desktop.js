define(function(require, exports){
    // var notification = new Notification('Notification title', {
    //   icon: 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
    //   body: "Hey there! You've been notified!",
    // });

    document.addEventListener('DOMContentLoaded', function () {
      if (!Notification) {
        return;
      }

        if (Notification.permission !== "granted"){
            Notification.requestPermission();
        }
    });
    var notify = function(title, data){
        var notification = new Notification(title, data);
        notification.onclick = function(){
            window.open('http://127.0.0.1:8000/show/');
        }
    }

    return notify;
})