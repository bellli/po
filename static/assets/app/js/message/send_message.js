define(function(require, exports){
    var send_message = function(res, on_message){
        var me = arguments.callee;
        var open = me.open || false;

        if(!me.ws || me.ws.readyState == 3 || me.ws.readyState == 2){
            me.ws = new WebSocket("ws://127.0.0.1:8001/chatsocket");
            // me.ws = new WebSocket("ws://us3.polarins.xyz:8001/chatsocket");
            me.ws.onmessage = function (evt) {
               console.log(evt.data);
               if(on_message && typeof on_message == 'function'){
                    on_message( JSON.parse(evt.data) );
               }
            };
            me.open = false;
            me.ws.onopen = function(){
               if(me.ws.readyState == 1){
                    me.open = true;
                }
            }
        }

        if(me.open){
            me.ws.send(JSON.stringify(res));
        }else{
            setTimeout(function(){
                me(res);
            }, 1000);
        }
        console.log(me.ws.readyState);

        return me.ws;
    }
    return send_message;
})