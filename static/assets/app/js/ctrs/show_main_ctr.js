define(function(require, exports){
    var SignForm = require('appjs/forms/sign_form.js');
    var LoginForm = require('appjs/forms/login_form.js');
    var LoginSignForm = require('appjs/forms/LoginSignForm');
    var send_message = require('appjs/message/send_message');
    var MainPane = require('appjs/pane/main_pane');
    var GroupForm = require('appjs/forms/group_form');
    
    var groups = [];
    //[ create channel start ]
    var handle_data = function(data, _form, _this){
        if(data.status == 1){
            var list = [];
            $(data.groups).each(function(a, b){
                list.push({
                    'title': b.name,
                    'id': b.id,
                    'is_alive': b.is_alive
                });
            });
            list.reverse();
            _this.setList(list);
            groups = list;
        }else{
            alert(data.message);
        }
    }
    // 
    var handle_click = function(e, id, _this){
        $.ajax({
            'url': '/alive_group/',
            'type': 'POST',
            'dataType': 'json',
            'data': {
                'group_id': id
            },
            'success': function(data){
                $(groups).each(function(a, b){
                    b.is_alive = 0;
                    if(b.id == id){
                        b.is_alive = 1;
                    }
                });
                _this.setList(groups);
            },
            'error': function(){

            }
        })
    }
    var Box = require('appjs/pane/box');
    var InviteBox = require('appjs/pane/invite_box');
    

    // 
    // var success = function(){
    //     show_main();
    // }
    // ReactDOM.render(
    //     <LoginSignForm show_sign={true} success={success} show_sign={false} hide_all={false} />,
    //     document.getElementById('J_main')
    // );
    // 

    var data_watch = require('appjs/data/data_watch');
    var Info = require('appjs/pane/info');
    var top_index = 100;
    // 'add_user_to_channel_box', 1, 'change',
    var box_init_config = {
        'is_show': false
    }
    ReactDOM.render(
            <Box data={box_init_config} />,
            document.getElementById('J_add_user_to_channel')
    )

    ReactDOM.render(
            <InviteBox data={box_init_config} />,
            document.getElementById('J_add_channel_box')
    );

    var info_init_data = {
        'is_show': false,
        'info_text': 'hello'
    };
    ReactDOM.render(
            <Info data={info_init_data} />,
            document.getElementById('J_info')
    )

    // data_watch.callback('add_user_to_channel_box', 1, 'change', {}, function(a, b){
    //     a.setState({
    //         'is_show': false
    //     });
    // });

    data_watch.register('add_team_pane', 1, 'hide', function(){
        $('#J_group_form').hide();
    });
    data_watch.register('add_team_pane', 1, 'show', function(){
        seajs.use('appjs/ctrs/team');
        $('#J_group_form').show();
        $('#J_group_form').css({
            'zIndex': top_index ++
        });
    });
    data_watch.register('login_sign_pane', 1, 'hide', function(){
        $('#J_sign_form').hide();

    });
    data_watch.register('login_sign_pane', 1, 'show', function(){
        $('#J_sign_form').show();
        $('#J_sign_form').css({
            'zIndex': top_index ++
        });
    });

    data_watch.register('group_form', 1, 'show', function(){
        $('#J_group_form').show();
        $('#J_group_form').css({
            'zIndex': top_index ++
        });
    });
    data_watch.register('group_form', 1, 'hide', function(){
        $('#J_group_form').hide();
    });
    data_watch.register('J_add_channel', 1, 'show', function(){
        $('#J_add_channel').show();
        $('#J_add_channel').css({
            'zIndex': top_index ++
        });
    });
    data_watch.register('J_add_channel', 1, 'hide', function(){
        $('#J_add_channel').hide();
    });
    // data_watch.callback('J_add_channel', 1, 'show');
    // show_main
    seajs.use('appjs/ctrs/login_sign_ctr');
    seajs.use('appjs/ctrs/team');
    seajs.use('appjs/ctrs/create_channel_ctr');

    data_watch.register('channel_form_result', 1, 'change', function(data){

        if(data.status != 1){

        }else{
            alert('添加成功');
            // 
            
            
            // 
        }
    });

    var user_item_data_watch = {
        'data': {

        },
        'set': function(val, data){

        },
        'register':function(change_name, fn){

        }
        
    }
    var show_login = function(){
        var success = function(){
            show_main();
        }
        ReactDOM.render(
            <LoginSignForm show_sign={true} success={success} show_sign={false} hide_all={false} />,
            document.getElementById('J_sign_form')
        );
    }
    // 
    var alive_group_action = require('appjs/ctrs/alive_group');
    data_watch.on_register('left_pane_team_item', 'change', function(a, b, c){
        data_watch.register('left_pane_team_item', b, 'click', function(b){
            // 
            alive_group_action(b.state.id, function(){
                window.location.reload();
            })
            // 
        });
    });
    // 
    var hide_login = function(){
        ReactDOM.render(
            <LoginSignForm show_sign={true} show_sign={false} hide_all={true} />,
            document.getElementById('J_sign_form')
        );
    }

    var globle_data = {
        'chat_ws': {}
    }
    var g_update_state = function(channel_item, user_items, left_pane, main_pane){
        console.log(channel_item);
    }
    var notify = require('appjs/message/notify_desktop');
    var on_ws_message = function(data){
        console.log(data);
        var action = data.action;
        if(action == 'message'){
            globle_data.chat_ws[data.data.channel].add_message({
                'message': data.data.message,
                'time': data.data.send_time,
                'username': data.data.from.username
            });
            globle_data.chat_ws[data.data.channel].to_bottom();
            notify(data.data.from.username, {
                'icon': 'https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg',
                'body': data.data.message
            });
        }
        // 
        if(action == 'channel_base'){
            data_watch.callback('channel_item', data.data.channel_id, 'change', {}, function(ob){
                if(ob.state.is_focus){
                    clear_channel_message_num(ob.state.id);
                }else{
                    ob.setState({'num': data.data.channel_num});
                }
            });
        }
        // 
        if(action == 'add_group'){
            var group_item = data.data;
            data_watch.callback('left_down_groups', 1, 'change', {}, function(ob){
                ob.add_group(group_item);
                // group_item.key = new Date().getTime() + Math.random();
                // ob.state.groups.push(group_item);
                // ob.setState({'groups': ob.state.groups})
            });
        }
        if(action == 'update_channel_user_list'){
            data_watch.callback('right_pane', data.data.channel , 'change', {}, function(ob){
                    console.log(data.data.channel_users);
                    ob.set_channel_users(data.data.channel_users);
            });
        }
        //add_channel
        if(action == 'add_channel'){
            if(!data.data.channel.id){
                return ;
            }
            var new_channel_item = {
                'id': data.data.channel.id,
                'num': data.data.channel.num,
                'icon': 'https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg',
                'name': data.data.channel.name,
                'is_focus': false,
                'handle_click': function(id){},
                'update_state': function(){}
            };
            data_watch.callback('channel_item_items', 1, 'change', {}, function(ob){
                channel_list = ob.state.channel_list;
                // channel_list.push(new_channel_item);
                // ob.setState({
                //     'channel_list': channel_list
                // });
            });
            // 
            var new_chat_data = {
                'icon': 'https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg',
                'name': data.data.channel.name,
                'id': data.data.channel.id,
                'send_message': function(){alert('r')},
                'after_mounted': function(a){
                    return _this;
                },
                'is_show': false,
                'brief': data.data.channel.brief,
                'message_data': [{}],
                'message_data': [],
                'channel_users': data.data.channel.user_list,
                'key': new Date().getTime() + Math.random() 
            };
            
            new_chat_data.send_message = function(a, b, chat_pane){

                send_message({
                    'action': 'send_message_to_channel',
                    'data': {
                        'channel': a,
                        'message': b
                    }
                }, function(data){
                    on_ws_message(data);
                });

            }
            data_watch.callback('chat_pane', 1, 'change', {}, function(ob){
                // channel_list = ob.state.channel_list;
                // channel_list.push(new_channel_item);
                var channel_list = ob.state.left_pane_data.UserItem.channel_list;
                channel_list.push(new_chat_data);
                ob.setState({
                    'left_pane_data': ob.state.left_pane_data
                });
                
                // alert(channel_list.length);
            });
            data_watch.callback('inited_all_item', 1, 'change');
            // 
        }
        // 
    }
    // 
    var focus_out_other_channel = function(channel_items, ob){
        for(var item in channel_items){
            if(ob.state.id != item){
                data_watch.callback('channel_item', item, 'change', {}, function(ob){
                    ob.focus_out();
                });
            }
        }
    }

    var show_chat_pane = function(id){
        
        var channel_items = data_watch.data_fn['right_pane'];
        for(var item in channel_items){
            if(id != item){
                data_watch.callback('right_pane', item, 'change', {}, function(ob){
                    ob.setState({'is_show': false});
                });
            }else{
                data_watch.callback('right_pane', item, 'change', {}, function(ob){
                    ob.show();

                    data_watch.callback('message_form', item, 'change', {}, function(m_f){
                        m_f.focus();
                    });

                });
            }
        }
    }
    // 
    var clear_channel_message_num = function(id){
        send_message({'action': 'clear_message_num', data: {'channel': id}}, function(data){
                    on_ws_message(data);
        });
    }
    // 
    // 
    data_watch.register('inited_all_item', 1, 'change', function(){
        var channel_items = data_watch.data_fn['channel_item'];
        for(var item in channel_items){
            data_watch.register('channel_item', item, 'click', function(ob){
                ob.focus();
                focus_out_other_channel(channel_items, ob);
                ob.setState({
                    'num': 0
                });
                show_chat_pane(ob.state.id);
                clear_channel_message_num(ob.state.id);
            });
        }
    });
    // 
    send_message({}, function(data){
        on_ws_message(data);
    });
    var show_main = function(){
        var main_el = $('#J_main');
        
        get_channels(function(res){
            var data = {};
            
            var left_pane_data = {
                'UserItem': {
                    'channel_name': '频道',
                    'channel_num': '3',
                    'channel_list': res['res']['channel_list'],
                    'channel_select': function(id){
                        for(var item in globle_data['chat_ws']){
                            globle_data['chat_ws'][item].check_to_show(id);
                        }
                    }
                }
            }

            if( !res['groups'] || !res['res']['channel_list'][0]){
                data_watch.callback('group_form', 1, 'show');
                return ;
            }
            left_pane_data['groups'] = res['groups'];
            data['left_pane_data'] = left_pane_data;

            data['after_mounted'] = function(a, b){
                globle_data.chat_ws[a.state.id] = a;
            }
            data['update_state'] = function(channel_item, user_items, left_pane, main_pane){
                g_update_state(channel_item, user_items, left_pane, main_pane);
            }
            $(left_pane_data.UserItem.channel_list).each(function(a, b){
                b.send_message = function(a, b, chat_pane){

                    send_message({
                        'action': 'send_message_to_channel',
                        'data': {
                            'channel': a,
                            'message': b
                        }
                    }, function(data){
                        on_ws_message(data);
                    });

                }
            });
            res['res']['channel_list'][0]['is_show'] = true;
            res['res']['channel_list'][0]['is_focus'] = true;
            res['res']['channel_list'][0]['num'] = 0;
            ReactDOM.render(
                    <MainPane main_el={main_el} data={data} hide={false} />,
                    document.getElementById('J_main')
            );
            
            data_watch.callback('inited_all_item', 1, 'change');
            clear_channel_message_num(res['res']['channel_list'][0]['id']);
        });
        
    }

    var hide_main = function(){
        var main_el = $('#J_main');
        ReactDOM.render(
                <MainPane main_el={main_el} hide={true} />,
                document.getElementById('J_main')
        );
    }
    
    data_watch.register('login_sign_result', 1, 'data', function(data){
        if(data.status != 1){
            data_watch.callback('login_sign_pane', 1, 'show');
            send_message.ws = null;
        }else{
            data_watch.callback('login_sign_pane', 1, 'hide');
            show_main();
        }
    });

    var is_login = function(){
        $.ajax({
            'url': '/is_login/',
            'dataType': 'json',
            'success': function(data){
                data_watch.callback('login_sign_result', 1, 'data', data);
            }
        })
    }
    
    is_login();

    var get_channels = function(after_fn){
        $.ajax({
            'url': '/get_channels/',
            'type': 'POST',
            'dataType': 'json',
            'success': function(data){
                if(after_fn){
                    after_fn(data);
                }
            },
            'error': function(){

            }
        })
    }
    // get_channels();
    
    // var get_groups = function(){
        // $.ajax({
        //     'url': '/get_my_groups/',
        //     'type': 'POST',
        //     'dataType': 'json',
        //     'success': function(data){
        //         var list = [];
        //         $(data.groups).each(function(a, b){
        //             list.push({
        //                 'title': b.name,
        //                 'id': b.id,
        //                 'is_alive': b.is_alive
        //             });
        //         });
        //         groups = list;
        //         ReactDOM.render(
        //                 <GroupForm list={list}  is_show={true} title="创建你的团队" handle_data={handle_data} handle_click={handle_click} exchange_text="" action_text="创建" />,
        //                 document.getElementById('J_main')
        //         );
        //     },
        //     'error': function(){

        //     }
        // });
    // }
    // get_groups();
    // 
})