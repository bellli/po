define(function(require, exports){
    var SignForm = require('appjs/forms/sign_form.js');
    var LoginForm = require('appjs/forms/login_form.js');
    var LoginSignForm = require('appjs/forms/LoginSignForm');
    var send_message = require('appjs/message/send_message');
    var MainPane = require('appjs/pane/main_pane');
    var GroupForm = require('appjs/forms/group_form');

    var groups = [];
    var data_watch = require('appjs/data/data_watch');
    
    var handle_data = function(data, _form, _this){
        if(data.status == 1){
            var list = [];
            $(data.groups).each(function(a, b){
                list.push({
                    'title': b.name,
                    'id': b.id,
                    'is_alive': b.is_alive
                });
            });
            list.reverse();
            _this.setList(list);
            groups = list;
        }else{
            alert(data.message);
        }
    }
    var handle_click = function(e, id, _this){
        $.ajax({
            'url': '/alive_group/',
            'type': 'POST',
            'dataType': 'json',
            'data': {
                'group_id': id
            },
            'success': function(data){
                $(groups).each(function(a, b){
                    b.is_alive = 0;
                    if(b.id == id){
                        b.is_alive = 1;
                    }
                });
                _this.setList(groups);
            },
            'error': function(){

            }
        })
    }

    ReactDOM.render(
            <GroupForm  is_show={true} title="创建你的团队" handle_data={handle_data} handle_click={handle_click} exchange_text="" action_text="创建" />,
            document.getElementById('J_group_form')
    );

    // 
    // var success = function(){
    //     show_main();
    // }
    // ReactDOM.render(
    //     <LoginSignForm show_sign={true} success={success} show_sign={false} hide_all={false} />,
    //     document.getElementById('J_main')
    // );
    // 
    var show_login = function(){
        var success = function(){
            show_main();
        }
        ReactDOM.render(
            <LoginSignForm show_sign={true} success={success} show_sign={false} hide_all={false} />,
            document.getElementById('J_sign_form')
        );
    }

    var hide_login = function(){
        ReactDOM.render(
            <LoginSignForm show_sign={true} show_sign={false} hide_all={true} />,
            document.getElementById('J_group_form')
        );
    }

    var show_main = function(){
        var main_el = $('#J_main');
        ReactDOM.render(
                <MainPane main_el={main_el} hide={false} />,
                document.getElementById('J_main')
        );
    }

    var hide_main = function(){
        var main_el = $('#J_main');
        ReactDOM.render(
                <MainPane main_el={main_el} hide={true} />,
                document.getElementById('J_main')
        );
    }

    var is_login = function(){
        $.ajax({
            'url': '/is_login/',
            'dataType': 'json',
            'success': function(data){
                if(data.status != 1){
                    show_login();
                    hide_main();
                }else{
                    hide_login();
                    show_main();
                }
            }
        })
    }
    
    // is_login();

    var get_channels = function(){
        $.ajax({
            'url': '/get_channels/',
            'type': 'POST',
            'dataType': 'json',
            'success': function(){

            },
            'error': function(){

            }
        })
    }
    // get_channels();
    
    var get_groups = function(){
        $.ajax({
            'url': '/get_my_groups/',
            'type': 'POST',
            'dataType': 'json',
            'success': function(data){
                var list = [];
                $(data.groups).each(function(a, b){
                    list.push({
                        'title': b.name,
                        'id': b.id,
                        'is_alive': b.is_alive
                    });
                });
                groups = list;
                ReactDOM.render(
                        <GroupForm list={list}  is_show={true} title="创建你的团队" handle_data={handle_data} handle_click={handle_click} exchange_text="" action_text="创建" />,
                        document.getElementById('J_group_form')
                );
            },
            'error': function(){

            }
        })
    }
    get_groups();
    // 
})