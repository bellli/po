define(function(require, exports){
    var SignForm = require('appjs/forms/sign_form.js');
    var LoginForm = require('appjs/forms/login_form.js');
    var LoginSignForm = require('appjs/forms/LoginSignForm');
    var send_message = require('appjs/message/send_message');
    var MainPane = require('appjs/pane/main_pane');
    var GroupForm = require('appjs/forms/group_form');

    var groups = [];
    var data_watch = require('appjs/data/data_watch');
    
    var handle_data = function(data, _form, _this){
        if(data.status == 1){
            var list = [];
            $(data.groups).each(function(a, b){
                list.push({
                    'title': b.name,
                    'id': b.id,
                    'is_alive': b.is_alive
                });
            });
            list.reverse();
            _this.setList(list);
            groups = list;
        }else{
            alert(data.message);
        }
    }
    var handle_click = function(id, success_fn){
        $.ajax({
            'url': '/alive_group/',
            'type': 'POST',
            'dataType': 'json',
            'data': {
                'group_id': id
            },
            'success': function(data){
                if(success_fn && typeof success_fn == 'function'){
                    success_fn();
                }
            },
            'error': function(){

            }
        })
    }

    return handle_click;

})