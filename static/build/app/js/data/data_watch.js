define("app/js/data/data_watch", [], function(require, exports) {
    var a = {
        data_fn: {
            key_name: {
                id: function() {}
            },
            on_register_key_name: {}
        },
        callback: function(a, n, t, _, e) {
            var f = this, r = a;
            f.data_fn[r] && f.data_fn[r][n] && f.data_fn[r][n][t] && f.data_fn[r][n][t](_, e, r);
        },
        on_register: function(a, n, t) {
            var _ = a + n;
            this.data_fn.on_register_key_name[_] = t;
        },
        register: function(a, n, t, _) {
            var e = this;
            e.data_fn[a] = e.data_fn[a] || {}, e.data_fn[a][n] = e.data_fn[a][n] || {}, e.data_fn[a][n][t] = _, 
            e.data_fn.on_register_key_name[a + t] && e.data_fn.on_register_key_name[a + t](a, n, t);
        }
    };
    return a;
});