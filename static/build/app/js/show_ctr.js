define("app/js/show_ctr", [ "jquery", "gallery/react/react" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = 12;
    !function() {
        var t = window.screen.width < document.body.offsetWidth ? window.screen.width : document.body.offsetWidth, n = t / 375 * 12;
        $("html").css({
            fontSize: n + "px"
        }), e = t / 375 * 12;
    }();
});