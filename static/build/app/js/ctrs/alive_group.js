define("app/js/ctrs/alive_group", [ "appjs/forms/sign_form.js", "appjs/forms/login_form.js", "appjs/forms/LoginSignForm", "appjs/message/send_message", "appjs/pane/main_pane", "appjs/forms/group_form", "appjs/data/data_watch" ], function(require, exports) {
    var a = (require("appjs/forms/sign_form.js"), require("appjs/forms/login_form.js"), 
    require("appjs/forms/LoginSignForm"), require("appjs/message/send_message"), require("appjs/pane/main_pane"), 
    require("appjs/forms/group_form"), require("appjs/data/data_watch"), function(a, s) {
        $.ajax({
            url: "/alive_group/",
            type: "POST",
            dataType: "json",
            data: {
                group_id: a
            },
            success: function(a) {
                s && "function" == typeof s && s();
            },
            error: function() {}
        });
    });
    return a;
});