define("app/js/ctrs/create_channel_ctr", [ "appjs/forms/sign_form.js", "appjs/forms/login_form.js", "appjs/forms/LoginSignForm", "appjs/message/send_message", "appjs/pane/main_pane", "appjs/forms/channel_form" ], function(require, exports) {
    var e = (require("appjs/forms/sign_form.js"), require("appjs/forms/login_form.js"), 
    require("appjs/forms/LoginSignForm"), require("appjs/message/send_message"), require("appjs/pane/main_pane"), 
    require("appjs/forms/channel_form")), a = [], s = function(e, s, n) {
        if (1 == e.status) {
            var i = [];
            $(e.groups).each(function(e, a) {
                i.push({
                    title: a.name,
                    id: a.id,
                    is_alive: a.is_alive
                });
            }), i.reverse(), n.setList(i), a = i;
        } else alert(e.message);
    }, n = function(e, s, n) {
        $.ajax({
            url: "/alive_group/",
            type: "POST",
            dataType: "json",
            data: {
                group_id: s
            },
            success: function(e) {
                $(a).each(function(e, a) {
                    a.is_alive = 0, a.id == s && (a.is_alive = 1);
                }), n.setList(a);
            },
            error: function() {}
        });
    }, i = function() {
        $.ajax({
            url: "/get_channels/",
            type: "POST",
            dataType: "json",
            success: function(i) {
                var t = [];
                $(i.groups).each(function(e, a) {
                    t.push({
                        title: a.name,
                        id: a.id,
                        is_alive: a.is_alive
                    });
                }), a = t, ReactDOM.render(React.createElement(e, {
                    list: t,
                    is_show: !0,
                    title: "创建你频道",
                    handle_data: s,
                    handle_click: n,
                    exchange_text: "",
                    action_text: "创建"
                }), document.getElementById("J_add_channel"));
            },
            error: function() {}
        });
    };
    i();
});