define("app/js/ctrs/show_main_ctr", [ "appjs/forms/sign_form.js", "appjs/forms/login_form.js", "appjs/forms/LoginSignForm", "appjs/message/send_message", "appjs/pane/main_pane", "appjs/forms/group_form", "appjs/pane/box", "appjs/pane/invite_box", "appjs/data/data_watch", "appjs/pane/info", "appjs/ctrs/alive_group", "appjs/message/notify_desktop" ], function(require, exports) {
    var e = (require("appjs/forms/sign_form.js"), require("appjs/forms/login_form.js"), 
    require("appjs/forms/LoginSignForm"), require("appjs/message/send_message")), n = require("appjs/pane/main_pane"), a = (require("appjs/forms/group_form"), 
    require("appjs/pane/box")), t = require("appjs/pane/invite_box"), s = require("appjs/data/data_watch"), c = require("appjs/pane/info"), i = 100, o = {
        is_show: !1
    };
    ReactDOM.render(React.createElement(a, {
        data: o
    }), document.getElementById("J_add_user_to_channel")), ReactDOM.render(React.createElement(t, {
        data: o
    }), document.getElementById("J_add_channel_box"));
    var _ = {
        is_show: !1,
        info_text: "hello"
    };
    ReactDOM.render(React.createElement(c, {
        data: _
    }), document.getElementById("J_info")), s.register("add_team_pane", 1, "hide", function() {
        $("#J_group_form").hide();
    }), s.register("add_team_pane", 1, "show", function() {
        seajs.use("appjs/ctrs/team"), $("#J_group_form").show(), $("#J_group_form").css({
            zIndex: i++
        });
    }), s.register("login_sign_pane", 1, "hide", function() {
        $("#J_sign_form").hide();
    }), s.register("login_sign_pane", 1, "show", function() {
        $("#J_sign_form").show(), $("#J_sign_form").css({
            zIndex: i++
        });
    }), s.register("group_form", 1, "show", function() {
        $("#J_group_form").show(), $("#J_group_form").css({
            zIndex: i++
        });
    }), s.register("group_form", 1, "hide", function() {
        $("#J_group_form").hide();
    }), s.register("J_add_channel", 1, "show", function() {
        $("#J_add_channel").show(), $("#J_add_channel").css({
            zIndex: i++
        });
    }), s.register("J_add_channel", 1, "hide", function() {
        $("#J_add_channel").hide();
    }), seajs.use("appjs/ctrs/login_sign_ctr"), seajs.use("appjs/ctrs/team"), seajs.use("appjs/ctrs/create_channel_ctr"), 
    s.register("channel_form_result", 1, "change", function(e) {
        1 != e.status || alert("添加成功");
    });
    var r = require("appjs/ctrs/alive_group");
    s.on_register("left_pane_team_item", "change", function(e, n, a) {
        s.register("left_pane_team_item", n, "click", function(e) {
            r(e.state.id, function() {
                window.location.reload();
            });
        });
    });
    var l = {
        chat_ws: {}
    }, d = function(e, n, a, t) {
        console.log(e);
    }, h = require("appjs/message/notify_desktop"), u = function(n) {
        console.log(n);
        var a = n.action;
        if ("message" == a && (l.chat_ws[n.data.channel].add_message({
            message: n.data.message,
            time: n.data.send_time,
            username: n.data.from.username
        }), l.chat_ws[n.data.channel].to_bottom(), h(n.data.from.username, {
            icon: "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg",
            body: n.data.message
        })), "channel_base" == a && s.callback("channel_item", n.data.channel_id, "change", {}, function(e) {
            e.state.is_focus ? m(e.state.id) : e.setState({
                num: n.data.channel_num
            });
        }), "add_group" == a) {
            var t = n.data;
            s.callback("left_down_groups", 1, "change", {}, function(e) {
                e.add_group(t);
            });
        }
        if ("update_channel_user_list" == a && s.callback("right_pane", n.data.channel, "change", {}, function(e) {
            console.log(n.data.channel_users), e.set_channel_users(n.data.channel_users);
        }), "add_channel" == a) {
            if (!n.data.channel.id) return;
            ({
                id: n.data.channel.id,
                num: n.data.channel.num,
                icon: "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg",
                name: n.data.channel.name,
                is_focus: !1,
                handle_click: function(e) {},
                update_state: function() {}
            });
            s.callback("channel_item_items", 1, "change", {}, function(e) {
                channel_list = e.state.channel_list;
            });
            var c = {
                icon: "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg",
                name: n.data.channel.name,
                id: n.data.channel.id,
                send_message: function() {
                    alert("r");
                },
                after_mounted: function(e) {
                    return _this;
                },
                is_show: !1,
                brief: n.data.channel.brief,
                message_data: [ {} ],
                message_data: [],
                channel_users: n.data.channel.user_list,
                key: new Date().getTime() + Math.random()
            };
            c.send_message = function(n, a, t) {
                e({
                    action: "send_message_to_channel",
                    data: {
                        channel: n,
                        message: a
                    }
                }, function(e) {
                    u(e);
                });
            }, s.callback("chat_pane", 1, "change", {}, function(e) {
                var n = e.state.left_pane_data.UserItem.channel_list;
                n.push(c), e.setState({
                    left_pane_data: e.state.left_pane_data
                });
            }), s.callback("inited_all_item", 1, "change");
        }
    }, f = function(e, n) {
        for (var a in e) n.state.id != a && s.callback("channel_item", a, "change", {}, function(e) {
            e.focus_out();
        });
    }, g = function(e) {
        var n = s.data_fn.right_pane;
        for (var a in n) e != a ? s.callback("right_pane", a, "change", {}, function(e) {
            e.setState({
                is_show: !1
            });
        }) : s.callback("right_pane", a, "change", {}, function(e) {
            e.show(), s.callback("message_form", a, "change", {}, function(e) {
                e.focus();
            });
        });
    }, m = function(n) {
        e({
            action: "clear_message_num",
            data: {
                channel: n
            }
        }, function(e) {
            u(e);
        });
    };
    s.register("inited_all_item", 1, "change", function() {
        var e = s.data_fn.channel_item;
        for (var n in e) s.register("channel_item", n, "click", function(n) {
            n.focus(), f(e, n), n.setState({
                num: 0
            }), g(n.state.id), m(n.state.id);
        });
    }), e({}, function(e) {
        u(e);
    });
    var p = function() {
        var a = $("#J_main");
        v(function(t) {
            var c = {}, i = {
                UserItem: {
                    channel_name: "频道",
                    channel_num: "3",
                    channel_list: t.res.channel_list,
                    channel_select: function(e) {
                        for (var n in l.chat_ws) l.chat_ws[n].check_to_show(e);
                    }
                }
            };
            return t.groups && t.res.channel_list[0] ? (i.groups = t.groups, c.left_pane_data = i, 
            c.after_mounted = function(e, n) {
                l.chat_ws[e.state.id] = e;
            }, c.update_state = function(e, n, a, t) {
                d(e, n, a, t);
            }, $(i.UserItem.channel_list).each(function(n, a) {
                a.send_message = function(n, a, t) {
                    e({
                        action: "send_message_to_channel",
                        data: {
                            channel: n,
                            message: a
                        }
                    }, function(e) {
                        u(e);
                    });
                };
            }), t.res.channel_list[0].is_show = !0, t.res.channel_list[0].is_focus = !0, t.res.channel_list[0].num = 0, 
            ReactDOM.render(React.createElement(n, {
                main_el: a,
                data: c,
                hide: !1
            }), document.getElementById("J_main")), s.callback("inited_all_item", 1, "change"), 
            void m(t.res.channel_list[0].id)) : void s.callback("group_form", 1, "show");
        });
    };
    s.register("login_sign_result", 1, "data", function(n) {
        1 != n.status ? (s.callback("login_sign_pane", 1, "show"), e.ws = null) : (s.callback("login_sign_pane", 1, "hide"), 
        p());
    });
    var w = function() {
        $.ajax({
            url: "/is_login/",
            dataType: "json",
            success: function(e) {
                s.callback("login_sign_result", 1, "data", e);
            }
        });
    };
    w();
    var v = function(e) {
        $.ajax({
            url: "/get_channels/",
            type: "POST",
            dataType: "json",
            success: function(n) {
                e && e(n);
            },
            error: function() {}
        });
    };
});