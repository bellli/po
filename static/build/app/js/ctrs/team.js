define("app/js/ctrs/team", [ "appjs/forms/sign_form.js", "appjs/forms/login_form.js", "appjs/forms/LoginSignForm", "appjs/message/send_message", "appjs/pane/main_pane", "appjs/forms/group_form", "appjs/data/data_watch" ], function(require, exports) {
    var e = (require("appjs/forms/sign_form.js"), require("appjs/forms/login_form.js"), 
    require("appjs/forms/LoginSignForm"), require("appjs/message/send_message"), require("appjs/pane/main_pane"), 
    require("appjs/forms/group_form")), a = [], t = (require("appjs/data/data_watch"), 
    function(e, t, s) {
        if (1 == e.status) {
            var n = [];
            $(e.groups).each(function(e, a) {
                n.push({
                    title: a.name,
                    id: a.id,
                    is_alive: a.is_alive
                });
            }), n.reverse(), s.setList(n), a = n;
        } else alert(e.message);
    }), s = function(e, t, s) {
        $.ajax({
            url: "/alive_group/",
            type: "POST",
            dataType: "json",
            data: {
                group_id: t
            },
            success: function(e) {
                $(a).each(function(e, a) {
                    a.is_alive = 0, a.id == t && (a.is_alive = 1);
                }), s.setList(a);
            },
            error: function() {}
        });
    };
    ReactDOM.render(React.createElement(e, {
        is_show: !0,
        title: "创建你的团队",
        handle_data: t,
        handle_click: s,
        exchange_text: "",
        action_text: "创建"
    }), document.getElementById("J_group_form"));
    var n = function() {
        $.ajax({
            url: "/get_my_groups/",
            type: "POST",
            dataType: "json",
            success: function(n) {
                var i = [];
                $(n.groups).each(function(e, a) {
                    i.push({
                        title: a.name,
                        id: a.id,
                        is_alive: a.is_alive
                    });
                }), a = i, ReactDOM.render(React.createElement(e, {
                    list: i,
                    is_show: !0,
                    title: "创建你的团队",
                    handle_data: t,
                    handle_click: s,
                    exchange_text: "",
                    action_text: "创建"
                }), document.getElementById("J_group_form"));
            },
            error: function() {}
        });
    };
    n();
});