define("app/js/pane/left_down_item", [ "jquery", "gallery/react/react", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var t = require("appjs/data/data_watch"), e = React.createClass({
        displayName: "ChannelItem",
        getInitialState: function() {
            var t = {
                id: 0,
                is_alive: 0,
                name: "funk"
            }, e = this.props.data || {};
            for (var i in t) t[i] = e[i] || t[i];
            return t;
        },
        resize: function() {
            alert("resize");
        },
        update_state: function() {
            this.state.update_state(this);
        },
        focus: function() {
            this.setState({
                is_focus: !0
            });
        },
        focus_out: function() {
            this.setState({
                is_focus: !1
            });
        },
        handle_click: function() {
            var e = this;
            this.state.is_alive || t.callback("left_pane_team_item", this.state.id, "click", e);
        },
        componentDidMount: function() {
            var e = this;
            t && t.register("left_pane_team_item", this.state.id, "change", function(t, i, a) {
                i && "function" == typeof i && i(e, t), e[a] && "function" == typeof e[a] && e[a]();
            });
        },
        render: function() {
            var t = this, e = (t.props.main_el, this.state.num ? "user_info_num" : "user_info_num hide", 
            this.state.is_focus ? "user_item user_item_focus" : "user_item", this.state.is_alive ? "is_alive_team" : "is_alive_team hide");
            return React.createElement("div", {
                className: "team_ct_item",
                onClick: this.handle_click
            }, React.createElement("div", {
                className: e
            }), this.state.name);
        }
    });
    return e;
});