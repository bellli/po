define("app/js/pane/channel_user_item", [ "jquery", "gallery/react/react" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = React.createClass({
        displayName: "ChannelUserItem",
        getInitialState: function() {
            var e = {
                icon: "https://cfl.dropboxstatic.com/static/images/avatar/faceholder-64-vflHTEplh.png",
                username: "None",
                email: ""
            }, t = this.props.data || {};
            for (var a in e) e[a] = t[a] || e[a];
            return e;
        },
        resize: function() {},
        show_des_pane: function() {},
        componentDidMount: function() {},
        render: function() {
            var e = this;
            e.props.main_el;
            return React.createElement("div", {
                className: "channel_user_item",
                onClick: this.show_des_pane
            }, React.createElement("img", {
                src: this.state.icon
            }));
        }
    });
    return e;
});