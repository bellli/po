define("app/js/pane/right_pane", [ "jquery", "gallery/react/react", "appjs/forms/MessageForm.js", "appjs/pane/message_item", "appjs/pane/channel_users", "appjs/data/data_watch", "appjs/forms/add_to_do_list_form", "appjs/pane/to_do_list_items" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = require("appjs/forms/MessageForm.js"), t = require("appjs/pane/message_item"), a = require("appjs/pane/channel_users"), s = require("appjs/data/data_watch"), n = require("appjs/forms/add_to_do_list_form"), c = require("appjs/pane/to_do_list_items"), i = React.createClass({
        displayName: "RightPane",
        getInitialState: function() {
            var e = this, t = {
                icon: "https://www.cdn.whatsapp.net/img/v4/icon-shield.png",
                name: "None",
                id: 0,
                send_message: function() {
                    alert("r");
                },
                after_mounted: function(t) {
                    return e;
                },
                is_show: !1,
                brief: "这里是设计师部门的主场，主要用于设计师团队内部的沟通交流，知识分享等。",
                message_data: [ {} ],
                message_data: [],
                first_show: !0,
                message_form_key: new Date().getTime() + "*" + Math.random() + "*" + Math.random(),
                channel_user_key: new Date().getTime() + "*" + Math.random() + "*" + Math.random(),
                TodoListForm_key: new Date().getTime() + "*" + Math.random() + "*" + Math.random(),
                channel_users: [ {
                    username: "a",
                    email: "a@a.com"
                }, {
                    username: "a",
                    email: "a@a.com"
                } ]
            }, a = this.props.data || {};
            for (var s in t) t[s] = a[s] || t[s];
            var n = t.send_message;
            return t.send_message = function(t, a) {
                n(t, a, e);
            }, t;
        },
        componentDidMount: function() {
            var e = this;
            this.state.after_mounted(e), e.to_bottom(), s && s.register("right_pane", this.state.id, "change", function(t, a, s) {
                a && "function" == typeof a && a(e, t), e[s] && "function" == typeof e[s] && e[s]();
            });
        },
        show_user_channel_box: function() {},
        resize: function() {},
        hide: function() {
            this.setState({
                is_show: !1
            });
        },
        show: function() {
            var e = this;
            e.setState({
                is_show: !0
            }), e.state.first_show && (setTimeout(function() {
                e.to_bottom();
            }, 50), e.setState({
                first_show: !1
            }));
        },
        first_show: function() {
            this.state.first_show && (this.to_bottom(), this.setState({
                first_show: !1
            }));
        },
        check_to_show: function(e) {
            e == this.state.id ? this.show() : this.hide();
        },
        show_to_do_form: function() {
            var e = this;
            s.callback("to_do_form", e.state.id, "change", {}, function(e, t) {
                e.setState({
                    is_show: !0
                });
            });
        },
        to_bottom: function() {
            var e = this;
            e.pane.scrollTop = e.pane.scrollHeight;
        },
        set_channel_users: function(e) {
            var t = this;
            t.setState({
                channel_users: e,
                channel_user_key: new Date().getTime() + Math.random() + 2e5 * Math.random()
            });
        },
        add_message: function(e) {
            this.state.message_data.push(e);
            var t = this.state.message_data;
            this.setState({
                message_data: t
            });
        },
        render: function() {
            for (var i = this, o = (i.props.main_el, {
                id: this.state.id
            }), r = this.state.message_data, _ = [], m = r.length - 1; m >= 0; m--) {
                var h = r[r.length - m - 1];
                h.key = h.key ? h.key : new Date().getTime() + "*" + Math.random() + "*" + Math.random(), 
                _.push(React.createElement(t, {
                    key: h.key,
                    data: h
                }));
            }
            var l = {
                channel_users: this.state.channel_users,
                id: this.state.id,
                add_user_to_channel: function() {
                    s.callback("add_user_to_channel_box", 1, "change", {}, function(e, t) {
                        e.setState({
                            is_show: !0,
                            name: i.state.name,
                            id: i.state.id
                        });
                    }), s.callback("add_user_to_channel_form", 1, "change", {}, function(e, t) {
                        e.setState({
                            id: i.state.id
                        }), e.load_data(i.state.id);
                    });
                }
            }, d = this.state.is_show ? "right_pane when_right_show" : "right_pane when_right_show hide";
            return React.createElement("div", {
                className: d
            }, React.createElement("div", {
                className: "top_nav"
            }, React.createElement("div", {
                className: "channel_hd_brief clr"
            }, React.createElement("div", {
                className: "channel_hd_pic"
            }, React.createElement("img", {
                src: this.state.icon,
                className: "com_in_img"
            })), React.createElement("div", {
                className: "channel_hd_txt"
            }, React.createElement("div", {
                className: "channel_hd_name"
            }, this.state.name))), React.createElement("div", {
                className: "to_do_icon"
            })), React.createElement("div", {
                className: "to_do_pane"
            }, React.createElement("div", {
                className: "to_do_top_nav"
            }, React.createElement("a", {
                href: "javascript:;",
                className: "start_to_do",
                onClick: i.show_to_do_form
            }, React.createElement("span", {
                className: "add_icon"
            }), React.createElement("span", {
                className: "start_to_do_text"
            }, "添加任务...")), React.createElement("a", {
                href: "",
                className: "finished_to_action"
            })), React.createElement(n, {
                data: o
            }), React.createElement(c, null)), React.createElement("div", {
                className: "right_pane_inner_ct"
            }, React.createElement("div", {
                className: "right_pane_inner",
                ref: function(e) {
                    i.pane = e;
                }
            }, React.createElement("div", {
                className: "msg_item"
            }, React.createElement("div", {
                className: "msg_item_logo"
            }, React.createElement("img", {
                src: "https://cfl.dropboxstatic.com/static/images/icons/blue_dropbox_glyph-vflOJKOUw.png"
            })), React.createElement("div", {
                className: "channel_detail_show"
            }, React.createElement("div", {
                className: "channel_detail_title"
            }, "频道详情"), React.createElement("div", {
                className: "channel_detail_text"
            }, this.state.brief)), React.createElement(a, {
                key: this.state.channel_user_key,
                data: l
            })), _)), React.createElement(e, {
                key: i.state.message_form_key,
                data: this.state
            }));
        }
    });
    return i;
});