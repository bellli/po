define("app/js/pane/to_do_textares", [ "jquery", "gallery/react/react", "appjs/pane/channel_item", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = (require("appjs/pane/channel_item"), require("appjs/data/data_watch")), t = React.createClass({
        displayName: "UserItem",
        getInitialState: function() {
            var e = {
                id: "",
                placeholder: ""
            }, t = this.props.data || {};
            for (var a in e) e[a] = t[a] || e[a];
            return e;
        },
        resize: function() {},
        handle_click: function(e, t) {
            var a = this;
            t.focus(), $(a.state.channel_list).each(function(t, a) {
                a.is_focus = !1, a.id == e && (a.is_focus = !0), a.key = new Date().getTime() + Math.random();
            }), a.setState({
                channel_list: a.state.channel_list
            }), a.state.channel_select && "function" == typeof a.state.channel_select && a.state.channel_select(e);
        },
        start_add_channel: function() {
            e.callback("add_user_to_channel_invite_box", 1, "change", {}, function(e) {
                e.setState({
                    is_show: !0
                });
            });
        },
        componentDidMount: function() {
            var t = this;
            e && e.register("channel_item_items", 1, "change", function(e, a, n) {
                a && "function" == typeof a && a(t, e), t[n] && "function" == typeof t[n] && t[n]();
            });
        },
        add_channel: function() {},
        render: function() {
            return React.createElement("textarea", {
                className: "atd_text_area",
                placeholder: this.state.placeholder
            });
        }
    });
    return t;
});