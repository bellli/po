define("app/js/pane/left_down", [ "jquery", "gallery/react/react", "appjs/data/data_watch", "appjs/pane/left_down_item", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = require("appjs/data/data_watch"), t = require("appjs/pane/left_down_item"), e = require("appjs/data/data_watch"), a = React.createClass({
        displayName: "ChannelItem",
        getInitialState: function() {
            var e = {
                id: 0,
                name: "",
                groups: [],
                is_show: !1
            }, t = this.props.data || {};
            for (var a in e) e[a] = t[a] || e[a];
            return e.name = "", $(e.groups).each(function(t, a) {
                a.key = a.key || new Date().getTime() + Math.random(), a.is_alive && (e.name = a.name, 
                e.id = a.id, e.username = a.username, e.email = a.email);
            }), e;
        },
        resize: function() {
            alert("resize");
        },
        update_state: function() {
            this.state.update_state(this);
        },
        focus: function() {
            this.setState({
                is_focus: !0
            });
        },
        focus_out: function() {
            this.setState({
                is_focus: !1
            });
        },
        handle_click: function() {
            var t = this;
            e.callback("channel_item", this.state.id, "click", t);
        },
        add_group: function(e) {
            var t = this, a = !1;
            $(t.state.groups).each(function(t, s) {
                s.id == e.id && (a = !0);
            }), a || (t.state.groups.push(e), e.key = new Date().getTime() + Math.random(), 
            t.setState({
                groups: t.state.groups
            }));
        },
        componentDidMount: function() {
            var t = this;
            e && e.register("left_down_groups", 1, "change", function(e, a, s) {
                a && "function" == typeof a && a(t, e), t[s] && "function" == typeof t[s] && t[s]();
            }), $(document.body).on("click", function() {
                t.setState({});
            });
        },
        toggle: function() {
            var e = this;
            e.state.is_show ? e.setState({
                is_show: !1
            }) : e.setState({
                is_show: !0
            });
        },
        add_team: function() {
            e.callback("add_team_pane", 1, "show");
        },
        render: function() {
            for (var e = this.state.is_show ? "profile_dn_des" : "profile_dn_des hide", a = [], s = this.state.groups.length - 1; s >= 0; s--) {
                var n = this.state.groups[s];
                a.push(React.createElement(t, {
                    data: n,
                    key: n.key
                }));
            }
            return React.createElement("div", {
                className: "channel_hd_brief group_hd_brief clr"
            }, React.createElement("div", {
                className: "channel_hd_txt",
                onClick: this.toggle
            }, React.createElement("div", {
                className: "channel_hd_name"
            }, this.state.name), React.createElement("div", {
                className: "channel_hd_person_name"
            }, this.state.email)), React.createElement("div", {
                className: e
            }, React.createElement("div", {
                className: "team_ct"
            }, React.createElement("div", {
                className: "team_ct_hd"
            }, "团队", React.createElement("span", {
                className: "add_team",
                onClick: this.add_team
            }, "新建")), a)));
        }
    });
    return a;
});