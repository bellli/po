define("app/js/pane/main_pane", [ "jquery", "gallery/react/react", "appjs/pane/left_pane", "appjs/pane/right_pane", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = require("appjs/pane/left_pane"), t = require("appjs/pane/right_pane"), n = require("appjs/data/data_watch"), a = React.createClass({
        displayName: "MainPane",
        getInitialState: function() {
            var e = {
                left_pane_data: {
                    UserItem: {
                        channel_name: "频道",
                        channel_num: "3",
                        channel_list: [ {
                            name: "bandon",
                            num: 8,
                            id: 100,
                            is_show: !0,
                            channel_users: [],
                            channel_select: function() {}
                        }, {
                            name: "bandon",
                            num: 2,
                            id: 1002,
                            is_show: !1,
                            channel_users: [],
                            channel_select: function() {}
                        }, {
                            name: "bandon",
                            is_show: !1,
                            channel_users: [],
                            channel_select: function() {}
                        } ]
                    }
                },
                after_mounted: function() {},
                channel_select: function() {},
                update_state: function() {}
            }, t = this.props.data || {};
            for (var n in e) e[n] = t[n] || e[n];
            return e;
        },
        resize: function() {},
        componentDidMount: function() {
            var e = this;
            this.state.after_mounted(e), n && n.register("chat_pane", 1, "change", function(t, n, a) {
                n && "function" == typeof n && n(e, t), e[a] && "function" == typeof e[a] && e[a]();
            });
        },
        render: function() {
            var n = this, a = n.props.main_el;
            n.props.hide ? a.hide() : a.show();
            for (var s = this.state.left_pane_data.UserItem.channel_list, i = [], c = s.length - 1; c >= 0; c--) {
                var r = s[c];
                r.after_mounted = function(e) {
                    n.state.after_mounted(e);
                }, r.key = r.key ? r.key : new Date().getTime() + Math.random(), i.push(React.createElement(t, {
                    key: r.key,
                    data: r
                }));
            }
            var o = this.state.left_pane_data;
            return o.update_state = function(e, t, a) {
                this.state.update_state(e, t, a, n);
            }, React.createElement("div", {
                onResize: n.resize
            }, React.createElement(e, {
                data: this.state.left_pane_data
            }), i);
        }
    });
    return a;
});