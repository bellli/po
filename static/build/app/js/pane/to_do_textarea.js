define("app/js/pane/to_do_textarea", [ "jquery", "gallery/react/react", "appjs/pane/channel_item", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var t = (require("appjs/pane/channel_item"), require("appjs/data/data_watch")), e = React.createClass({
        displayName: "UserItem",
        getInitialState: function() {
            var t = {
                id: "",
                placeholder: "",
                scrollHeight: 0
            }, e = this.props.data || {};
            for (var n in t) t[n] = e[n] || t[n];
            return t;
        },
        resize: function() {},
        handle_click: function(t, e) {
            var n = this;
            e.focus(), $(n.state.channel_list).each(function(e, n) {
                n.is_focus = !1, n.id == t && (n.is_focus = !0), n.key = new Date().getTime() + Math.random();
            }), n.setState({
                channel_list: n.state.channel_list
            }), n.state.channel_select && "function" == typeof n.state.channel_select && n.state.channel_select(t);
        },
        start_add_channel: function() {
            t.callback("add_user_to_channel_invite_box", 1, "change", {}, function(t) {
                t.setState({
                    is_show: !0
                });
            });
        },
        componentDidMount: function() {
            var e = this;
            t && t.register("channel_item_items", 1, "change", function(t, n, a) {
                n && "function" == typeof n && n(e, t), e[a] && "function" == typeof e[a] && e[a]();
            });
        },
        focusout: function() {
            var t = this, e = t.input;
            console.log(e.offsetHeight), console.log(e.scrollHeight), e.offsetHeight < e.scrollHeight;
        },
        init_base_config: function(t) {
            var e = this;
            e.input;
        },
        add_channel: function() {},
        render: function() {
            var t = this;
            return React.createElement("textarea", {
                className: "atd_text_area",
                ref: function(e) {
                    t.input = e, t.init_base_config();
                },
                onBlur: t.focusout,
                placeholder: this.state.placeholder
            });
        }
    });
    return e;
});