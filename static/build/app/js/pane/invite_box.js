define("app/js/pane/invite_box", [ "jquery", "gallery/react/react", "appjs/data/data_watch", "appjs/forms/invite_item_form" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = require("appjs/data/data_watch"), t = require("appjs/forms/invite_item_form"), a = React.createClass({
        displayName: "InviteBox",
        getInitialState: function() {
            var e = {
                id: 0,
                name: "None",
                send_message: function() {
                    alert("m");
                },
                is_show: !1
            }, t = this.props.data || {};
            for (var a in e) e[a] = t[a] || e[a];
            return e;
        },
        send_message: function(e) {
            var t = e.keyCode;
            if (13 == t) {
                var a = e.target || e.srcElement;
                a = $(a);
                var n = a.val();
                this.state.send_message && "function" == typeof this.state.send_message && this.state.send_message(this.state.id, n), 
                this.clear();
            }
        },
        clear: function() {
            this.input.value = "";
        },
        close: function() {
            var e = this;
            e.setState({
                is_show: !1
            });
        },
        componentDidMount: function() {
            var t = this;
            e && e.register("add_user_to_channel_invite_box", 1, "change", function(e, a, n) {
                a && "function" == typeof a && a(t, e), t[n] && "function" == typeof t[n] && t[n]();
            });
        },
        render: function() {
            var e = this, a = this.state.is_show ? "box_mask" : "box_mask hide";
            return React.createElement("div", {
                className: a,
                ref: function(t) {
                    e.box_el = t;
                }
            }, React.createElement("div", {
                className: "box_inner"
            }, React.createElement("div", {
                className: "close_pane_action",
                onClick: e.close
            }, React.createElement("div", {
                className: "plus_icon"
            })), React.createElement("div", {
                className: "box_inner_title"
            }, "创建频道"), React.createElement("div", {
                className: "box_container"
            }, React.createElement(t, null))));
        }
    });
    return a;
});