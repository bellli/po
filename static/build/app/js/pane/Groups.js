define("app/js/pane/Groups", [ "jquery", "gallery/react/react", "appjs/pane/Group_item" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = require("appjs/pane/Group_item"), t = React.createClass({
        displayName: "Groups",
        getInitialState: function() {
            var e = {};
            return e.title = this.props.title, this.props.list = this.props.list || [], e.num = this.props.list.length, 
            e.list = this.props.list, $(e.list).each(function(e, t) {
                t.key = new Date().getTime() + Math.random();
            }), e;
        },
        resize: function() {},
        componentDidMount: function() {},
        render: function() {
            for (var t = this, i = (t.props.main_el, []), a = t.state.list.length - 1; a >= 0; a--) {
                var s = t.state.list[a], n = function(e, i) {
                    t.props.handle_click && t.props.handle_click(e, i);
                };
                i.push(React.createElement(e, {
                    is_alive: s.is_alive,
                    title: s.title,
                    num: s.num,
                    id: s.id,
                    handle_click: n,
                    key: s.key
                }));
            }
            return React.createElement("div", {
                className: "user_items"
            }, React.createElement("div", {
                className: "user_item user_item_title"
            }, React.createElement("div", {
                className: "user_item_ricon"
            }, React.createElement("div", {
                className: "user_item_icon_ct"
            }, React.createElement("img", {
                src: "http://192.168.0.100:1022/po/static/images/channel_icon.png"
            }))), React.createElement("div", {
                className: "user_item_inner"
            }, React.createElement("span", {
                className: "user_item_text"
            }, this.state.title, " ", this.state.num))), i);
        }
    });
    return t;
});