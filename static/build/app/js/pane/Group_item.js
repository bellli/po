define("app/js/pane/Group_item", [ "jquery", "gallery/react/react", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = require("appjs/data/data_watch"), t = React.createClass({
        displayName: "GroupsItem",
        getInitialState: function() {
            var e = {};
            return e.title = this.props.title, e.num = this.props.num, e.icon = this.props.icon || "https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg", 
            e.key = new Date().getTime() + Math.random(), e.id = this.props.id, e;
        },
        resize: function() {},
        componentDidMount: function() {
            var t = this;
            e && e.register("left_pane_team_item", this.state.id, "change", function(e, a, i) {
                a && "function" == typeof a && a(t, e), t[i] && "function" == typeof t[i] && t[i]();
            });
        },
        render: function() {
            var t = this, a = (t.props.main_el, function(a) {
                e.callback("left_pane_team_item", t.state.id, "click", t), t.props.handle_click && t.props.handle_click(a, t.state.id);
            }), i = this.state.num ? "user_info_num" : "user_info_num hide", s = this.props.is_alive ? "is_alive_team" : "is_alive_team hide";
            return React.createElement("div", {
                className: "user_item",
                key: this.state.key,
                attr_id: this.state.id,
                onClick: a
            }, React.createElement("div", {
                className: i
            }, this.state.num), React.createElement("div", {
                className: s
            }), React.createElement("div", {
                className: "user_item_ricon"
            }, React.createElement("div", {
                className: "user_item_icon_ct"
            }, React.createElement("img", {
                src: this.state.icon
            }))), React.createElement("div", {
                className: "user_item_inner"
            }, React.createElement("span", {
                className: "user_item_text"
            }, this.state.title)));
        }
    });
    return t;
});