define("app/js/pane/left_pane", [ "jquery", "gallery/react/react", "appjs/pane/user_item", "appjs/pane/left_down" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = require("appjs/pane/user_item"), t = require("appjs/pane/left_down"), a = React.createClass({
        displayName: "LeftPane",
        getInitialState: function() {
            var e = {
                UserItem: {},
                channel_select: function(e) {},
                update_state: function() {},
                groups: [ {} ]
            }, t = this.props.data || {};
            for (var a in e) e[a] = t[a] || e[a];
            return e;
        },
        resize: function() {},
        componentDidMount: function() {},
        render: function() {
            var a = this, n = (a.props.main_el, this.state.UserItem);
            return n.update_state = function(e, t) {
                a.state.update_state(e, t, a);
            }, React.createElement("div", {
                className: "left_pane"
            }, React.createElement("div", {
                className: "top_nav"
            }, React.createElement(t, {
                data: this.state
            })), React.createElement("div", {
                className: "left_pane_inner"
            }, React.createElement(e, {
                data: n
            })));
        }
    });
    return a;
});