define("app/js/pane/info", [ "jquery", "gallery/react/react", "appjs/data/data_watch", "appjs/forms/add_user_to_channel_form" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var t = require("appjs/data/data_watch"), e = (require("appjs/forms/add_user_to_channel_form"), 
    React.createClass({
        displayName: "Info",
        getInitialState: function() {
            var t = {
                id: 0,
                name: "None",
                send_message: function() {
                    alert("m");
                },
                is_show: !1,
                info_text: ""
            }, e = this.props.data || {};
            for (var s in t) t[s] = e[s] || t[s];
            return t;
        },
        send_message: function(t) {
            var e = t.keyCode;
            if (13 == e) {
                var s = t.target || t.srcElement;
                s = $(s);
                var a = s.val();
                this.state.send_message && "function" == typeof this.state.send_message && this.state.send_message(this.state.id, a), 
                this.clear();
            }
        },
        clear: function() {
            this.input.value = "";
        },
        close: function() {
            var t = this;
            t.setState({
                is_show: !1
            });
        },
        show_text: function(t) {
            var e = this;
            e.setState({
                is_show: !0,
                info_text: t
            }), setTimeout(function() {
                e.setState({
                    is_show: !1
                });
            }, 2e3);
        },
        componentDidMount: function() {
            var e = this;
            t && t.register("info_message", 1, "change", function(t, s, a) {
                s && "function" == typeof s && s(e, t), e[a] && "function" == typeof e[a] && e[a]();
            });
        },
        render: function() {
            var t = this.state.is_show ? "info_msg_ct" : "info_msg_ct hide";
            return React.createElement("div", {
                className: t
            }, this.state.info_text);
        }
    }));
    return e;
});