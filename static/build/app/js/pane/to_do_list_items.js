define("app/js/pane/to_do_list_items", [ "jquery", "gallery/react/react", "appjs/data/data_watch", "appjs/pane/to_do_list_item" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var t = require("appjs/data/data_watch"), e = require("appjs/pane/to_do_list_item"), a = React.createClass({
        displayName: "ToDoListItems",
        getInitialState: function() {
            var t = {
                id: 0,
                num: 0,
                icon: parseInt(10 * Math.random() / 2) ? "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg" : "https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg",
                name: "None",
                list: [ {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {} ],
                handle_click: function(t) {},
                update_state: function() {}
            }, e = this.props.data || {};
            for (var a in t) t[a] = e[a] || t[a];
            return t;
        },
        componentDidMount: function() {
            var e = this;
            t && t.register("to_do_list_items", this.state.id, "change", function(t, a, i) {
                a && "function" == typeof a && a(e, t), e[i] && "function" == typeof e[i] && e[i]();
            });
        },
        select: function() {
            var t = this, e = !t.state.select;
            t.setState({
                select: e
            });
        },
        close: function() {
            var t = this;
            t.setState({
                is_show: !1
            });
        },
        render: function() {
            var t = this, a = (t.state.is_show ? "add_to_do_list_form" : "add_to_do_list_form hide", 
            t.state.select ? "list_item list_item_selected" : "list_item", []);
            return $(t.state.list).each(function(t, i) {
                i.key = i.key || new Date().getTime() + "" + Math.random(), i.name = (i.name || "官网todolist 设计") + parseInt(100 * Math.random()), 
                a.push(React.createElement(e, {
                    data: i,
                    key: i.key
                }));
            }), React.createElement("div", {
                className: "list_pane_ct"
            }, React.createElement("div", {
                className: "list_pane"
            }, a));
        }
    });
    return a;
});