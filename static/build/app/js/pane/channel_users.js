define("app/js/pane/channel_users", [ "jquery", "gallery/react/react", "appjs/pane/channel_user_item", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = require("appjs/pane/channel_user_item"), a = require("appjs/data/data_watch"), t = React.createClass({
        displayName: "ChannelUsers",
        getInitialState: function() {
            var e = {
                id: 0,
                icon: "https://www.cdn.whatsapp.net/img/v4/icon-shield.png",
                add_user_to_channel: function() {},
                channel_users: [ {
                    icon: "https://cfl.dropboxstatic.com/static/images/icons/blue_dropbox_glyph-vflOJKOUw.png",
                    username: "a",
                    email: "a@a.com"
                }, {
                    icon: "https://www.cdn.whatsapp.net/img/v4/icon-shield.png",
                    username: "a",
                    email: "a@a.com"
                }, {
                    icon: "https://cfl.dropboxstatic.com/static/images/icons/blue_dropbox_glyph-vflOJKOUw.png",
                    username: "a",
                    email: "a@a.com"
                }, {
                    icon: "https://www.cdn.whatsapp.net/img/v4/icon-shield.png",
                    username: "a",
                    email: "a@a.com"
                } ]
            }, a = this.props.data || {};
            for (var t in e) e[t] = a[t] || e[t];
            return e;
        },
        resize: function() {},
        add_user_to_channel: function() {
            var e = this;
            e.state.add_user_to_channel && e.state.add_user_to_channel();
        },
        componentDidMount: function() {
            var e = this;
            a && a.register("channel_users", this.state.id, "change", function(a, t, n) {
                t && "function" == typeof t && t(e, a), e[n] && "function" == typeof e[n] && e[n]();
            });
        },
        render: function() {
            for (var a = this.state.channel_users, t = [], n = a.length - 1; n >= 0; n--) {
                var c = a[n];
                c.key = new Date().getTime() + Math.random(), t.push(React.createElement(e, {
                    data: c,
                    key: c.key
                }));
            }
            return React.createElement("div", {
                className: "channel_user_list_ct clr"
            }, t, React.createElement("div", {
                className: "channel_user_item plus_icon_ct",
                onClick: this.add_user_to_channel
            }, React.createElement("div", {
                className: "plus_icon"
            })));
        }
    });
    return t;
});