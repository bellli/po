define("app/js/pane/user_item", [ "jquery", "gallery/react/react", "appjs/pane/channel_item", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = require("appjs/pane/channel_item"), t = require("appjs/data/data_watch"), a = React.createClass({
        displayName: "UserItem",
        getInitialState: function() {
            var e = this, t = {
                channel_name: "",
                channel_id: "",
                channel_num: "",
                channel_list: [ {} ],
                channel_select: function(e) {},
                update_state: function() {}
            }, a = this.props.data || {};
            for (var n in t) t[n] = a[n] || t[n];
            return $(t.channel_list).each(function(t, a) {
                a.handle_click = e.handle_click, a.key = a.key || new Date().getTime() + Math.random();
            }), t.channel_num = t.channel_list.length, t;
        },
        resize: function() {},
        handle_click: function(e, t) {
            var a = this;
            t.focus(), $(a.state.channel_list).each(function(t, a) {
                a.is_focus = !1, a.id == e && (a.is_focus = !0), a.key = new Date().getTime() + Math.random();
            }), a.setState({
                channel_list: a.state.channel_list
            }), a.state.channel_select && "function" == typeof a.state.channel_select && a.state.channel_select(e);
        },
        start_add_channel: function() {
            t.callback("add_user_to_channel_invite_box", 1, "change", {}, function(e) {
                e.setState({
                    is_show: !0
                });
            });
        },
        componentDidMount: function() {
            var e = this;
            t && t.register("channel_item_items", 1, "change", function(t, a, n) {
                a && "function" == typeof a && a(e, t), e[n] && "function" == typeof e[n] && e[n]();
            });
        },
        add_channel: function() {},
        render: function() {
            for (var t = this, a = (t.props.main_el, []), n = this.state.channel_list.length - 1; n >= 0; n--) {
                var c = this.state.channel_list[this.state.channel_list.length - 1 - n];
                c.update_state = function(e) {
                    t.setState.update_state(e, t);
                }, a.push(React.createElement(e, {
                    data: c,
                    key: c.key
                }));
            }
            return React.createElement("div", {
                className: "user_items"
            }, React.createElement("div", {
                className: "user_item user_item_title"
            }, React.createElement("div", {
                className: "user_item_ricon"
            }, React.createElement("div", {
                className: "user_item_icon_ct user_item_chat_icon"
            })), React.createElement("div", {
                className: "user_item_inner"
            }, React.createElement("span", {
                className: "user_item_text"
            }, this.state.channel_name, " ", this.state.channel_num)), React.createElement("div", {
                className: "add_channel_action",
                onClick: this.start_add_channel
            }, React.createElement("div", {
                className: "plus_icon"
            }))), a);
        }
    });
    return a;
});