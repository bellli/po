define("app/js/pane/to_do_list_item", [ "jquery", "gallery/react/react", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var t = require("appjs/data/data_watch"), e = React.createClass({
        displayName: "ToDoListItem",
        getInitialState: function() {
            var t = {
                id: 0,
                num: 0,
                icon: parseInt(10 * Math.random() / 2) ? "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg" : "https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg",
                name: "None",
                is_show: !0,
                handle_click: function(t) {},
                update_state: function() {}
            }, e = this.props.data || {};
            for (var a in t) t[a] = e[a] || t[a];
            return t;
        },
        componentDidMount: function() {
            var e = this;
            t && t.register("to_to_list_item", this.state.id, "change", function(t, a, s) {
                a && "function" == typeof a && a(e, t), e[s] && "function" == typeof e[s] && e[s]();
            });
        },
        select: function() {
            var t = this, e = !t.state.select;
            t.setState({
                select: e
            }), setTimeout(function() {
                t.setState({
                    is_show: !1
                });
            }, 500);
        },
        close: function() {
            var t = this;
            t.setState({
                is_show: !1
            });
        },
        render: function() {
            var t = this, e = (t.state.is_show ? "add_to_do_list_form" : "add_to_do_list_form hide", 
            t.state.select ? "list_item list_item_selected" : "list_item"), e = t.state.is_show ? e : e + " hide";
            return React.createElement("div", {
                className: e
            }, React.createElement("div", {
                className: "list_item_select_ct",
                onClick: t.select
            }, React.createElement("div", {
                className: "list_select"
            }, React.createElement("div", {
                className: "list_select_pointer"
            }))), React.createElement("div", {
                className: "list_item_brief_ct"
            }, React.createElement("div", {
                className: "list_item_brief_text"
            }, this.state.name), React.createElement("div", {
                className: "list_item_create_info"
            }, "5月12日 周五 来自 Jeffbloom")), React.createElement("div", {
                className: "list_create_hd"
            }, React.createElement("img", {
                src: "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg"
            })));
        }
    });
    return e;
});