define("app/js/pane/channel_item", [ "jquery", "gallery/react/react", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var t = require("appjs/data/data_watch"), e = React.createClass({
        displayName: "ChannelItem",
        getInitialState: function() {
            var t = {
                id: 0,
                num: 0,
                icon: parseInt(10 * Math.random() / 2) ? "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg" : "https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg",
                name: "None",
                is_focus: !1,
                handle_click: function(t) {},
                update_state: function() {}
            }, e = this.props.data || {};
            for (var a in t) t[a] = e[a] || t[a];
            return t;
        },
        resize: function() {
            alert("resize");
        },
        update_state: function() {
            this.state.update_state(this);
        },
        focus: function() {
            this.setState({
                is_focus: !0
            });
        },
        focus_out: function() {
            this.setState({
                is_focus: !1
            });
        },
        handle_click: function() {
            var e = this;
            t.callback("channel_item", this.state.id, "click", e);
        },
        componentDidMount: function() {
            var e = this;
            t && t.register("channel_item", this.state.id, "change", function(t, a, i) {
                a && "function" == typeof a && a(e, t), e[i] && "function" == typeof e[i] && e[i]();
            });
        },
        render: function() {
            var t = this, e = (t.props.main_el, this.state.num ? "user_info_num" : "user_info_num hide"), a = this.state.is_focus ? "user_item user_item_focus" : "user_item";
            return React.createElement("div", {
                className: a,
                onClick: this.handle_click
            }, React.createElement("div", {
                className: e
            }, this.state.num), React.createElement("div", {
                className: "user_item_ricon"
            }, React.createElement("div", {
                className: "user_item_icon_ct"
            }, React.createElement("img", {
                src: this.state.icon
            }))), React.createElement("div", {
                className: "user_item_inner"
            }, React.createElement("span", {
                className: "user_item_text"
            }, this.state.name)));
        }
    });
    return e;
});