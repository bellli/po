define("app/js/pane/message_item", [ "jquery", "gallery/react/react" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = React.createClass({
        displayName: "MessageItem",
        getInitialState: function() {
            var e = {
                icon: "https://www.cdn.whatsapp.net/img/v4/icon-shield.png",
                username: "Nodne",
                message: "",
                time: "PM",
                key: new Date().getTime() + "*" + Math.random() + "*" + Math.random()
            }, a = this.props.data || {};
            for (var t in e) e[t] = a[t] || e[t];
            return e;
        },
        resize: function() {},
        componentDidMount: function() {},
        render: function() {
            var e = this, a = this.state.message, t = new Date().getTime() + Math.random(), n = a.match(/@\w+\s+/g), s = a.split(/@\w+[\s+|s]/g, t), m = [];
            return n ? $(s).each(function(e, a) {
                var t = new Date().getTime() + "*" + Math.random() + "*" + Math.random(), s = React.createElement("span", {
                    key: t
                }, a), t = new Date().getTime() + "*" + Math.random() + "*" + Math.random(), c = React.createElement("span", {
                    key: t
                }), t = new Date().getTime() + "*" + Math.random() + "*" + Math.random();
                if (n[e]) var c = React.createElement("span", {
                    key: t,
                    className: "message_at_name"
                }, n[e]);
                m.push(s), m.push(c);
            }) : m.push(React.createElement("span", {
                key: e.state.key
            }, a)), React.createElement("div", {
                className: "msg_item"
            }, React.createElement("div", {
                className: "msg_item_logo"
            }, React.createElement("img", {
                src: this.state.icon
            })), React.createElement("div", {
                className: "channel_message_ct"
            }, React.createElement("div", {
                className: "channel_message_title"
            }, React.createElement("span", null, this.state.username), React.createElement("span", {
                className: "channel_message_time"
            }, this.state.time)), React.createElement("div", {
                className: "channel_message_text"
            }, m)));
        }
    });
    return e;
});