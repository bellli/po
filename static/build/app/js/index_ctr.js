define("app/js/index_ctr", [ "appjs/forms/sign_form.js", "appjs/forms/login_form.js", "appjs/forms/LoginSignForm", "appjs/message/send_message", "appjs/pane/main_pane", "appjs/forms/group_form" ], function(require, exports) {
    require("appjs/forms/sign_form.js"), require("appjs/forms/login_form.js"), require("appjs/forms/LoginSignForm"), 
    require("appjs/message/send_message"), require("appjs/pane/main_pane"), require("appjs/forms/group_form");
    seajs.use("appjs/ctrs/show_main_ctr");
});