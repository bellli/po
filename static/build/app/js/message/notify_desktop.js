define("app/js/message/notify_desktop", [], function(require, exports) {
    document.addEventListener("DOMContentLoaded", function() {
        Notification && "granted" !== Notification.permission && Notification.requestPermission();
    });
    var n = function(n, i) {
        var t = new Notification(n, i);
        t.onclick = function() {
            window.open("http://127.0.0.1:8000/show/");
        };
    };
    return n;
});