define("app/js/message/send_message", [], function(require, exports) {
    var e = function(e, n) {
        var t = arguments.callee;
        t.open || !1;
        return t.ws && 3 != t.ws.readyState && 2 != t.ws.readyState || (t.ws = new WebSocket("ws://127.0.0.1:8001/chatsocket"), 
        t.ws.onmessage = function(e) {
            console.log(e.data), n && "function" == typeof n && n(JSON.parse(e.data));
        }, t.open = !1, t.ws.onopen = function() {
            1 == t.ws.readyState && (t.open = !0);
        }), t.open ? t.ws.send(JSON.stringify(e)) : setTimeout(function() {
            t(e);
        }, 1e3), console.log(t.ws.readyState), t.ws;
    };
    return e;
});