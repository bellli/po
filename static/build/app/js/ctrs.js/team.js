define("app/js/ctrs.js/team", [ "appjs/forms/sign_form.js", "appjs/forms/login_form.js", "appjs/forms/LoginSignForm", "appjs/message/send_message", "appjs/pane/main_pane", "appjs/forms/group_form" ], function(require, exports) {
    var e = require("appjs/forms/sign_form.js");
    var n = require("appjs/forms/login_form.js");
    var a = require("appjs/forms/LoginSignForm");
    var t = require("appjs/message/send_message");
    var r = require("appjs/pane/main_pane");
    var s = require("appjs/forms/group_form");
    var i = function(e, n, a) {
        if (e.status == 1) {
            var t = [];
            $(e.groups).each(function(e, n) {
                t.push({
                    title: n.name,
                    id: n.id
                });
            });
            t.reverse();
            a.setList(t);
        } else {
            alert(e.message);
        }
    };
    var c = function(e, n, a) {
        $.ajax({
            url: "/alive_group/",
            type: "POST",
            dataType: "json",
            data: {
                group_id: n
            },
            success: function(e) {
                alert(n);
            },
            error: function() {}
        });
    };
    var o = function() {
        var e = function() {
            l();
        };
        ReactDOM.render(React.createElement(a, {
            show_sign: true,
            success: e,
            show_sign: false,
            hide_all: false
        }), document.getElementById("J_sign_form"));
    };
    var u = function() {
        ReactDOM.render(React.createElement(a, {
            show_sign: true,
            show_sign: false,
            hide_all: true
        }), document.getElementById("J_sign_form"));
    };
    var l = function() {
        var e = $("#J_main");
        ReactDOM.render(React.createElement(r, {
            main_el: e,
            hide: false
        }), document.getElementById("J_main"));
    };
    var m = function() {
        var e = $("#J_main");
        ReactDOM.render(React.createElement(r, {
            main_el: e,
            hide: true
        }), document.getElementById("J_main"));
    };
    var f = function() {
        $.ajax({
            url: "/is_login/",
            dataType: "json",
            success: function(e) {
                if (e.status != 1) {
                    o();
                    m();
                } else {
                    u();
                    l();
                }
            }
        });
    };
    var d = function() {
        $.ajax({
            url: "/get_channels/",
            type: "POST",
            dataType: "json",
            success: function() {},
            error: function() {}
        });
    };
    var _ = function() {
        $.ajax({
            url: "/get_my_groups/",
            type: "POST",
            dataType: "json",
            success: function(e) {
                var n = [];
                $(e.groups).each(function(e, a) {
                    n.push({
                        title: a.name,
                        id: a.id
                    });
                });
                ReactDOM.render(React.createElement(s, {
                    list: n,
                    is_show: true,
                    title: "创建你的团队",
                    handle_data: i,
                    handle_click: c,
                    exchange_text: "",
                    action_text: "创建"
                }), document.getElementById("J_main"));
            },
            error: function() {}
        });
    };
    _();
});