define("app/js/sign_ctr", [ "jquery", "gallery/react/react" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = React.createClass({
        displayName: "SignForm",
        getInitialState: function() {
            return ob = {}, ob.action = this.props.action, $.extend(ob, {
                user_name: "",
                password: ""
            }), ob;
        },
        handleChange: function(e) {
            var t = e.target || e.srcElement;
            t = $(t);
            var a = t.val(), n = t.attr("name"), r = {};
            r[n] = a, this.setState(r);
        },
        submit: function() {
            var e = this;
            $.ajax({
                url: e.state.action,
                type: e.state.type,
                dataType: "json",
                success: function(e) {
                    alert("success");
                },
                error: function() {
                    alert("error");
                }
            });
        },
        render: function() {
            return React.createElement("div", null, React.createElement("form", {
                action: this.props.action,
                type: this.props.type
            }, React.createElement("input", {
                type: "text",
                onChange: this.handleChange,
                value: this.state.user_name,
                name: "user_name",
                placeholder: "用户名"
            }), React.createElement("input", {
                type: "text",
                onChange: this.handleChange,
                value: this.state.password,
                name: "password",
                placeholder: "密码"
            }), React.createElement("a", {
                onClick: this.submit,
                href: "javascript:;"
            }, "注册")));
        }
    });
    ReactDOM.render(React.createElement(e, {
        action: "/",
        type: "POST"
    }), document.getElementById("J_sign_form"));
});