define("app/js/forms/choose_list_item", [ "jquery", "gallery/react/react", "appjs/pane/to_do_textarea", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = (require("appjs/pane/to_do_textarea"), require("appjs/data/data_watch")), t = React.createClass({
        displayName: "ChooseListItem",
        getInitialState: function() {
            var e = {
                id: 0,
                num: 0,
                icon: parseInt(10 * Math.random() / 2) ? "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg" : "https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg",
                username: "None",
                email: "no email",
                is_select: !1,
                is_show: !0,
                onSelect: function() {},
                select_ob: {},
                handle_click: function(e) {},
                update_state: function() {}
            }, t = this.props.data || {};
            for (var a in e) e[a] = t[a] || e[a];
            return e;
        },
        componentDidMount: function() {
            var t = this;
            e && e.register("choose_list_item", this.state.id, "change", function(e, a, s) {
                a && "function" == typeof a && a(t, e), t[s] && "function" == typeof t[s] && t[s]();
            });
        },
        select: function() {},
        close: function() {
            var e = this;
            e.setState({
                is_show: !1
            });
        },
        focus: function() {
            var e = this, t = e.list;
            $(t).show();
        },
        out: function() {
            var e = this;
            e.list;
        },
        choose: function() {
            var e = this, t = !e.state.is_select;
            e.setState({
                is_select: t
            }), e.state.onSelect && e.state.onSelect(e, t);
        },
        render: function() {
            var e = this, t = e.state.is_select ? "user_item user_item_focus duser_item" : "user_item  duser_item";
            return React.createElement("div", {
                className: t,
                onClick: e.choose
            }, React.createElement("div", {
                className: "user_item_ricon"
            }, React.createElement("div", {
                className: "user_item_icon_ct"
            }, React.createElement("img", {
                src: "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg"
            }))), React.createElement("div", {
                className: "user_item_inner"
            }, React.createElement("div", {
                className: "us_sel_item_name"
            }, this.state.username), React.createElement("div", {
                className: "us_sel_item_email"
            }, this.state.email)), React.createElement("div", {
                className: "item_radio_circle"
            }, React.createElement("div", {
                className: "item_radio_circle_pointer"
            })));
        }
    });
    return t;
});