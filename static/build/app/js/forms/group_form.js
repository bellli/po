define("app/js/forms/group_form", [ "jquery", "gallery/react/react", "appjs/forms/ConcatForm.js", "appjs/pane/Groups", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var t = require("appjs/forms/ConcatForm.js"), e = require("appjs/pane/Groups"), a = require("appjs/data/data_watch"), i = React.createClass({
        displayName: "GroupForm",
        getInitialState: function() {
            ob = {}, ob.action = this.props.action;
            var t = [ {
                type: "text",
                placeholder: "团队名称",
                name: "name"
            } ];
            return ob.ipt_items = t, ob.list = this.props.list || [], ob.is_show = this.props.is_show, 
            alive_group = !1, $(ob.list).each(function(t, e) {
                e.key = new Date().getTime() + Math.random(), alive_group = e.is_alive ? e.is_alive : alive_group;
            }), ob.alive_group = alive_group, $.extend(ob, {
                data: {}
            }), ob;
        },
        setList: function(t) {
            $(t).each(function(t, e) {
                e.key = new Date().getTime() + Math.random();
            }), this.setState({
                list: t
            });
        },
        handleChange: function(t) {
            var e = t.target || t.srcElement;
            e = $(e);
            var a = e.val(), i = e.attr("name");
            console.log(a);
            var s = {
                data: {}
            }, o = this.state.data;
            s.data[i] = a;
            var r = $.extend(o, s.data);
            console.log(r);
        },
        submit: function() {
            var t = this;
            $.ajax({
                url: t.state.action,
                type: t.props.type,
                dataType: "json",
                data: t.state.data,
                success: function(t) {
                    1 != t.status && alert(t.message);
                },
                error: function() {
                    alert("error");
                }
            });
        },
        out_hide: function() {
            var t = this;
            a.callback("group_form", 1, "hide", t);
        },
        componentDidMount: function() {
            var t = this;
            alive_group = !1, $.ajax({
                url: "/get_my_groups/",
                type: "POST",
                dataType: "json",
                success: function(e) {
                    var a = [];
                    $(e.groups).each(function(t, e) {
                        a.push({
                            title: e.name,
                            id: e.id,
                            is_alive: e.is_alive
                        }), alive_group = e.is_alive ? e.is_alive : alive_group;
                    }), groups = a, t.setState({
                        list: groups,
                        alive_group: alive_group
                    });
                },
                error: function() {}
            });
        },
        render: function() {
            for (var a = this, i = function(t, e) {
                a.props.handle_data(t, e, a);
            }, s = function(t, e) {
                a.props.handle_click && a.props.handle_click(t, e, a);
            }, o = [], r = 100; r >= 0; r--) o.push(React.createElement("li", null, "Polar"));
            var n = [];
            if (a.state.list.length) {
                var l = new Date().getTime() + Math.random();
                n.push(React.createElement(e, {
                    key: l,
                    title: "可选择团队列表",
                    handle_click: s,
                    num: "3",
                    list: a.state.list
                }));
            }
            var c = this.state.is_show ? "base_auth_form" : "base_auth_form hide", p = this.state.alive_group ? "close_pane_action" : "close_pane_action hide";
            return React.createElement("div", {
                className: c
            }, React.createElement("div", {
                className: p,
                onClick: this.out_hide
            }, React.createElement("div", {
                className: "plus_icon"
            })), React.createElement(t, {
                title: this.props.title,
                show_ot: !1,
                action: "/create_group/",
                type: "POST",
                exe: this.props.exe,
                handle_data: i,
                exchange_text: "",
                action_text: "创建",
                is_show: this.props.is_show,
                ipt_items: a.state.ipt_items
            }), n);
        }
    });
    return i;
});