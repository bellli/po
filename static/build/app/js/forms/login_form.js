define("app/js/forms/login_form", [ "jquery", "gallery/react/react", "appjs/forms/ConcatForm.js" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var t = require("appjs/forms/ConcatForm.js"), e = React.createClass({
        displayName: "SignForm",
        getInitialState: function() {
            ob = {}, ob.action = this.props.action;
            var t = [ {
                type: "text",
                placeholder: "邮箱",
                name: "email"
            }, {
                type: "text",
                placeholder: "密码",
                name: "password"
            } ];
            return ob.ipt_items = t, $.extend(ob, {
                data: {}
            }), ob;
        },
        handleChange: function(t) {
            var e = t.target || t.srcElement;
            e = $(e);
            var a = e.val(), n = e.attr("name");
            console.log(a);
            var r = {
                data: {}
            }, s = this.state.data;
            r.data[n] = a;
            var o = $.extend(s, r.data);
            console.log(o);
        },
        submit: function() {
            var t = this;
            $.ajax({
                url: t.state.action,
                type: t.props.type,
                dataType: "json",
                data: t.state.data,
                success: function(t) {
                    1 != t.status && alert(t.message);
                },
                error: function() {
                    alert("error");
                }
            });
        },
        render: function() {
            var e = this, a = function(t, a) {
                e.props.handle_data(t, a);
            };
            return React.createElement(t, {
                destext: "还没有账户？",
                title: "",
                action: "/login/",
                type: "POST",
                exe: this.props.exe,
                handle_data: a,
                exchange_text: "注册",
                action_text: "登录",
                is_show: this.props.is_show,
                ipt_items: e.state.ipt_items
            });
        }
    });
    return e;
});