define("app/js/forms/add_user_to_channel_form", [ "jquery", "gallery/react/react", "appjs/forms/ConcatForm.js", "appjs/pane/Groups", "appjs/data/data_watch", "appjs/forms/user_item_channel_form" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = (require("appjs/forms/ConcatForm.js"), require("appjs/pane/Groups"), require("appjs/data/data_watch")), t = require("appjs/forms/user_item_channel_form"), a = React.createClass({
        displayName: "ChannelForm",
        getInitialState: function() {
            var e = {
                id: 0,
                num: 0,
                icon: parseInt(10 * Math.random() / 2) ? "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg" : "https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg",
                name: "None",
                is_focus: !1,
                group_user_list: [ {
                    username: "bandon.li"
                }, {}, {}, {} ],
                selected_email_list: [],
                handle_click: function(e) {},
                update_state: function() {}
            }, t = this.props.data || {};
            for (var a in e) e[a] = t[a] || e[a];
            return $(e.group_user_list).each(function(e, t) {
                t.key = new Date().getTime() + Math.random();
            }), e;
        },
        setList: function(e) {
            $(e).each(function(e, t) {
                t.key = new Date().getTime() + Math.random();
            }), this.setState({
                list: e
            });
        },
        handleChange: function(e) {
            var t = e.target || e.srcElement;
            t = $(t);
            var a = t.val(), s = t.attr("name");
            console.log(a);
            var i = {
                data: {}
            }, n = this.state.data;
            i.data[s] = a;
            var c = $.extend(n, i.data);
            console.log(c);
        },
        update_user_to_channel: function() {
            var t = this;
            $.ajax({
                url: "/update_user_to_channel/",
                type: "POST",
                dataType: "json",
                data: {
                    channel: t.state.id,
                    users: t.state.selected_email_list.join(";")
                },
                success: function(t) {
                    e.callback("info_message", 1, "change", {}, function(e, t) {
                        e.show_text("保存成功");
                    });
                },
                error: function() {
                    alert("error");
                }
            });
        },
        load_data: function(e) {
            var t = this;
            $.ajax({
                url: "/get_alive_group_user/",
                type: "POST",
                dataType: "json",
                data: {
                    channel: e || t.state.id
                },
                success: function(e) {
                    var a = [];
                    $(e.group_user_list).each(function(e, t) {
                        t.key = new Date().getTime() + Math.random(), t.is_focus && a.push(t.email);
                    }), t.setState({
                        group_user_list: e.group_user_list,
                        selected_email_list: a
                    });
                },
                error: function() {}
            });
        },
        componentDidMount: function() {
            var t = this;
            t.load_data(), e && e.register("add_user_to_channel_form", 1, "change", function(e, a, s) {
                a && "function" == typeof a && a(t, e), t[s] && "function" == typeof t[s] && t[s]();
            });
        },
        add_email: function(e) {
            var t = e.keyCode, a = this;
            if (13 == t) {
                var s = $(a.ipt), i = {
                    username: "",
                    email: s.val(),
                    is_focus: !0,
                    key: new Date().getTime() + Math.random()
                };
                a.state.selected_email_list.push(s.val()), a.state.group_user_list.push(i), a.setState({
                    group_user_list: a.state.group_user_list,
                    selected_email_list: a.state.selected_email_list
                }), s.val("");
            }
        },
        out_hide: function() {
            e.callback("J_add_channel", 1, "hide");
        },
        render: function() {
            var e = this;
            console.log(this.state.selected_email_list);
            for (var a = [], s = this.state.group_user_list.length - 1; s >= 0; s--) {
                var i = this.state.group_user_list[s];
                i.selected = function(t, a) {
                    var s = e.state.selected_email_list, i = !1, n = -1;
                    $(s).each(function(e, a) {
                        a == t && (i = !0, n = e);
                    }), !a && i && s.splice(n, 1), a && !i && s.push(t), e.setState({
                        selected_email_list: s
                    }), console.log(s);
                }, a.push(React.createElement(t, {
                    data: i,
                    key: i.key
                }));
            }
            return React.createElement("div", {
                className: ""
            }, a, React.createElement("div", {
                className: "user_item"
            }, React.createElement("div", {
                className: "user_item_ricon"
            }, React.createElement("div", {
                className: "user_item_icon_ct invite_ipt_icon"
            })), React.createElement("div", {
                className: "user_item_inner"
            }, React.createElement("input", {
                className: "invite_ipt",
                ref: function(t) {
                    e.ipt = t;
                },
                value: this.state.add_email,
                onKeyUp: this.add_email,
                placeholder: "添加你需要邀请的朋友邮箱"
            })), React.createElement("div", {
                className: "item_radio_circle"
            }, React.createElement("div", {
                className: "item_radio_circle_pointer"
            }))), React.createElement("div", {
                className: "user_item user_item_focus add_com_mask_btn",
                onClick: this.update_user_to_channel
            }, "保存"));
        }
    });
    return a;
});