define("app/js/forms/MessageForm", [ "jquery", "gallery/react/react", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = require("appjs/data/data_watch"), t = React.createClass({
        displayName: "MessageForm",
        getInitialState: function() {
            var e = {
                id: 0,
                send_message: function() {
                    alert("m");
                },
                is_focus: !1
            }, t = this.props.data || {};
            for (var a in e) e[a] = t[a] || e[a];
            return e;
        },
        send_message: function(e) {
            var t = (e.keyCode, e.target || e.srcElement);
            t = $(t);
            var a = t.val();
            a && (this.state.send_message && "function" == typeof this.state.send_message && this.state.send_message(this.state.id, a), 
            this.clear());
        },
        clear: function() {
            this.input.value = "";
        },
        focus: function() {
            setTimeout(function() {}, 50);
        },
        componentDidMount: function() {
            var t = this;
            e && e.register("message_form", this.state.id, "change", function(e, a, s) {
                a && "function" == typeof a && a(t, e), t[s] && "function" == typeof t[s] && t[s]();
            }), $(t.input).on("change", function(e) {
                t.send_message(e);
            });
        },
        render: function() {
            var e = this;
            return React.createElement("div", {
                className: "msg_ipt_mg"
            }, React.createElement("div", {
                className: "msg_ipt_mg_inner_tools"
            }, React.createElement("input", {
                className: "msg_ipt_tool",
                ref: function(t) {
                    e.input = t;
                },
                placeholder: "说点什么吧..."
            }), React.createElement("div", {
                className: "msg_ipt_toos_ct"
            }, React.createElement("div", {
                className: "msg_ipt_toos_action "
            }, React.createElement("div", {
                className: "mgs_ipt_icon todo_ipt_icon"
            })), React.createElement("div", {
                className: "msg_ipt_toos_action "
            }, React.createElement("div", {
                className: "mgs_ipt_icon file_ipt_icon"
            })))));
        }
    });
    return t;
});