define("app/js/forms/channel_form", [ "jquery", "gallery/react/react", "appjs/forms/ConcatForm.js", "appjs/pane/Groups", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = require("appjs/forms/ConcatForm.js"), t = require("appjs/pane/Groups"), a = require("appjs/data/data_watch"), n = React.createClass({
        displayName: "ChannelForm",
        getInitialState: function() {
            ob = {}, ob.action = this.props.action;
            var e = [ {
                type: "text",
                placeholder: "频道名称",
                name: "name"
            }, {
                type: "text",
                placeholder: "用户名称以;分割",
                name: "users"
            }, {
                type: "text",
                placeholder: "简介",
                name: "brief"
            } ];
            return ob.ipt_items = e, ob.list = this.props.list || [], $(ob.list).each(function(e, t) {
                t.key = new Date().getTime() + Math.random();
            }), $.extend(ob, {
                data: {}
            }), ob;
        },
        setList: function(e) {
            $(e).each(function(e, t) {
                t.key = new Date().getTime() + Math.random();
            }), this.setState({
                list: e
            });
        },
        handleChange: function(e) {
            var t = e.target || e.srcElement;
            t = $(t);
            var a = t.val(), n = t.attr("name");
            console.log(a);
            var s = {
                data: {}
            }, c = this.state.data;
            s.data[n] = a;
            var r = $.extend(c, s.data);
            console.log(r);
        },
        submit: function() {
            var e = this;
            $.ajax({
                url: e.state.action,
                type: e.props.type,
                dataType: "json",
                data: e.state.data,
                success: function(e) {
                    a.callback("channel_form_result", 1, "change", e), 1 != e.status && alert(e.message);
                },
                error: function() {
                    alert("error");
                }
            });
        },
        out_hide: function() {
            a.callback("J_add_channel", 1, "hide");
        },
        render: function() {
            for (var n = this, s = function(e, t) {
                a.callback("channel_form_result", 1, "change", e), n.props.handle_data(e, t, n);
            }, c = function(e, t) {
                n.props.handle_click && n.props.handle_click(e, t, n);
            }, r = [], i = 100; i >= 0; i--) r.push(React.createElement("li", null, "Polar"));
            var l = [];
            if (n.state.list.length) {
                var o = new Date().getTime() + Math.random();
                l.push(React.createElement(t, {
                    key: o,
                    title: "频道列表",
                    handle_click: c,
                    num: "3",
                    list: n.state.list
                }));
            }
            var l = [];
            return React.createElement("div", {
                className: "base_auth_form"
            }, React.createElement("div", {
                className: "close_pane_action",
                onClick: this.out_hide
            }, React.createElement("div", {
                className: "plus_icon"
            })), React.createElement(e, {
                title: this.props.title,
                action: "/create_channel/",
                type: "POST",
                exe: this.props.exe,
                handle_data: s,
                exchange_text: "",
                action_text: "创建",
                is_show: this.props.is_show,
                ipt_items: n.state.ipt_items
            }), l);
        }
    });
    return n;
});