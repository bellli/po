define("app/js/forms/choose_date_to_do", [ "jquery", "gallery/react/react", "appjs/pane/to_do_textarea", "appjs/data/data_watch", "appjs/forms/choose_list_item" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var t = (require("appjs/pane/to_do_textarea"), require("appjs/data/data_watch")), e = (require("appjs/forms/choose_list_item"), 
    React.createClass({
        displayName: "ChooseDateTodo",
        getInitialState: function() {
            var t = {
                id: 0,
                num: 0,
                icon: parseInt(10 * Math.random() / 2) ? "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg" : "https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg",
                name: "None",
                is_show: !0,
                user_list: [ {}, {}, {}, {} ],
                seleted_emails: {},
                placeholder: "",
                key: new Date().getTime() + "*" + Math.random() + "*" + Math.random()
            }, e = this.props.data || {};
            for (var a in t) t[a] = e[a] || t[a];
            return t;
        },
        componentDidMount: function() {
            var e = this;
            t && t.register("choose_date_to_do", this.state.id, "change", function(t, a, s) {
                a && "function" == typeof a && a(e, t), e[s] && "function" == typeof e[s] && e[s]();
            });
            e.state.id;
            $(document).on("click", function(t) {
                var a = t.target || t.srcElement;
                $.contains(e.area, a) || (a === e.area ? e.toggle() : e.out());
            });
        },
        select: function() {
            var t = this, e = !t.state.select;
            t.setState({
                select: e
            }), setTimeout(function() {
                t.setState({
                    is_show: !1
                });
            }, 500);
        },
        close: function() {
            var t = this;
            t.setState({
                is_show: !1
            });
        },
        focus: function() {
            var t = this;
            t.list;
            t.setState({
                is_show_list: !0
            }), t.input.focus(), t.get_user_data();
        },
        get_user_data: function() {
            var t = this;
            t.state.seleted_emails;
        },
        out: function(t) {
            var e = this;
            e.list;
            e.setState({
                is_show_list: !1
            });
        },
        toggle: function() {
            var t = this, e = !t.state.is_show_list;
            t.setState({
                is_show_list: e
            });
        },
        toggle_select_email: function(t, e) {
            var a = this, s = a.state.seleted_emails;
            s[t] = e, console.log(s), a.setState({
                seleted_emails: s
            });
        },
        render: function() {
            var t = this, e = ({
                placeholder: this.state.placeholder || "设置到期日"
            }, this.state.is_show_list ? "down_ai_select" : "down_ai_select hide");
            return React.createElement("div", {
                className: "atd_item",
                ref: function(e) {
                    t.area = e;
                }
            }, React.createElement("div", {
                className: "atd_item_icon"
            }), React.createElement("div", {
                className: "atd_val_ct"
            }, React.createElement("textarea", {
                onFocus: t.focus,
                ref: function(e) {
                    t.input = e;
                },
                className: "input_text_area_normal",
                placeholder: this.state.placeholder
            })), React.createElement("div", {
                onClick: t.list_show,
                className: e,
                ref: function(e) {
                    t.list = e;
                }
            }));
        }
    }));
    return e;
});