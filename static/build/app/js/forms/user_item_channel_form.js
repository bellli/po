define("app/js/forms/user_item_channel_form", [ "jquery", "gallery/react/react", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = require("appjs/data/data_watch"), t = React.createClass({
        displayName: "ChannelItem",
        getInitialState: function() {
            var e = {
                id: 0,
                num: 0,
                icon: parseInt(10 * Math.random() / 2) ? "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg" : "https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg",
                username: "",
                email: "None",
                is_focus: !1,
                selected: function() {},
                handle_click: function(e) {},
                update_state: function() {}
            }, t = this.props.data || {};
            for (var a in e) e[a] = t[a] || e[a];
            return e;
        },
        resize: function() {
            alert("resize");
        },
        update_state: function() {
            this.state.update_state(this);
        },
        focus: function() {
            this.setState({
                is_focus: !0
            });
        },
        focus_out: function() {
            this.setState({
                is_focus: !1
            });
        },
        handle_click: function() {
            var e = this;
            e.state.is_focus ? (e.setState({
                is_focus: !1
            }), e.state.selected(e.state.email, !1)) : (e.setState({
                is_focus: !0
            }), e.state.selected(e.state.email, !0));
        },
        componentDidMount: function() {
            var t = this;
            e && e.register("group_user_list_item", this.state.id, "change", function(e, a, s) {
                a && "function" == typeof a && a(t, e), t[s] && "function" == typeof t[s] && t[s]();
            });
        },
        render: function() {
            var e = this, t = (e.props.main_el, this.state.num ? "user_info_num" : "user_info_num hide", 
            this.state.is_focus ? "user_item user_item_focus" : "user_item");
            return React.createElement("div", {
                className: t,
                onClick: this.handle_click
            }, React.createElement("div", {
                className: "user_item_ricon"
            }, React.createElement("div", {
                className: "user_item_icon_ct"
            }, React.createElement("img", {
                src: this.state.icon
            }))), React.createElement("div", {
                className: "user_item_inner"
            }, React.createElement("span", {
                className: "user_item_text"
            }, this.state.username, "  "), React.createElement("span", {
                className: "user_item_email"
            }, this.state.email)), React.createElement("div", {
                className: "item_radio_circle"
            }, React.createElement("div", {
                className: "item_radio_circle_pointer"
            })));
        }
    });
    return t;
});