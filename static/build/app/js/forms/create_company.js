define("app/js/forms/create_company", [ "jquery", "gallery/react/react" ], function(require, exports) {
    require("jquery");
    require("gallery/react/react");
    var t = React.createClass({
        displayName: "CompanyForm",
        getInitialState: function() {
            var t = {};
            t[this.props.data.name] = this.props.value;
            t["name"] = this.props.data.name;
            t["value"] = this.props.value;
            return t;
        },
        render: function() {
            var t = this;
            var e = function(e) {
                var a = e.target || e.srcElement;
                a = $(a);
                var r = a.val();
                var n = a.attr("name");
                var p = {};
                p[n] = r;
                t.setState(p);
                t.props.data.ipt_change(e, n, r);
                if (t.props.getState) {
                    t.props.getState(t.state);
                }
                return r;
            };
            return React.createElement("div", {
                className: "ipt_item"
            }, React.createElement("input", {
                type: this.props.data.type,
                onKeyUp: e,
                name: this.state.name,
                className: "input",
                placeholder: this.props.data.placeholder
            }));
        }
    });
    return t;
});