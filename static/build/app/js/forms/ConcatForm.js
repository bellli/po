define("app/js/forms/ConcatForm", [ "jquery", "gallery/react/react", "appjs/forms/InputItem" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var t = require("appjs/forms/InputItem"), e = React.createClass({
        displayName: "SignForm",
        getInitialState: function() {
            var t = {};
            t.action = this.props.action;
            var e = this.props.ipt_items;
            return $(e).each(function(t, e) {
                e.key = new Date().getTime() + "" + Math.random();
            }), t.title = this.props.title || "", t.ipt_items = e, t.destext = this.props.destext, 
            t.show_ot = "boolean" == typeof this.props.show_ot ? this.props.show_ot : !0, $.extend(t, {
                data: {}
            }), t;
        },
        show_error: function(t) {
            this.setState({
                is_error: !0,
                error_text: t
            });
        },
        handle_Change: function(t) {
            var e = t.target || t.srcElement;
            e = $(e);
            var a = e.val(), r = e.attr("name"), o = {
                data: {}
            }, s = this.state.data;
            o.data[r] = a;
            var i = $.extend(s, o.data);
            console.log(i);
        },
        submit: function(t) {
            t ? t.preventDefault() : "";
            var e = this.props.handle_data, a = this;
            $.ajax({
                url: a.state.action,
                type: a.props.type,
                dataType: "json",
                data: a.state.data,
                success: function(t) {
                    e && "function" == typeof e && e(t, a);
                },
                error: function() {
                    alert("error");
                }
            });
        },
        render: function() {
            for (var e = this, a = e.state.ipt_items, r = [], o = 0; o <= a.length - 1; o++) {
                console.log(a[o]), a[o].ipt_change = function(t, a, r) {
                    e.handle_Change(t);
                };
                var s = function(t) {
                    console.log(t);
                }, i = function() {
                    e.submit();
                }, n = React.createElement(t, {
                    onEnter: i,
                    getState: s,
                    data: a[o],
                    key: a[o].key
                });
                r.push(n);
            }
            var c = e.props.is_show ? "lg_form" : "lg_form hide", p = e.props.exchange_text || "", l = e.props.action_text || "", m = e.state.is_error ? "show_form_error" : "show_form_error hide", h = e.state.show_ot ? "box_form box_form_ot" : "box_form box_form_ot hide";
            return React.createElement("div", {
                className: c,
                formtype: "form"
            }, React.createElement("div", {
                className: "title"
            }, " ", e.state.title, " "), React.createElement("div", {
                className: m
            }, " ", e.state.error_text, " "), React.createElement("form", {
                action: this.props.action,
                type: this.props.type,
                onSubmit: this.submit
            }, React.createElement("div", {
                className: "box_form"
            }, r, React.createElement("div", {
                className: "ipt_item ipt_action_ct"
            }, React.createElement("a", {
                onClick: this.submit,
                className: "btn",
                href: "javascript:;"
            }, l))), React.createElement("div", {
                className: h
            }, React.createElement("div", {
                className: "ipt_item ipt_ot_item"
            }, React.createElement("span", null, e.state.destext), React.createElement("a", {
                href: "javascript:;",
                onClick: this.props.exe
            }, p)))));
        }
    });
    return e;
});