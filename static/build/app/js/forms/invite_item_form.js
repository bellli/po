define("app/js/forms/invite_item_form", [ "jquery", "gallery/react/react", "appjs/forms/ConcatForm.js", "appjs/pane/Groups", "appjs/data/data_watch", "appjs/forms/user_item_channel_form" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = (require("appjs/forms/ConcatForm.js"), require("appjs/pane/Groups"), require("appjs/data/data_watch")), t = require("appjs/forms/user_item_channel_form"), a = React.createClass({
        displayName: "ChannelForm",
        getInitialState: function() {
            var e = {
                id: 0,
                num: 0,
                icon: parseInt(10 * Math.random() / 2) ? "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg" : "https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg",
                name: "None",
                is_focus: !1,
                group_user_list: [ {
                    username: "bandon.li"
                }, {}, {}, {} ],
                selected_email_list: [],
                handle_click: function(e) {},
                update_state: function() {}
            }, t = this.props.data || {};
            for (var a in e) e[a] = t[a] || e[a];
            return $(e.group_user_list).each(function(e, t) {
                t.key = new Date().getTime() + Math.random();
            }), e;
        },
        setList: function(e) {
            $(e).each(function(e, t) {
                t.key = new Date().getTime() + Math.random();
            }), this.setState({
                list: e
            });
        },
        handleChange: function(e) {
            var t = e.target || e.srcElement;
            t = $(t);
            var a = t.val(), i = t.attr("name");
            console.log(a);
            var s = {
                data: {}
            }, n = this.state.data;
            s.data[i] = a;
            var c = $.extend(n, s.data);
            console.log(c);
        },
        close: function() {
            e.callback("add_user_to_channel_invite_box", 1, "change", {}, function(e) {
                e.setState({
                    is_show: !1
                });
            });
        },
        update_user_to_channel: function() {
            var t = this, a = $(t.title_ipt), i = $(t.brief_ipt);
            $.ajax({
                url: "/create_channel/",
                type: "POST",
                dataType: "json",
                data: {
                    name: a.val(),
                    users: t.state.selected_email_list.join(";"),
                    brief: i.val()
                },
                success: function(a) {
                    1 != a.status ? e.callback("info_message", 1, "change", {}, function(e, t) {
                        e.show_text(a.message);
                    }) : (e.callback("info_message", 1, "change", {}, function(e, t) {
                        e.show_text("创建成功");
                    }), t.close());
                },
                error: function() {
                    alert("error");
                }
            });
        },
        load_data: function(e) {
            var t = this;
            $.ajax({
                url: "/get_alive_group_user/",
                type: "POST",
                dataType: "json",
                data: {
                    channel: e || t.state.id
                },
                success: function(e) {
                    var a = [];
                    $(e.group_user_list).each(function(e, t) {
                        t.key = new Date().getTime() + Math.random(), t.is_focus && a.push(t.email);
                    }), t.setState({
                        group_user_list: e.group_user_list,
                        selected_email_list: a
                    });
                },
                error: function() {}
            });
        },
        add_email: function(e) {
            var t = e.keyCode, a = this;
            if (13 == t) {
                var i = $(a.ipt).val(), s = {
                    username: "",
                    email: i,
                    is_focus: !0,
                    key: new Date().getTime() + Math.random()
                };
                a.state.selected_email_list.push(i), a.state.group_user_list.push(s), a.setState({
                    group_user_list: a.state.group_user_list,
                    selected_email_list: a.state.selected_email_list
                }), $(a.ipt).val("");
            }
        },
        componentDidMount: function() {
            var t = this;
            t.load_data(), e && e.register("create_channel_form", 1, "change", function(e, a, i) {
                a && "function" == typeof a && a(t, e), t[i] && "function" == typeof t[i] && t[i]();
            });
        },
        out_hide: function() {
            e.callback("J_add_channel", 1, "hide");
        },
        render: function() {
            var e = this;
            console.log(this.state.selected_email_list);
            for (var a = [], i = this.state.group_user_list.length - 1; i >= 0; i--) {
                var s = this.state.group_user_list[i];
                s.selected = function(t, a) {
                    var i = e.state.selected_email_list, s = !1, n = -1;
                    $(i).each(function(e, a) {
                        a == t && (s = !0, n = e);
                    }), !a && s && i.splice(n, 1), a && !s && i.push(t), e.setState({
                        selected_email_list: i
                    }), console.log(i);
                }, a.push(React.createElement(t, {
                    data: s,
                    key: s.key
                }));
            }
            return React.createElement("div", {
                className: ""
            }, React.createElement("input", {
                className: "invite_ipt_title",
                placeholder: "频道名称",
                ref: function(t) {
                    e.title_ipt = t;
                }
            }), React.createElement("textarea", {
                className: "invite_ipt_title",
                placeholder: "频道主题",
                ref: function(t) {
                    e.brief_ipt = t;
                }
            }), a, React.createElement("div", {
                className: "user_item"
            }, React.createElement("div", {
                className: "user_item_ricon"
            }, React.createElement("div", {
                className: "user_item_icon_ct invite_ipt_icon"
            })), React.createElement("div", {
                className: "user_item_inner"
            }, React.createElement("input", {
                className: "invite_ipt",
                ref: function(t) {
                    e.ipt = t;
                },
                value: this.state.add_email,
                onKeyUp: this.add_email,
                placeholder: "输入邮箱，邀请朋友使用"
            })), React.createElement("div", {
                className: "item_radio_circle"
            }, React.createElement("div", {
                className: "item_radio_circle_pointer"
            }))), React.createElement("div", {
                className: "user_item user_item_focus add_com_mask_btn",
                onClick: this.update_user_to_channel
            }, "添加"));
        }
    });
    return a;
});