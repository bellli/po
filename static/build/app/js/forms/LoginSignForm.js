define("app/js/forms/LoginSignForm", [ "appjs/forms/sign_form.js", "appjs/forms/login_form.js", "gallery/react/react", "appjs/data/data_watch" ], function(require, exports) {
    var e = require("appjs/forms/sign_form.js"), s = require("appjs/forms/login_form.js");
    require("gallery/react/react");
    var t = require("appjs/data/data_watch"), a = React.createClass({
        displayName: "LoginSignForm",
        getInitialState: function() {
            var e = {
                show_sign: !1,
                show_login: !0,
                hide_all: !1
            };
            return e.show_sign = "boolean" == typeof this.props.show_sign ? this.props.show_sign : !1, 
            e.show_login = "boolean" == typeof this.props.show_login ? this.props.show_login : !0, 
            e.hide_all = "boolean" == typeof this.props.hide_all ? this.props.hide_all : !0, 
            e;
        },
        out_hide: function() {
            var e = this;
            t.callback("login_sign_pane", 1, "hide", e);
        },
        render: function() {
            var a = this, o = function() {
                a.state.show_sign ? a.setState({
                    show_sign: !1,
                    show_login: !0
                }) : a.setState({
                    show_sign: !0,
                    show_login: !1
                });
            }, i = function() {
                a.setState({
                    show_sign: !1,
                    show_login: !1,
                    hide_all: !0
                }), a.props.success && a.props.success();
            }, n = function(e, s) {
                t.callback("login_sign_result", 1, "data", e), 1 == e.status ? i() : s.show_error(e.message);
            }, l = a.state.hide_all ? "base_auth_form hide" : "base_auth_form ";
            return React.createElement("div", {
                className: l
            }, React.createElement("div", {
                className: "close_pane_action",
                onClick: this.out_hide
            }, React.createElement("div", {
                className: "plus_icon"
            })), React.createElement("div", {
                className: "auth_logo"
            }, React.createElement("span", {
                className: "auth_logo_icon"
            }, React.createElement("span", {
                className: "auth_logo_beta"
            }, "beta"))), React.createElement(e, {
                is_show: this.state.show_sign,
                exe: o,
                handle_data: n,
                title: "注册"
            }), React.createElement(s, {
                is_show: this.state.show_login,
                exe: o,
                handle_data: n,
                title: "登录"
            }));
        }
    });
    return a;
});