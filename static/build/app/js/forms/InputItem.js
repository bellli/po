define("app/js/forms/InputItem", [ "jquery", "gallery/react/react" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = React.createClass({
        displayName: "InputItem",
        getInitialState: function() {
            var e = {};
            return e[this.props.data.name] = this.props.value, e.name = this.props.data.name, 
            e.value = this.props.value, e;
        },
        render: function() {
            var e = this, t = function(t) {
                var a = t.target || t.srcElement;
                a = $(a);
                var r = a.val(), n = a.attr("name"), p = {};
                return p[n] = r, e.props.data.ipt_change(t, n, r), e.props.getState && e.props.getState(e.state), 
                13 == t.keyCode && e.props.onEnter, r;
            };
            return React.createElement("div", {
                className: "ipt_item"
            }, React.createElement("input", {
                type: this.props.data.type,
                onKeyUp: t,
                name: this.state.name,
                className: "input",
                placeholder: this.props.data.placeholder
            }));
        }
    });
    return e;
});