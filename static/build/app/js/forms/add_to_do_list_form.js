define("app/js/forms/add_to_do_list_form", [ "jquery", "gallery/react/react", "appjs/pane/to_do_textarea", "appjs/forms/choose_list_to_do", "appjs/forms/choose_date_to_do", "appjs/data/data_watch" ], function(require, exports) {
    require("jquery"), require("gallery/react/react");
    var e = require("appjs/pane/to_do_textarea"), t = require("appjs/forms/choose_list_to_do"), a = require("appjs/forms/choose_date_to_do"), c = require("appjs/data/data_watch"), s = React.createClass({
        displayName: "AddToToForm",
        getInitialState: function() {
            var e = {
                id: 0,
                num: 0,
                icon: parseInt(10 * Math.random() / 2) ? "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg" : "https://s-media-cache-ak0.pinimg.com/236x/c7/0d/95/c70d95b40b86209cd0a36c95d6ad5313.jpg",
                name: "None",
                is_show: !1,
                handle_click: function(e) {},
                update_state: function() {}
            }, t = this.props.data || {};
            for (var a in e) e[a] = t[a] || e[a];
            return e;
        },
        componentDidMount: function() {
            var e = this;
            c && c.register("to_do_form", this.state.id, "change", function(t, a, c) {
                a && "function" == typeof a && a(e, t), e[c] && "function" == typeof e[c] && e[c]();
            });
        },
        close: function() {
            var e = this;
            e.setState({
                is_show: !1
            });
        },
        render: function() {
            var c = this, s = c.state.is_show ? "add_to_do_list_form" : "add_to_do_list_form hide", i = {
                placeholder: "任务描述"
            }, n = {
                id: this.state.id
            }, d = {
                id: this.state.id,
                placeholder: "设置到期日"
            }, l = {
                id: this.state.id,
                placeholder: "设置提醒日期"
            };
            return React.createElement("div", {
                className: s
            }, React.createElement("div", {
                className: "close_pane_action",
                onClick: c.close
            }, React.createElement("div", {
                className: "plus_icon"
            })), React.createElement("div", {
                className: "atd_item"
            }, React.createElement("div", {
                className: "atd_item_icon"
            }), React.createElement("div", {
                className: "atd_val_ct"
            }, React.createElement(e, {
                placeholder: "任务描述",
                data: i
            })), React.createElement("div", {
                className: "down_ai_select hide"
            }, React.createElement("div", {
                className: "user_item user_item_focus duser_item"
            }, React.createElement("div", {
                className: "user_item_ricon"
            }, React.createElement("div", {
                className: "user_item_icon_ct"
            }, React.createElement("img", {
                src: "https://www.cdn.whatsapp.net/img/v4/icon-iphone.svg"
            }))), React.createElement("div", {
                className: "user_item_inner"
            }, React.createElement("div", {
                className: "us_sel_item_name"
            }, "bandonbandonbandon Li"), React.createElement("div", {
                className: "us_sel_item_email"
            }, "jon@a.com")), React.createElement("div", {
                className: "item_radio_circle"
            }, React.createElement("div", {
                className: "item_radio_circle_pointer"
            }))))), React.createElement(t, {
                data: n
            }), React.createElement(a, {
                data: d
            }), React.createElement(a, {
                data: l
            }), React.createElement("div", {
                className: "atd_item"
            }, React.createElement("div", {
                className: "atd_item_icon"
            }), React.createElement("div", {
                className: "atd_val_ct"
            }, React.createElement("textarea", {
                className: "atd_text_area",
                placeholder: "填写备注"
            }))), React.createElement("div", {
                className: "atd_item"
            }, React.createElement("div", {
                className: "atd_item_icon"
            }), React.createElement("div", {
                className: "atd_val_ct"
            }, React.createElement("textarea", {
                className: "atd_text_area",
                placeholder: "添加文件"
            }))));
        }
    });
    return s;
});