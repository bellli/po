define("app/js/index", [ "jquery" ], function(require, exports) {
    require("jquery");
    var e = 12;
    (function() {
        var n = window.screen.width < document.body.offsetWidth ? window.screen.width : document.body.offsetWidth;
        var t = n / 375 * 12;
        $("html").css({
            fontSize: t + "px"
        });
        e = n / 375 * 12;
    })();
});