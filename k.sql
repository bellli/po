-- MySQL dump 10.13  Distrib 5.7.12, for osx10.11 (x86_64)
--
-- Host: localhost    Database: po
-- ------------------------------------------------------
-- Server version	5.7.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Groups`
--

DROP TABLE IF EXISTS `Groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `creator` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups`
--

LOCK TABLES `Groups` WRITE;
/*!40000 ALTER TABLE `Groups` DISABLE KEYS */;
INSERT INTO `Groups` VALUES (1,'用户体验研究',1),(2,'slack研究',2),(3,'funk',1),(4,'bandon',3),(5,'bandon12',3),(6,'alice',4),(7,'bandon121212',9),(8,'kkkdd',10),(9,'fuck',11),(10,'3434',11),(11,'8882323',11),(12,'x',2);
/*!40000 ALTER TABLE `Groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groups_user`
--

DROP TABLE IF EXISTS `Groups_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(255) NOT NULL,
  `user` int(11) NOT NULL,
  `is_alive` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Groups_user_group_5e8edbc6_uniq` (`group`,`user`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups_user`
--

LOCK TABLES `Groups_user` WRITE;
/*!40000 ALTER TABLE `Groups_user` DISABLE KEYS */;
INSERT INTO `Groups_user` VALUES (1,'1',1,1),(2,'2',2,0),(3,'1',2,0),(4,'3',1,0),(5,'4',3,0),(6,'5',3,1),(7,'6',4,1),(8,'7',9,1),(9,'8',10,1),(10,'9',11,0),(11,'10',11,1),(12,'11',11,0),(13,'11',1,0),(14,'11',2,0),(15,'10',1,0),(16,'10',2,0),(17,'10',3,0),(18,'12',2,1),(19,'12',11,0);
/*!40000 ALTER TABLE `Groups_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Message`
--

DROP TABLE IF EXISTS `Message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` longtext NOT NULL,
  `channel` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `send_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Message`
--

LOCK TABLES `Message` WRITE;
/*!40000 ALTER TABLE `Message` DISABLE KEYS */;
INSERT INTO `Message` VALUES (1,'hi',1,1,'0000-00-00 00:00:00.000000'),(2,'hello',3,1,'0000-00-00 00:00:00.000000'),(3,'hi',3,1,'0000-00-00 00:00:00.000000'),(4,'fuck',3,1,'0000-00-00 00:00:00.000000'),(5,'iiwewewe',3,1,'2016-09-26 08:48:06.124328'),(6,'hi',1,1,'2016-09-26 08:51:01.289634'),(7,'hello',3,1,'2016-09-26 08:54:37.723630'),(8,'fuck',3,2,'2016-09-26 08:54:50.404343'),(9,'what can i do for u',3,2,'2016-09-26 08:54:56.215482'),(10,'what',3,1,'2016-09-26 09:29:34.930547'),(11,'can i',3,1,'2016-09-26 09:29:37.066866'),(12,'do for you',3,1,'2016-09-26 09:29:39.803475'),(13,'fuck',3,1,'2016-09-26 09:29:42.317497'),(14,'在吗',3,2,'2016-09-26 09:42:33.188900'),(15,'fuck',3,1,'2016-09-26 10:01:46.331761'),(16,'in?',3,2,'2016-09-26 10:02:45.266349'),(17,'核力量',3,2,'2016-09-26 10:05:15.501864'),(18,'7890oio',3,1,'2016-09-26 10:17:15.882010'),(19,'23232323232323232',3,1,'2016-09-26 10:17:21.344935'),(20,'232sdsdsdsdsdsdsdsdsdsdwewe',3,1,'2016-09-26 10:17:27.255689'),(21,'what',3,1,'2016-09-26 10:17:29.939597'),(22,'kkkkkk2323232323232323',3,1,'2016-09-26 10:17:35.429199'),(23,'899990002323232323232323',3,1,'2016-09-26 10:17:47.069588'),(24,'232323',1,2,'2016-09-26 10:38:04.915167'),(25,'a@a.com',11,2,'2016-09-26 10:38:15.332729'),(26,'232323',16,2,'2016-09-26 10:38:44.647554'),(27,'hi',18,1,'2016-09-26 10:43:34.643212'),(28,'what',18,1,'2016-09-26 10:43:38.766933'),(29,'can i',18,1,'2016-09-26 10:43:41.237375'),(30,'to',18,1,'2016-09-26 10:43:43.154835'),(31,'is it in ?',3,2,'2016-09-26 10:43:50.631351'),(32,'hello',3,2,'2016-09-26 10:47:11.350783'),(33,'is it in ?',3,1,'2016-09-26 10:47:51.028510'),(34,'hello',18,1,'2016-09-26 12:22:13.980393'),(35,'what can i do for u',18,1,'2016-09-26 12:22:19.765307'),(36,'fuck',18,1,'2016-09-26 12:22:21.934810'),(37,'fuck',3,1,'2016-09-26 13:52:10.497281'),(38,'are you IN',3,1,'2016-09-26 13:52:15.897172'),(39,'hello',26,11,'2016-09-26 15:47:31.451303'),(40,'what can i do for you?',26,11,'2016-09-26 15:47:36.869868'),(41,'hello',27,11,'2016-09-26 15:49:25.998991'),(42,'what',27,11,'2016-09-26 15:53:14.219714'),(43,'kkkkd',27,11,'2016-09-26 15:53:18.318225'),(44,'kkwewe',27,11,'2016-09-26 15:53:21.076528'),(45,'fuck',27,11,'2016-09-26 15:53:45.647558'),(46,'holy shit',27,11,'2016-09-26 15:53:52.177369'),(47,'2323',26,11,'2016-09-26 16:54:00.544720'),(48,'23232323',26,11,'2016-09-26 17:00:22.664461'),(49,'holy shit',26,11,'2016-09-26 17:25:48.997288'),(50,'fuck',54,11,'2016-09-26 17:25:54.603647'),(51,'fuck',57,11,'2016-09-26 17:26:07.076683'),(52,'fuck',26,2,'2016-09-26 17:28:51.018694'),(53,'这么多消息了',26,2,'2016-09-26 17:28:57.764793'),(54,'hi',33,11,'2016-09-26 17:29:28.749175'),(55,'wolegequ',33,11,'2016-09-26 17:29:34.764310'),(56,'hi',1,11,'2016-09-26 17:33:47.821036'),(57,'a',33,2,'2016-09-26 19:17:03.706718'),(58,'we',33,2,'2016-09-26 19:17:08.047879'),(59,'232323',33,2,'2016-09-26 19:17:10.152568'),(60,'sdfsdfds',33,2,'2016-09-26 19:17:12.083855'),(61,'bandon',26,11,'2016-09-26 19:17:51.676694'),(62,'hello',26,11,'2016-09-26 19:19:46.537012'),(63,'hello',26,11,'2016-09-26 19:20:15.370880'),(64,'what can i do for you',26,11,'2016-09-26 19:20:20.966706'),(65,'maybe',26,11,'2016-09-26 19:20:28.461379'),(66,'u a r',26,11,'2016-09-26 19:20:33.278727'),(67,'890',26,2,'2016-09-26 19:22:55.377304'),(68,'1989',26,2,'2016-09-26 19:22:57.608791'),(69,'12 ',26,2,'2016-09-26 19:23:00.887451'),(70,'26',26,2,'2016-09-26 19:23:02.480371'),(71,'我们请您再次检查（或者再三检查）您的电话是有良好的互联网连接信号。请先尝试打开一个网站页面检查。如果您十分确定您的电话已连接成功（尝试连接到不同的 Wi-Fi 热点或者 4G），请继续阅读。  检查您手机的日期和时间设置正确。如果手机的日期或时间不对，那您就有可能未能连接到我们的服务器来下载您的媒体档案。因此，请确保您已正确的设置了手机的日期和时间。有关手机日期和时间设定的步骤，请浏览此页。',26,2,'2016-09-26 19:23:44.612622'),(72,'how',26,11,'2016-09-26 19:24:36.070391'),(73,'do',26,11,'2016-09-26 19:24:37.187291'),(74,'u',26,11,'2016-09-26 19:24:38.574631'),(75,'do',26,11,'2016-09-26 19:24:39.416429'),(76,'hello',26,11,'2016-09-26 19:30:12.498445'),(77,'what can i do for u',26,11,'2016-09-26 19:31:08.767105'),(78,'el',33,11,'2016-09-26 19:47:08.440518'),(79,'hello',59,11,'2016-09-26 19:53:46.500355'),(80,'hello',61,11,'2016-09-26 19:54:22.767582'),(81,'hi',64,11,'2016-09-26 20:01:12.735808'),(82,'kkkk',63,2,'2016-09-26 20:01:20.281863'),(83,'hello',63,2,'2016-09-26 20:04:43.290922'),(84,'sdsdsd',63,2,'2016-09-26 20:04:56.031791'),(85,'sdsdsd',63,2,'2016-09-26 20:04:57.041021'),(86,'232323',63,2,'2016-09-26 20:04:58.524498'),(87,'is it ok ?',65,2,'2016-09-26 20:05:51.981289');
/*!40000 ALTER TABLE `Message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add groups',1,'add_groups'),(2,'Can change groups',1,'change_groups'),(3,'Can delete groups',1,'delete_groups'),(4,'Can add group_user',2,'add_group_user'),(5,'Can change group_user',2,'change_group_user'),(6,'Can delete group_user',2,'delete_group_user'),(7,'Can add channel',3,'add_channel'),(8,'Can change channel',3,'change_channel'),(9,'Can delete channel',3,'delete_channel'),(10,'Can add message',4,'add_message'),(11,'Can change message',4,'change_message'),(12,'Can delete message',4,'delete_message'),(13,'Can add channel_user',5,'add_channel_user'),(14,'Can change channel_user',5,'change_channel_user'),(15,'Can delete channel_user',5,'delete_channel_user'),(16,'Can add channel_message',6,'add_channel_message'),(17,'Can change channel_message',6,'change_channel_message'),(18,'Can delete channel_message',6,'delete_channel_message'),(19,'Can add message_status',7,'add_message_status'),(20,'Can change message_status',7,'change_message_status'),(21,'Can delete message_status',7,'delete_message_status'),(22,'Can add log entry',8,'add_logentry'),(23,'Can change log entry',8,'change_logentry'),(24,'Can delete log entry',8,'delete_logentry'),(25,'Can add permission',9,'add_permission'),(26,'Can change permission',9,'change_permission'),(27,'Can delete permission',9,'delete_permission'),(28,'Can add group',10,'add_group'),(29,'Can change group',10,'change_group'),(30,'Can delete group',10,'delete_group'),(31,'Can add user',11,'add_user'),(32,'Can change user',11,'change_user'),(33,'Can delete user',11,'delete_user'),(34,'Can add session',12,'add_session'),(35,'Can change session',12,'change_session'),(36,'Can delete session',12,'delete_session'),(37,'Can add content type',13,'add_contenttype'),(38,'Can change content type',13,'change_contenttype'),(39,'Can delete content type',13,'delete_contenttype');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$24000$Gi5HAQ7TUK56$vY1B6+0aNBzdRgbPkj3uZMoY2UVK1ytutsHsrOEyPVs=','2016-09-26 15:20:27.785540',0,'bandon','','','a@a.com',0,1,'2016-09-26 08:32:28.564575'),(2,'pbkdf2_sha256$24000$W50UG2qHCtCO$qgJ7mjMbZTyXYBI5Wv3kNA6/guZDVrg9r9abTptBnzI=','2016-09-26 08:32:57.132493',0,'a2','','','a2@a.com',0,1,'2016-09-26 08:32:56.820249'),(3,'pbkdf2_sha256$24000$LEIxQzOdOYu9$hR6bUgmXC9gAidD+tXGNNgM0jeUqmrjSUYK30AFpffM=','2016-09-26 15:20:59.643615',0,'b2@b.com','','','b2@b.com',0,1,'2016-09-26 15:20:59.316667'),(4,'pbkdf2_sha256$24000$9ZhcPioE6juF$MfFFenetEVpRCKPYY3Vpnl30kJc0I3nZPh8bPsGfrPA=','2016-09-26 15:30:16.940651',0,'bb','','','bb@b.com',0,1,'2016-09-26 15:30:16.558928'),(5,'pbkdf2_sha256$24000$WCbQj8ogeBBV$sGCyIXcEBLHL+zoye+sDOKWvObxL6aLTLKCWr0xY7nY=','2016-09-26 15:31:23.597380',0,'yu','','','yu@b.com',0,1,'2016-09-26 15:31:23.255738'),(6,'pbkdf2_sha256$24000$h5TGnRgATCqO$Fkd0XvT4GAWyXqRhiZfYMuROJXfRRrPCufYf2BXFQdc=','2016-09-26 15:33:33.351262',0,'bbb','','','b3@b.com',0,1,'2016-09-26 15:33:32.983971'),(7,'pbkdf2_sha256$24000$MdQoNDrUhui5$HXziRw2+TxM3M1OSPha8DW/k53IBucAx4+jfd6P6AzA=','2016-09-26 15:34:51.018121',0,'56d','','','2323@b.com',0,1,'2016-09-26 15:34:50.631627'),(8,'pbkdf2_sha256$24000$c5VtlICJ8bIH$A7Uv5X2H/USw1iqMBmxwUroh57jbCs35tEkda8GO8I0=','2016-09-26 15:35:41.183522',0,'a222','','','a23@a.com',0,1,'2016-09-26 15:35:40.844213'),(9,'pbkdf2_sha256$24000$owssy32iX1QH$vz8V/V8iUKRIovXyhtG8E4d2W4HypYxD6fQyep5Kxrc=','2016-09-26 15:41:45.545657',0,'bbd@b.com','','','bandon12@b.com',0,1,'2016-09-26 15:41:45.028991'),(10,'pbkdf2_sha256$24000$zqgu5Ysax2d8$j4oZffQ9AYZTad2BlNdrqD81M3FKesLXbJPBNew+nN0=','2016-09-26 15:43:31.667416',0,'abc@b.com','','','bddd@b.com',0,1,'2016-09-26 15:43:31.248691'),(11,'pbkdf2_sha256$24000$KmwR4uSs5oRZ$WGx78czHrLgnrLz79cc+q2HrUwZW0i54bxHpaEOemeI=','2016-09-26 15:46:47.830202',0,'a@kd','','','adk@a.com',0,1,'2016-09-26 15:46:47.401945');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel`
--

DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `group` int(11) NOT NULL,
  `is_private` int(11) NOT NULL,
  `creator` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `brief` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `channel_creator_1f0b5dfa_uniq` (`creator`,`name`,`group`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel`
--

LOCK TABLES `channel` WRITE;
/*!40000 ALTER TABLE `channel` DISABLE KEYS */;
INSERT INTO `channel` VALUES (1,'all',1,0,1,'2016-09-26 08:33:40.933473','包含所有团队成员'),(2,'all',2,0,2,'2016-09-26 08:34:00.460091','包含所有团队成员'),(3,'whatsapp 技术研究',1,0,1,'2016-09-26 08:35:10.998239','用户可在电脑桌面上使用 WhatsApp 应用程序。如要安装 WhatsApp 桌面版本，请使用电脑浏览器前往至官方网页来进行安装。安装 WhatsApp 桌面版本最低的电脑操作系统版本要求是 Windows 8 或 Mac OSX 10.9，用户必须使用这些（或更新的）系统版本才可以安装 WhatsApp 到电脑上。至于其馀的操作系统或系统版本，请按此使用 WhatsApp 网页版。'),(5,'all',3,0,1,'2016-09-26 08:57:18.218510','包含所有团队成员'),(6,'es6 基础技术',1,0,1,'2016-09-26 10:29:27.089515','es6 基础技术'),(8,'es6 基础技术2',1,0,1,'2016-09-26 10:29:44.753489','es6 基础技术'),(10,'',1,0,1,'2016-09-26 10:32:51.470917','lddd'),(11,'李波2323',1,0,1,'2016-09-26 10:33:19.731232','lddd'),(13,'李波23232323',1,0,1,'2016-09-26 10:33:42.853987','lddd'),(14,'李波232323232323',1,0,1,'2016-09-26 10:35:34.738556','lddd'),(16,'b2323',1,0,1,'2016-09-26 10:36:21.203831','kkkkkkk'),(18,'is it in ?',1,0,1,'2016-09-26 10:39:02.037080','kkkkkkk'),(20,'all',4,0,3,'2016-09-26 15:24:05.771312','包含所有团队成员'),(21,'all',5,0,3,'2016-09-26 15:24:08.558822','包含所有团队成员'),(22,'all',6,0,4,'2016-09-26 15:30:24.296638','包含所有团队成员'),(23,'all',7,0,9,'2016-09-26 15:42:05.947188','包含所有团队成员'),(24,'all',8,0,10,'2016-09-26 15:43:36.287641','包含所有团队成员'),(25,'all',9,0,11,'2016-09-26 15:47:03.524004','包含所有团队成员'),(26,'all',10,0,11,'2016-09-26 15:47:20.562844','包含所有团队成员'),(27,'all',11,0,11,'2016-09-26 15:49:14.332628','包含所有团队成员'),(28,'bbb',11,0,11,'2016-09-26 16:28:26.793368','sdfdsfsd'),(29,'bbbb@b.com',11,0,11,'2016-09-26 16:29:12.135949','23423423423'),(31,'2323kkddd',11,0,11,'2016-09-26 16:33:18.086032','23232323'),(33,'a2',10,0,11,'2016-09-26 16:37:29.888526','232323'),(34,'232323',10,0,11,'2016-09-26 16:38:50.303215','242343243'),(35,'2323',10,0,11,'2016-09-26 16:47:44.822349','23232323'),(36,'232dddd',10,0,11,'2016-09-26 16:48:55.887912','23232323'),(37,'dddd',10,0,11,'2016-09-26 16:50:13.345961','2323232323'),(38,'ererer',10,0,11,'2016-09-26 16:51:18.913245','43534543'),(39,'232sddd',10,0,11,'2016-09-26 16:54:49.591885','bnadsfsdfs2323'),(40,'alice',10,0,11,'2016-09-26 16:56:07.665385','sdfsdfsfds'),(42,'232323fff',10,0,11,'2016-09-26 17:02:46.744618','2323dfdf'),(43,'5672323',10,0,11,'2016-09-26 17:06:09.586162','2323423432'),(44,'woheni',10,0,11,'2016-09-26 17:07:19.829317','232sdsdsd'),(45,'w1',10,0,11,'2016-09-26 17:09:16.660404','3232323'),(46,'w2',10,0,11,'2016-09-26 17:10:36.225988','sfdsfsdfds'),(47,'w333_____________',10,0,11,'2016-09-26 17:11:40.737666','2323232323'),(51,'232323fffGGG',10,0,11,'2016-09-26 17:16:28.210455','232323'),(52,'sdfdsfds',10,0,11,'2016-09-26 17:17:23.018193','23232323'),(53,'tyyy',10,0,11,'2016-09-26 17:18:52.311968','werewrew'),(54,'wg',10,0,11,'2016-09-26 17:20:23.096127','bsdfsdfsdfsd'),(55,'wg2',10,0,11,'2016-09-26 17:22:25.107134','232323'),(56,'222dd',10,0,11,'2016-09-26 17:23:50.265297','bandsdfds'),(57,'2ff',10,0,11,'2016-09-26 17:25:11.398562','bsdfsdfsd'),(58,'ali2',10,0,11,'2016-09-26 19:46:33.059044','我们请您再次检查（或者再三检查）您的电话是有良好的互联网连接信号。请先尝试打开一个网站页面检查。如果您十分确定您的电话已连接成功（尝试连接到不同的 Wi-Fi 热点或者 4G），请继续阅读。  检查您手机的日期和时间设置正确。如果手机的日期或时间不对，那您就有可能未能连接到我们的服务器来下载您的媒体档案。因此，请确保您已正确的设置了手机的日期和时间。有关手机日期和时间设定的步骤，请浏览此页。'),(59,'a2ss',10,0,11,'2016-09-26 19:52:10.637337','alice'),(60,'a2ss232323',10,0,11,'2016-09-26 19:53:36.628063','alice'),(61,'fuck_test',10,0,11,'2016-09-26 19:54:15.422516','alice'),(62,'all',12,0,2,'2016-09-26 19:57:55.707763','包含所有团队成员'),(63,'dsdsd',12,0,2,'2016-09-26 19:59:44.926233','232323'),(64,'23dsdsd23',12,0,2,'2016-09-26 20:00:00.449848','还要继续阅读？'),(65,'bingo',12,0,2,'2016-09-26 20:05:28.633803','还要继续阅读？');
/*!40000 ALTER TABLE `channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_message`
--

DROP TABLE IF EXISTS `channel_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` int(11) NOT NULL,
  `message` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_message`
--

LOCK TABLES `channel_message` WRITE;
/*!40000 ALTER TABLE `channel_message` DISABLE KEYS */;
INSERT INTO `channel_message` VALUES (1,3,1,'2016-09-26 08:36:05.980647'),(2,3,2,'2016-09-26 08:38:17.483755'),(3,3,3,'2016-09-26 08:41:10.230978'),(4,3,4,'2016-09-26 08:41:29.088377'),(5,3,5,'2016-09-26 08:48:06.129675'),(6,1,6,'2016-09-26 08:51:01.293848'),(7,3,7,'2016-09-26 08:54:37.737063'),(8,3,8,'2016-09-26 08:54:50.414794'),(9,3,9,'2016-09-26 08:54:56.217270'),(10,3,10,'2016-09-26 09:29:34.933532'),(11,3,11,'2016-09-26 09:29:37.076977'),(12,3,12,'2016-09-26 09:29:39.813552'),(13,3,13,'2016-09-26 09:29:42.327598'),(14,3,14,'2016-09-26 09:42:33.190923'),(15,3,15,'2016-09-26 10:01:46.341966'),(16,3,16,'2016-09-26 10:02:45.270048'),(17,3,17,'2016-09-26 10:05:15.505590'),(18,3,18,'2016-09-26 10:17:15.893116'),(19,3,19,'2016-09-26 10:17:21.354575'),(20,3,20,'2016-09-26 10:17:27.265446'),(21,3,21,'2016-09-26 10:17:29.949483'),(22,3,22,'2016-09-26 10:17:35.438844'),(23,3,23,'2016-09-26 10:17:47.078790'),(24,1,24,'2016-09-26 10:38:04.921151'),(25,11,25,'2016-09-26 10:38:15.343232'),(26,16,26,'2016-09-26 10:38:44.657481'),(27,18,27,'2016-09-26 10:43:34.655040'),(28,18,28,'2016-09-26 10:43:38.776093'),(29,18,29,'2016-09-26 10:43:41.246378'),(30,18,30,'2016-09-26 10:43:43.156966'),(31,3,31,'2016-09-26 10:43:50.641328'),(32,3,32,'2016-09-26 10:47:11.362949'),(33,3,33,'2016-09-26 10:47:51.038855'),(34,18,34,'2016-09-26 12:22:13.987740'),(35,18,35,'2016-09-26 12:22:19.768833'),(36,18,36,'2016-09-26 12:22:21.936699'),(37,3,37,'2016-09-26 13:52:10.505892'),(38,3,38,'2016-09-26 13:52:15.899267'),(39,26,39,'2016-09-26 15:47:31.454005'),(40,26,40,'2016-09-26 15:47:36.873459'),(41,27,41,'2016-09-26 15:49:26.001804'),(42,27,42,'2016-09-26 15:53:14.223448'),(43,27,43,'2016-09-26 15:53:18.321701'),(44,27,44,'2016-09-26 15:53:21.078643'),(45,27,45,'2016-09-26 15:53:45.650623'),(46,27,46,'2016-09-26 15:53:52.179500'),(47,26,47,'2016-09-26 16:54:00.553518'),(48,26,48,'2016-09-26 17:00:22.669694'),(49,26,49,'2016-09-26 17:25:49.000998'),(50,54,50,'2016-09-26 17:25:54.606871'),(51,57,51,'2016-09-26 17:26:07.078871'),(52,26,52,'2016-09-26 17:28:51.023539'),(53,26,53,'2016-09-26 17:28:57.766557'),(54,33,54,'2016-09-26 17:29:28.759545'),(55,33,55,'2016-09-26 17:29:34.777473'),(56,1,56,'2016-09-26 17:33:47.830890'),(57,33,57,'2016-09-26 19:17:03.714743'),(58,33,58,'2016-09-26 19:17:08.051444'),(59,33,59,'2016-09-26 19:17:10.156252'),(60,33,60,'2016-09-26 19:17:12.086749'),(61,26,61,'2016-09-26 19:17:51.681669'),(62,26,62,'2016-09-26 19:19:46.541208'),(63,26,63,'2016-09-26 19:20:15.374652'),(64,26,64,'2016-09-26 19:20:20.969774'),(65,26,65,'2016-09-26 19:20:28.464980'),(66,26,66,'2016-09-26 19:20:33.282975'),(67,26,67,'2016-09-26 19:22:55.379281'),(68,26,68,'2016-09-26 19:22:57.610707'),(69,26,69,'2016-09-26 19:23:00.889545'),(70,26,70,'2016-09-26 19:23:02.482597'),(71,26,71,'2016-09-26 19:23:44.617226'),(72,26,72,'2016-09-26 19:24:36.072388'),(73,26,73,'2016-09-26 19:24:37.189701'),(74,26,74,'2016-09-26 19:24:38.576310'),(75,26,75,'2016-09-26 19:24:39.419953'),(76,26,76,'2016-09-26 19:30:12.511662'),(77,26,77,'2016-09-26 19:31:08.771534'),(78,33,78,'2016-09-26 19:47:08.443444'),(79,59,79,'2016-09-26 19:53:46.510418'),(80,61,80,'2016-09-26 19:54:22.770501'),(81,64,81,'2016-09-26 20:01:12.737635'),(82,63,82,'2016-09-26 20:01:20.284070'),(83,63,83,'2016-09-26 20:04:43.296389'),(84,63,84,'2016-09-26 20:04:56.034527'),(85,63,85,'2016-09-26 20:04:57.042530'),(86,63,86,'2016-09-26 20:04:58.529713'),(87,65,87,'2016-09-26 20:05:51.983461');
/*!40000 ALTER TABLE `channel_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_user`
--

DROP TABLE IF EXISTS `channel_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `is_in_channel` int(11) NOT NULL,
  `is_in_group` int(11) NOT NULL,
  `message_num` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `channel_user_channel_e71b17b4_uniq` (`channel`,`user`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_user`
--

LOCK TABLES `channel_user` WRITE;
/*!40000 ALTER TABLE `channel_user` DISABLE KEYS */;
INSERT INTO `channel_user` VALUES (1,1,1,1,1,1),(2,2,2,1,1,0),(3,3,1,1,1,0),(4,3,2,1,1,0),(5,5,1,1,1,0),(6,11,1,1,1,0),(7,11,2,1,1,0),(8,13,1,1,1,0),(9,13,2,1,1,0),(10,14,1,1,1,0),(11,14,2,1,1,0),(12,1,2,1,1,1),(13,16,1,1,1,0),(14,16,2,1,1,0),(15,18,1,1,1,0),(16,20,3,1,1,0),(17,21,3,1,1,0),(18,22,4,1,1,0),(19,23,9,1,1,0),(20,24,10,1,1,0),(21,25,11,1,1,0),(22,26,11,1,1,0),(23,27,11,1,1,0),(24,28,1,1,1,0),(25,28,2,1,1,0),(26,28,11,1,1,0),(27,27,1,1,1,0),(28,27,2,1,1,0),(29,29,1,1,1,0),(30,29,2,1,1,0),(31,29,11,1,1,0),(32,31,1,1,1,0),(33,31,2,1,1,0),(34,31,11,1,1,0),(35,33,1,1,1,7),(36,33,2,1,1,0),(37,33,11,1,1,0),(38,26,1,1,1,22),(39,26,2,1,1,0),(40,34,1,1,1,0),(41,34,2,1,1,0),(42,34,11,1,1,0),(43,35,1,1,1,0),(44,35,11,1,1,0),(45,36,1,1,1,0),(46,36,11,1,1,0),(47,37,1,1,1,0),(48,37,3,1,1,0),(49,37,11,1,1,0),(50,26,3,1,1,22),(51,38,1,1,1,0),(52,38,11,1,1,0),(53,39,1,1,1,0),(54,39,11,1,1,0),(55,40,1,1,1,0),(56,40,11,1,1,0),(57,42,1,1,1,0),(58,42,11,1,1,0),(59,43,1,1,1,0),(60,43,11,1,1,0),(61,44,1,1,1,0),(62,44,11,1,1,0),(63,45,1,1,1,0),(64,45,11,1,1,0),(65,46,1,1,1,0),(66,46,11,1,1,0),(67,47,1,1,1,0),(68,47,11,1,1,0),(69,51,1,1,1,0),(70,51,11,1,1,0),(71,52,1,1,1,0),(72,52,11,1,1,0),(73,53,1,1,1,0),(74,53,11,1,1,0),(75,54,1,1,1,1),(76,54,11,1,1,0),(77,55,1,1,1,0),(78,55,11,1,1,0),(79,56,1,1,1,0),(80,56,11,1,1,0),(81,57,1,1,1,1),(82,57,11,1,1,0),(83,58,2,1,1,0),(84,58,11,1,1,0),(85,59,11,1,1,0),(86,60,2,1,1,0),(87,60,11,1,1,0),(88,61,2,1,1,0),(89,61,11,1,1,0),(90,62,2,1,1,0),(91,63,2,1,1,0),(92,63,11,1,1,0),(93,62,11,1,1,0),(94,64,2,1,1,0),(95,64,11,1,1,0),(96,65,2,1,1,0),(97,65,11,1,1,0);
/*!40000 ALTER TABLE `channel_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (8,'admin','logentry'),(10,'auth','group'),(9,'auth','permission'),(11,'auth','user'),(3,'channel','channel'),(6,'channel','channel_message'),(5,'channel','channel_user'),(1,'channel','groups'),(2,'channel','group_user'),(4,'channel','message'),(7,'channel','message_status'),(13,'contenttypes','contenttype'),(12,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2016-09-26 08:32:02.275079'),(2,'auth','0001_initial','2016-09-26 08:32:02.555969'),(3,'admin','0001_initial','2016-09-26 08:32:02.609048'),(4,'admin','0002_logentry_remove_auto_add','2016-09-26 08:32:02.639691'),(5,'contenttypes','0002_remove_content_type_name','2016-09-26 08:32:02.715023'),(6,'auth','0002_alter_permission_name_max_length','2016-09-26 08:32:02.746617'),(7,'auth','0003_alter_user_email_max_length','2016-09-26 08:32:02.775426'),(8,'auth','0004_alter_user_username_opts','2016-09-26 08:32:02.790330'),(9,'auth','0005_alter_user_last_login_null','2016-09-26 08:32:02.818987'),(10,'auth','0006_require_contenttypes_0002','2016-09-26 08:32:02.821995'),(11,'auth','0007_alter_validators_add_error_messages','2016-09-26 08:32:02.838759'),(12,'channel','0001_initial','2016-09-26 08:32:02.998835'),(13,'sessions','0001_initial','2016-09-26 08:32:03.030904'),(14,'channel','0002_message_user','2016-09-26 08:40:20.503706'),(15,'channel','0003_message_send_time','2016-09-26 08:47:27.461044');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('2syu7vxmgp6siobl3n91188bhpepb6mb','NzJmMTFlNjM1MGEzNDRjN2MwMGU4N2Q3NmU1M2YwMDJjOWY5YTA1Mjp7Il9hdXRoX3VzZXJfaGFzaCI6IjdlMjI1Y2Y2NDljY2YzMTJjYTg0ZTdkY2YzZWE5MmI4MTk0NGI0MTQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2016-10-10 15:19:43.450047'),('4i06h7algn198y4vrj0y0h0u0n2ax0ky','ZDE2ZGE1NTdmOTQzMzg0YTRhNTBlMTc5Nzk1MmM3MjY2M2E1YzMxNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjRlNmE1OTc1YWFhYjU3NzU5MjM1N2NkYTg3NjA0OWE4YTZmYTJmZjYiLCJfYXV0aF91c2VyX2lkIjoiNyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2016-10-10 15:35:02.609533'),('5r277vh8mga2yobhz0zwnlwwe9t5ioz9','NTg1OGE3ODQ0M2NlNTg3MjgwOWQ1NGQ0NjlmZDQ5MDdmMjViN2ExNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjdlMjI1Y2Y2NDljY2YzMTJjYTg0ZTdkY2YzZWE5MmI4MTk0NGI0MTQiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2016-10-10 15:20:27.970734'),('8e3fdkho3i5pv79ujfz6aemac6pnsp5i','MjE4Mjk5Mjc4MTdmODVhMzU0MDViZTQyNzkyNGVkYzZkZjc3MzcxMDp7Il9hdXRoX3VzZXJfaGFzaCI6IjUwNzE3OTU3YzlmODRjZDNjNzE4MjM3YzVkZjFiNmY3MDdhYjQ3NTMiLCJfYXV0aF91c2VyX2lkIjoiMyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2016-10-10 15:29:45.685586'),('9g8h8ucsbqx8pnznlb4gwxbfgy3goscz','MzAyYWVhZmE2MzUyY2YxYzczZWUyNmRhYTA4ZDZhN2U4N2UwZDE0Yzp7Il9hdXRoX3VzZXJfaGFzaCI6IjM1NjFiZmYxNDc1ZTU3NGNhYWEwNzllZjk2Zjc3ZTZiYzUwZGMwNDYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI2In0=','2016-10-10 15:34:30.118397'),('amc1773pqcatnj81ieipripujlhusovv','NmUxNjZhZjFlNThhZGFkY2E5NDdhMDE0ZWFhMmFkNTczNTc3MDc0ODp7Il9hdXRoX3VzZXJfaGFzaCI6IjBmZDM0MDY4N2EzNGYxYWFiOWMwNzBhMjgyM2ZhZGY2YTBkMmFkYjEiLCJfYXV0aF91c2VyX2lkIjoiOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2016-10-10 15:35:41.216295'),('b1v4ja51ty1ovnhqy3dmtqra7qn1q7hc','MjQxYzRlZDk2ZGJjYTUzY2EzN2E3MzUxMmNjM2RhOGNkNjIyOTY3Mjp7Il9hdXRoX3VzZXJfaGFzaCI6IjJhN2Q5MTdkYjk0YWU1OTY4NTY4YWM0MTA4NDJmYmJhYTViZjZkYzAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxMSJ9','2016-10-10 20:28:17.761476'),('boqjhjos7k7wazjd35707dkbwb9nu3ah','YmFkZjJmM2RhYmYyYTcxMjRiYjI3Yjk3MTQ4NGEzNmE2NTUwNzk5Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjE0ZjI4NDJlYjMwYjM1NGNhZjY5ZWM3YzFiMGQyZTk0ZGUwNTJjOWQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0In0=','2016-10-10 15:30:54.086346'),('dxyjmnuejnhabp1wnnfuglezwewbqdn4','MDk4MWQ3Yzk5ZmIxM2UyOTNiMmJmMjkyNDA1OWVkMTdkZTFiMjdkODp7Il9hdXRoX3VzZXJfaGFzaCI6ImFjZWE0Mjk5ODg5YmRmYTA0YzMxY2FkYTczMzFjZWM5ZGQ5YjI0Y2MiLCJfYXV0aF91c2VyX2lkIjoiMTAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2016-10-10 15:46:27.229015'),('htuo1q9hjqokcfn3n7ixn9ubkij9t07v','NzJmMTFlNjM1MGEzNDRjN2MwMGU4N2Q3NmU1M2YwMDJjOWY5YTA1Mjp7Il9hdXRoX3VzZXJfaGFzaCI6IjdlMjI1Y2Y2NDljY2YzMTJjYTg0ZTdkY2YzZWE5MmI4MTk0NGI0MTQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2016-10-10 14:49:42.194809'),('jkpecrvgpchiqlyhr00y9j889alzqolh','NzJmMTFlNjM1MGEzNDRjN2MwMGU4N2Q3NmU1M2YwMDJjOWY5YTA1Mjp7Il9hdXRoX3VzZXJfaGFzaCI6IjdlMjI1Y2Y2NDljY2YzMTJjYTg0ZTdkY2YzZWE5MmI4MTk0NGI0MTQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2016-10-10 15:08:41.585183'),('kbomkbl6t18p783qgom8i178s86xuqhv','Y2I5NWY5Y2I4MDE0ODQ1MmY0ZjhiYWJiOTdmMjQxODkxMTEwMzk3Yzp7Il9hdXRoX3VzZXJfaGFzaCI6IjQzNzE5YzUyOGM1ODAyNDU0NmRmZWMxMjQxYjQ0YjU3NzYyM2MwZWIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=','2016-10-10 20:27:02.628224'),('kjht3p0etmddqvt66dzp8tq5fh0ur6rj','NWY1OGMzMTE3MjhkMjhkMTNkNmU4NjUwYzg3NjA4MDQ4ZmE3YjQ5ZDp7Il9hdXRoX3VzZXJfaGFzaCI6IjdkMzJiYzBkY2UyOGRkYTg1MmMxYjQ2ZGU1OWFlMjY5NTViYzg5M2IiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI5In0=','2016-10-10 15:43:08.109458'),('rknf5pzn3tz46lovtun4fs78jzwriwu0','YmRkYjVmNDVhMjMxODcxODEyOWIwOWM2OWFiMjEwZTE0NTZlZGU3ODp7Il9hdXRoX3VzZXJfaGFzaCI6ImM0YzkyZDRkZDVkMDJjNjdlYTg2NDNlOWEyMDM2ZTc0NTQ2MzM5NmUiLCJfYXV0aF91c2VyX2lkIjoiNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2016-10-10 15:33:01.365194');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_status`
--

DROP TABLE IF EXISTS `message_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `channel` int(11) NOT NULL,
  `is_read` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_status`
--

LOCK TABLES `message_status` WRITE;
/*!40000 ALTER TABLE `message_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `message_status` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-27  4:33:31
